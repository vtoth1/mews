from lstm import *
from prepare_seq_input_prosp import *
import sys


if __name__ == '__main__':

    select_gpu(0)

    cfg = {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight7',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'], # 'BMINum', varscores removed, FullScore
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [256] * 5,  # 256
        'min_seq_len': 8,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.6,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': 'dropout_batchnorm_final_ens0:24'  # but_regr, w_rnn (order: rnn, dropout, batchnorm, ens)
    }
    cfg_prosp = copy.deepcopy(cfg)
    cfg_prosp['test_data_ratio'] = 1.  # all into test and validation

    data_size = 'small'  # mini/small/medium/''

    base_folder = '/mnt/data/mews/seqs_test/'
    base_folder_prospective = '/mnt/data/mews/seqs_prosp_test/'
    dsize = '' if data_size == '' else data_size + '_'
    mews_table = 'data/' + dsize + 'processed_mews2.csv'
    mews_prospective = 'data/' + dsize + 'processed_mews3.csv'
    dfname = data_file_name(cfg, data_size)
    dfname_prosp = data_file_name(cfg_prosp, data_size)

    # determine threshold first using retrospective data
    # print('LOADING DATA')
    # train_data_x, train_data_y, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies = \
    #     load_data(cfg, base_folder + dfname)

    print('BUILDING MODEL')
    # inp_dim = train_data_x.shape[2]
    # out_dim = train_data_y.shape[1]
    # print(inp_dim, out_dim)
    inp_dim = 27
    out_dim = 1
    model = build_model(cfg, inp_dim, out_dim, load_checkpoint=True, force_load=True)

    print('TESTING BEGINS TO GET THRESHOLDS')
    # test_folder = 'results/' + model_name(cfg) + '/'
    # thresholds = test_sleeping(model, cfg, test_data_x, test_data_y, x_cols, y_cols, normies, test_folder,
    #                            overwrite_results=False)
    # print('THRESHOLDS:', thresholds)
    # threshold = thresholds[-1]
    threshold = 0.01618314

    h5fname = base_folder_prospective + data_file_name(cfg_prosp, data_size)
    with open(h5fname + '_NIGHTS.pickle', 'rb') as f:
        NIGHTS = pickle.load(f)

    r = 0
    all_preds = []
    all_desired = []
    while True:
        print('CREATING PROSPECTIVE DATA')
        SAMPLES = create_seq_prosp(cfg_prosp, mews_prospective, NIGHTS, data_size, base_folder_prospective)
        print('SAMPLES size:', len(SAMPLES['test_data_y']))
        if len(SAMPLES) == 0:
            break

        print('LOAD PROSPECTIVE DATA')
        train_data_x, train_data_y, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies = \
            load_data(cfg, SAMPLES, load_from_prosp_dict=True)
        test_newid, test_start, test_end, test_pred_start, test_pred_end, test_dtms = load_ref_data_prosp(SAMPLES)

        # predict
        preds, desired = predict(model, cfg_prosp, test_data_x, test_data_y)
        all_preds.append(preds)
        all_desired.append(desired)
        sleepers = preds < threshold
        print('PREDS: {}% sleep ({})'.format(sleepers.mean() * 100, sleepers.sum()))

        # update NIGHTS flags
        for pred, newid, pred_start, pred_end in zip(preds, test_newid, test_pred_start, test_pred_end):
            newid = newid.decode('ascii')
            if pred < threshold:  # let sleep
                for i in range(len(NIGHTS[newid])):
                    if NIGHTS[newid][i][2] and date_overlap(pd.Timestamp(pred_start), pd.Timestamp(pred_end),
                                                            NIGHTS[newid][i][0], NIGHTS[newid][i][1]):
                        NIGHTS[newid][i][3] = True  # let sleep flag
                        break  # only one night predicted per newid
                    elif not NIGHTS[newid][i][2]:
                        break  # chronologically reached a night that is not predicted, so no nights after are

    all_preds = np.concatenate(all_preds)
    all_desired = np.concatenate(all_desired)

    with open('all_prosp_preds_and_desired_realdeal4.pickle', 'wb') as f:
        pickle.dump({'preds': all_preds, 'desired': all_desired}, f)

    # # check results
    # with open('all_prosp_preds_and_desired_realdeal4.pickle', 'rb') as f:
    #     save = pickle.load(f)
    #     preds = save['preds']
    #     desired = save['desired']
    #
    # model_idx_prosp = np.array(
    #     [np.argmin(np.abs(resp['thresholdsinv'] - th)) for th in res['thresholdsinv'][model_idx]])
    #
    # fig = plot_roc(resp['fprinv'], resp['tprinv'], resp['thresholdsinv'], model_idx_prosp, n, nfpinv)
    # fig2 = plot_tn_vs_fn(len(resp['predsinv']), resp['tpsinv'], resp['fpsinv'], resp['thresholdsinv'], model_idx_prosp,
    #                      sys.stdout)
    # plt.show()
