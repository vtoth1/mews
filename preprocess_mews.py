import pandas as pd
import numpy as np
import swifter
from scipy import stats
import tqdm


def npgroupby(a, by):
    return np.split(a[:, :], np.cumsum(np.unique(a[:, by], return_counts=True)[1])[:-1])


def ungroup(grpd):
    return np.array([row for grp in grpd for row in grp])


def update_avpu(score_update, x):
    if x['AVPU'] in score_update:
        old_score = x['AVPUScore']
        new_score = max(score_update[x['AVPU']], old_score)  # keeps old if greater
        fullscore_delta = new_score - old_score
        x['AVPUScore'] = new_score
        x['FullScore'] += fullscore_delta
    return x


def preprocess(mews_df_path, output_df_path, min_record_per_visit):
    # load
    mews = pd.read_csv(mews_df_path, na_values=['missing height or weight', 'not documented', ''], parse_dates=['AlertDtm'])
    mews = mews.sort_values(['ClientVisitGUID', 'AlertDtm']).reset_index()  # should already be sorted though
    print('ORIGINAL NROWS:', len(mews))
    print('NVISITS raw:', len(mews.groupby(by='ClientVisitGUID')))
    print('Earliest, latest dates:', mews['AlertDtm'].min(), mews['AlertDtm'].max())

    # add extra columns
    mews['ts'] = mews['AlertDtm'].swifter.apply(lambda x: x.value / 3.6e+12)  # timestamp in hours
    mews['td'] = np.int64(0)
    mews['dfs'] = np.float32(0)  # delta FullScore
    mews['tod'] = mews['AlertDtm'].swifter.apply(lambda x: x.hour)  # hour of day
    mews['relts'] = np.int64(0)
    mews['newid_flag'] = np.int32(0)
    mews['newid'] = mews['ClientVisitGUID'].swifter.apply(lambda x: str(x))
    mews['fsgt7'] = mews['FullScore'] >= 7
    mews['last'] = np.int32(0)

    # filter outliers
    # (instead of hard coded valid thresholds, the 95 percentile of the field could be used)
    mews.loc[mews['BMINum'] > 100, 'BMINum'] = np.NaN
    mews.loc[mews['BMINum'] < 2, 'BMINum'] = np.NaN
    mews.loc[mews['HRNum'] > 300, 'HRNum'] = np.NaN
    mews.loc[mews['HRNum'] < 10, 'HRNum'] = np.NaN
    mews.loc[mews['RespRateNum'] > 47, 'RespRateNum'] = np.NaN
    mews.loc[mews['RespRateNum'] < 5, 'RespRateNum'] = np.NaN
    mews.loc[mews['BPNum'] > 300, 'BPNum'] = np.NaN
    mews.loc[mews['BPNum'] < 50, 'BPNum'] = np.NaN
    mews.loc[mews['TmprNum'] > 50, 'TmprNum'] = np.NaN
    mews.loc[mews['TmprNum'] < 20, 'TmprNum'] = np.NaN
    mews.loc[mews['FullScore'] > 15, 'FullScore'] = 15
    mews.loc[mews['AgeNum'] < 0, 'AgeNum'] = np.NaN
    mews.loc[mews['AgeNum'] > 89, 'AgeNum'] = 89

    # fill age (barely decreases non-nan tho)
    mean_age_per_id = mews.groupby('ClientVisitGUID')['AgeNum'].mean()
    mews = mews.join(mean_age_per_id, on='ClientVisitGUID', how='left', rsuffix='_recov')
    mews['AgeNum'] = mews['AgeNum'].fillna(mews['AgeNum_recov'])
    mews = mews.drop('AgeNum_recov', axis=1)
    col = {v: i for i, v in enumerate(mews.columns.values)}
    cols = [c for c in mews.columns.values]

    # avpu score assignment
    score_update = {'arouses to voice': 1, 'confused': 1, 'lethargic': 1, 'arouses to touch/gentle shaking': 1,
                    'arouses to pain': 2, 'unresponsive': 3, 'arouses to repeated stimulation': 1,
                    'arouses to vigorous stimulation': 2, 'obtunded': 3, 'somnolent': 2, 'sedated': 2,
                    'arouses tovoice': 1,
                    'arousesto voice': 1, 'semicomatose': 3}
    mews = mews.swifter.apply(lambda x: update_avpu(score_update, x), axis=1)

    mvalue_cols = [c for c in mews.columns.values if 'Num' in c]
    mscore_cols = [c[:-3] + 'Score' for c in mvalue_cols]  # cut off 'Num', append 'Score'; aligned to mvalue_cols

    # construct td using numpy
    rmews = np.array(mews)
    td = np.concatenate([np.array([0]), np.diff(rmews[:, col['ts']])])
    mews['td'] = td

    # assign new id after large gap (without grouping by id, as dataset is already ordered by id and time)
    # columns to add in order: dtm diff, >24, accum >24, individual id for different accum where >0
    new_id_limit = 24  # hours
    mews['newid_flag'] = mews['td'].swifter.apply(lambda x: np.int32(x > new_id_limit))
    mews['newid_flag'] = mews['newid_flag'].cumsum(axis=0)
    mews['newid'] = mews.swifter.apply(lambda x: x['newid'] + '_' + str(x['newid_flag']), axis=1)

    mews = mews.sort_values(['newid', 'AlertDtm'])
    rmews = np.array(mews)
    rvisits = npgroupby(rmews, col['newid'])

    # measure amount of nan values here
    for c in mvalue_cols + ['FullScore']:
        print('Rows with NaNs for {}: {}'.format(c, mews.shape[0] - mews.dropna(axis=0, subset=[c]).shape[0]))

    # add relative time taken from the first encounter (relts)
    # update avpu score if current is null and prev <12h
    # update all num nan values and corresponding scores with prev available value
    # note: column has to be named '*Num' and it should have a corresponding '*Score' column to be updated
    # also, delta full score is also defined here
    avpu_copy_window = 12  # hours
    num_copy_window = [12] * len(mvalue_cols)  # hours, for all num fields, indexed as mvalue_cols; this is the default
    num_copy_window[mvalue_cols.index('AgeNum')] = 4380  # half a year
    num_copy_window[mvalue_cols.index('BMINum')] = 720  # 1 month
    n_fs_updated = 0
    for visit_i, visit in enumerate(rvisits):
        visit[0, col['td']] = 0  # set the first time diff element to 0
        visit[:, col['relts']] = np.cumsum(visit[:, col['td']], axis=0)
        visit[-1, col['last']] = 1

        last_num_values = [np.NaN] * len(mvalue_cols)
        last_score_values = [0] * len(mvalue_cols)
        time_accum = [9999] * len(mvalue_cols)

        last_avpu_state = np.NaN
        last_avpu_score = 0
        avpu_time_accum = 9999  # it's zeroed out when encountering non-nan

        prev_fscore = visit[0, col['FullScore']]
        for row_i, row in enumerate(visit):
            # add_to_fullscore = 0
            avpu_time_accum += row[col['td']]
            time_accum = [t + row[col['td']] for t in time_accum]

            # all num (and score) update
            for col_i, (num_col, score_col) in enumerate(zip(mvalue_cols, mscore_cols)):
                if pd.isnull(row[col[num_col]]) and time_accum[col_i] < num_copy_window[col_i] and not pd.isnull(
                        last_num_values[col_i]):
                    row[col[num_col]] = last_num_values[col_i]
                    row[col[score_col]] = last_score_values[col_i] if not pd.isnull(last_score_values[col_i]) else 0.
                elif not pd.isnull(row[col[num_col]]):  # update latest values
                    last_num_values[col_i] = row[col[num_col]]
                    last_score_values[col_i] = row[col[score_col]]
                    time_accum[col_i] = 0

            # AVPU update (AVPU does not have 'Num' in its column name, so it's not included in the general case)
            # TODO weighted interpolation by dt
            if pd.isnull(row[col['AVPU']]) and avpu_time_accum < avpu_copy_window and not pd.isnull(last_avpu_state):
                row[col['AVPU']] = last_avpu_state
                row[col['AVPUScore']] = last_avpu_score if not pd.isnull(last_avpu_score) else 0.
                # add_to_fullscore += last_avpu_score if not pd.isnull(last_avpu_score) else 0.
            elif not pd.isnull(row[col['AVPU']]):  # update latest values
                last_avpu_state = row[col['AVPU']]
                last_avpu_score = row[col['AVPUScore']]
                avpu_time_accum = 0

            # update scores that are still null to be zero
            # TODO MEWS score computation logic here
            for score_col in mscore_cols:
                if pd.isnull(row[col[score_col]]):
                    row[col[score_col]] = 0

            # update full score and delta full score
            fs = row[col['FullScore']]
            fs = fs if not np.isnan(fs) else 0.
            new_fs = np.nansum(row[[col[c] for c in mscore_cols]])
            if fs != new_fs:
                n_fs_updated += 1
            # new_fs = fs + add_to_fullscore
            row[col['FullScore']] = new_fs
            row[col['dfs']] = new_fs - prev_fscore
            prev_fscore = new_fs

        visit[0, col['dfs']] = 0  # set the first mews score diff to 0; just to be sure

    print('Number of MEWS scores updated:', n_fs_updated)

    # ... and measure amount of nan values here
    mews = pd.DataFrame(ungroup(rvisits), columns=cols)
    for c in mvalue_cols + ['FullScore']:
        print('Rows with NaNs after update for {}: {}'.format(c, mews.shape[0] - mews.dropna(axis=0, subset=[c]).shape[0]))
    print('Rows in total:', mews.shape[0])

    # rm visits with less than n records
    L = len(rvisits)
    rvisits = np.array([visit for visit in rvisits if visit.shape[0] > min_record_per_visit])
    L2 = len(rvisits)
    print('Visits before and after removal for being shorter than {}: {} out of {} ({}%)'.format(min_record_per_visit,
                                                                                                 L2, L, L2 / L))

    # print statistics
    print('Median of MEWS: {}; std: {}; iqr: {}'.format(mews['FullScore'].median(), mews['FullScore'].std(), stats.iqr(mews['FullScore'])))
    for c in mvalue_cols:
        print('Mean of {}: {} ({})'.format(c, mews[c].mean(), mews[c].std()))

    # save file
    mews = pd.DataFrame(ungroup(rvisits), columns=cols)
    print('NROWS after removal:', len(mews))
    mews = mews.loc[:, ~mews.columns.str.contains('^Unnamed')]
    mews.to_csv(output_df_path, index=False)

    return mews


def preprocess_lskl(mews_df_path, output_df_path, min_record_per_visit):  # TODO !!!
    # load
    mews = pd.read_csv(mews_df_path, parse_dates=['AlertDtm'])
    mews = mews.sort_values(['ClientVisitGUID', 'AlertDtm']).reset_index()  # should already be sorted though
    print('ORIGINAL NROWS:', len(mews))
    print('NVISITS raw:', len(mews.groupby(by='ClientVisitGUID')))
    print('Earliest, latest dates:', mews['AlertDtm'].min(), mews['AlertDtm'].max())

    # add extra columns
    mews['ts'] = mews['AlertDtm'].astype(int) / 3.6e+12  # timestamp in hours
    mews['td'] = np.int64(0)
    mews['tod'] = mews['AlertDtm'].swifter.apply(lambda x: x.hour)  # hour of day
    mews['relts'] = np.int64(0)
    mews['newid_flag'] = np.int32(0)
    mews['newid'] = mews['ClientVisitGUID'].swifter.apply(lambda x: str(x))
    mews['last'] = np.int32(0)

    # filter outliers TODO
    mews.loc[mews['HR'] > 500, 'HR'] = np.NaN
    mews.loc[mews['HR'] < 10, 'HR'] = np.NaN
    mews.loc[mews['SBP'] > 300, 'SBP'] = np.NaN
    mews.loc[mews['SBP'] < 20, 'SBP'] = np.NaN
    mews.loc[mews['TC'] > 50, 'TC'] = np.NaN
    mews.loc[mews['TC'] < 20, 'TC'] = np.NaN
    mews.loc[mews['age_days'] < 0, 'age_days'] = np.NaN
    mews.loc[mews['age_days'] > 21 * 365, 'age_days'] = np.NaN

    col = {v: i for i, v in enumerate(mews.columns.values)}
    cols = [c for c in mews.columns.values]
    mvalue_cols = ['HR', 'TC', 'SBP', 'SPO2', 'age_days']

    # construct td using numpy
    rmews = np.array(mews)
    td = np.concatenate([np.array([0]), np.diff(rmews[:, col['ts']])])
    mews['td'] = td

    # assign new id after large gap (without grouping by id, as dataset is already ordered by id and time)
    # columns to add in order: dtm diff, >24, accum >24, individual id for different accum where >0
    new_id_limit = 24  # hours
    mews['newid_flag'] = mews['td'].swifter.apply(lambda x: np.int32(x > new_id_limit))
    mews['newid_flag'] = mews['newid_flag'].cumsum(axis=0)
    mews['newid'] = mews.swifter.apply(lambda x: x['newid'] + '_' + str(x['newid_flag']), axis=1)

    mews = mews.sort_values(['newid', 'AlertDtm'])
    rmews = np.array(mews)
    rvisits = npgroupby(rmews, col['newid'])

    # measure amount of nan values here
    for c in mvalue_cols:
        print('Rows with NaNs for {}: {}'.format(c, mews.shape[0] - mews.dropna(axis=0, subset=[c]).shape[0]))

    # rm visits with less than n records
    L = len(rvisits)
    rvisits = np.array([visit for visit in rvisits if visit.shape[0] > min_record_per_visit])
    L2 = len(rvisits)
    print('Visits before and after removal for being shorter than {}: {} out of {} ({}%)'.format(min_record_per_visit,
                                                                                                 L2, L, L2 / L))

    # print statistics
    for c in mvalue_cols:
        print('Mean of {}: {} ({})'.format(c, mews[c].mean(), mews[c].std()))
    print(f'Mean of td: {mews["td"].loc[mews["td"] > 0].mean()} ({mews["td"].loc[mews["td"] > 0].std()})')

    # save file
    mews = pd.DataFrame(ungroup(rvisits), columns=cols)
    print('NROWS after removal:', len(mews))
    mews = mews.loc[:, ~mews.columns.str.contains('^Unnamed')]
    mews.to_csv(output_df_path, index=False)

    return mews


if __name__ == '__main__':

    min_record_per_visit = 4
    data_size = ''  # 'mini_' or nothing

    # # MEWS
    # mews_df_path = '/mnt/data/mews/tables/' + data_size + 'mews2.csv'  # retrospective
    # output_df_path = '/mnt/data/mews/tables/' + data_size + 'processed_mews2.csv'
    # # mews_df_path = '/mnt/data/mews/tables/' + data_size + 'mews3.csv'  # prospective
    # # output_df_path = '/mnt/data/mews/tables/' + data_size + 'processed_mews3.csv'
    #
    # preprocess(mews_df_path, output_df_path, min_record_per_visit)

    # lskl
    lskl_ver = '030521'
    lskl_path = '/mnt/data/lskl/tables'
    lskl_df_path = f'{lskl_path}/{data_size}lskl_{lskl_ver}.csv'
    output_df_path = f'{lskl_path}/{data_size}processed_lskl_{lskl_ver}.csv'
    preprocess_lskl(lskl_df_path, output_df_path, min_record_per_visit)


# MEWS
# latest retrospective:
# ORIGINAL NROWS: 24288165
# NVISITS raw: 2132131
# Earliest, latest dates: 2012-02-23 16:06:17+00:00 2019-04-30 21:41:50+00:00
# Rows with NaNs for BPNum: 1148109
# Rows with NaNs for HRNum: 977073
# Rows with NaNs for TmprNum: 3264432
# Rows with NaNs for RespRateNum: 2003646
# Rows with NaNs for BMINum: 4012546
# Rows with NaNs for AgeNum: 193
# Rows with NaNs for FullScore: 30284
# Number of MEWS scores updated: 2081856
# Rows with NaNs after update for BPNum: 108493
# Rows with NaNs after update for HRNum: 58689
# Rows with NaNs after update for TmprNum: 369191
# Rows with NaNs after update for RespRateNum: 156185
# Rows with NaNs after update for BMINum: 3914057
# Rows with NaNs after update for AgeNum: 193
# Rows with NaNs after update for FullScore: 0
# Rows in total: 24288165
# Visits before and after removal for being shorter than 8: 716038 out of 2249023 (0.31837735763484853%)
# Median of MEWS: 3.0 1.4940701235879128
# Mean of BPNum: 128.4191149904763 (22.632169885643258)
# Mean of HRNum: 81.33733044825237 (16.570734356050266)
# Mean of TmprNum: 36.78505828501029 (0.4992902556486007)
# Mean of RespRateNum: 17.800845434150038 (1.9742522792436357)
# Mean of BMINum: 28.22127432246684 (7.5982825969180325)
# Mean of AgeNum: 64.42412943418506 (19.006462145955542)
# NROWS after removal: 20683459


# prospective:
# ORIGINAL NROWS: 1912865
# NVISITS raw: 186375
# Earliest, latest dates: 2019-04-29 04:00:01+00:00 2019-08-22 01:50:27+00:00
# Rows with NaNs for BPNum: 94092
# Rows with NaNs for HRNum: 72627
# Rows with NaNs for TmprNum: 255946
# Rows with NaNs for RespRateNum: 160004
# Rows with NaNs for BMINum: 315510
# Rows with NaNs for AgeNum: 0
# Rows with NaNs for FullScore: 3
# Number of MEWS scores updated: 156232
# Rows with NaNs after update for BPNum: 11513
# Rows with NaNs after update for HRNum: 5506
# Rows with NaNs after update for TmprNum: 26235
# Rows with NaNs after update for RespRateNum: 12365
# Rows with NaNs after update for BMINum: 311666
# Rows with NaNs after update for AgeNum: 0
# Rows with NaNs after update for FullScore: 0
# Rows in total: 1912865
# Visits before and after removal for being shorter than 8: 55296 out of 193778 (0.28535747092033154%)
# Median of MEWS: 3.0 1.5038470969095037
# Mean of BPNum: 128.95928213187247 (22.983604776560664)
# Mean of HRNum: 81.15886574053442 (16.787383038608418)
# Mean of TmprNum: 36.759408363089825 (0.529161589039048)
# Mean of RespRateNum: 17.80707708497764 (1.9607289767630882)
# Mean of BMINum: 28.236105006298473 (7.525581001577798)
# Mean of AgeNum: 63.99766423662935 (18.779205797265448)
# NROWS after removal: 1564966
