import numpy as np
import tensorflow as tf
from tensorflow.python import keras
import tensorflow_probability as tfp
import tables
from tensorflow.python.keras import Sequential
from tensorflow.python.keras.layers import Dense, LSTM, CuDNNLSTM, TimeDistributed, Dropout, BatchNormalization, SimpleRNN, Bidirectional
import matplotlib.pyplot as plt
import pandas as pd
import os, shutil
from prepare_seq_input import data_file_name, create_seq
from pprint import pprint
from sklearn.metrics import roc_curve, auc, confusion_matrix, precision_recall_curve, f1_score, matthews_corrcoef, roc_auc_score
from scipy.stats import norm
import copy
from matplotlib.patches import Rectangle
import pickle
from prepare_seq_input_prosp import create_seq_prosp, date_overlap
from collections.abc import Iterable
tfd = tfp.distributions


def model_name(cfg):
    layers = '-'.join([str(l) for l in cfg['layers']])
    augm_step_size = cfg['data_augmentation']['step']
    augm_skip = cfg['data_augmentation']['skip']
    augm = '{}-{}-{}-{}'.format(augm_step_size[0], augm_step_size[1], augm_skip[0], augm_skip[1])
    balancing = '{}_{}:{}-{}:{}'.format(cfg['majority_class_rate'], 0, cfg['class_weight'][0], 1, cfg['class_weight'][1])
    return 'model_{}_{}_{}_{}_{}_{}_{}_{}_{}_{}-{}_{}'.format(cfg['type'], cfg['task'], cfg['test_rand_scheme'], augm, balancing, '+'.join(cfg['input']), '+'.join(cfg['predict']),
                                                              layers, '+'.join(cfg['onehot_fields']), cfg['min_seq_len'], cfg['seq_len'], cfg['name_suffix'])


def select_gpu(gpu_id=-1, max_usage=.9, disable_eager=True):  # max 2 gpu only
    os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu_id) if gpu_id != -1 else '0,1'
    gpus = tf.config.experimental.list_physical_devices('GPU')
    max_memory = 11534  # MB got from: grep -i --color memory /var/log/Xorg.0.log
    for gpu in gpus:
        print('GPU FOUND:', gpu)
        tf.config.experimental.set_memory_growth(gpu, True)
        # tf.config.experimental.set_virtual_device_configuration(gpu,
        #     [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=max_memory * max_usage)])
    print('RUNNING ON GPU #{}'.format(gpu_id))
    if disable_eager:
        tf.compat.v1.disable_eager_execution()


def not_padding_from(x):  # assuming left sided padding; returns start index after padding
    # x: nsample x nseq x nfeature
    pad = np.all(x == 0, axis=2)  # all features are 0 -> padding
    npad = np.sum(pad, axis=1)
    return npad  # for each sequence the end (exclusive) index of padding


def not_padding(x):  # removes padded parts of the sequence
    return ~np.all(x == 0, axis=2)


class normie:  # aka min-max normalization
    def __init__(self, data=None, minmax=None):
        assert not(data is None and minmax is None)
        if data is not None:
            self.min = np.min(data)
            self.max = np.max(data)
        else:
            self.min, self.max = minmax  # tuple

    def __call__(self, data):
        return (data - self.min) / (self.max - self.min)

    def reverse(self, normed_data):
        return normed_data * (self.max - self.min) + self.min

    def mm(self):
        return self.min, self.max


# functions defined to turn columns into onehot representations
# function should return the onehot values and the corresponding column names
def onehot_encode(x, levels=None):  # only for uniformly binned integer columns
    if levels is None:
        levels = int(x.max() - x.min() + 1)
    xflat = x.flatten()
    onehot = np.zeros((len(xflat), levels), dtype=np.float32)
    onehot[np.arange(len(xflat)), xflat.astype(np.int32) - int(xflat.min())] = 1.
    onehot = onehot.reshape([x.shape[0], x.shape[1], levels])
    return onehot

ONEHOT_TD_BINS = np.array([0.5, 1., 2., 3., 5., 7., 9., 12., 16., 5000.])  # in hours
# delta time (td) can be > 24h, because we remove rows in prepare_seq_input,
# and then update dt without spawning new visits
def onehot_td(td):
    # the extra one is needed because np.digitize return nbins+1 bins
    # onehot_td_cols = ['onehotdt{}'.format(i) for i in range(len(ONEHOT_TD_BINS))] + ['onehotdt{}'.format(len(ONEHOT_TD_BINS))]
    digitized = np.digitize(td, bins=ONEHOT_TD_BINS, right=True)
    onehot = onehot_encode(digitized, len(ONEHOT_TD_BINS))
    onehot_td_cols = ['onehotdt{}'.format(i) for i in range(onehot.shape[-1])]
    return onehot, onehot_td_cols

def onehot_fs(fs):
    onehot_fs_cols = ['onehotfs{}'.format(i) for i in range(16)]
    return onehot_encode(fs, len(onehot_fs_cols)), onehot_fs_cols

def onehot_tod(tod, bin_size=3):
    onehot_tod_cols = ['onehottod{}'.format(i) for i in range(bin_size//2, 24, bin_size)]  # mid value of bins
    onehot = onehot_encode(tod, 24)
    binned_shape = list(onehot.shape) + [bin_size]
    binned_shape[-2] = binned_shape[-2] // bin_size  # account for the extra dimension
    onehot = np.reshape(onehot, binned_shape)
    onehot = np.sum(onehot, axis=-1)  # sum within bins
    return onehot, onehot_tod_cols

def onehot_hospital(hosp):
    return onehot_encode(hosp), ['hospital{}'.format(i) for i in range(int(np.max(hosp)))]

ONEHOT_FUNS = {'td': onehot_td, 'FullScore': onehot_fs, 'tod': onehot_tod, 'Hospital': onehot_hospital}  # input is 2D

# reverse onehot functions, turns ARRAY OF onehot values to numerical
def rev_onehot_td(onehot_td):
    return ONEHOT_TD_BINS[np.argmax(onehot_td, axis=1)]

def rev_simple(onehot_fs):
    return np.argmax(onehot_fs, axis=1)

def rev_onehot_tod(onehot_tod, bin_size=3):  # static bin size defined here
    # assume that the hour is at the middle of the bin
    midhours = np.arange(bin_size//2, 24, bin_size)
    return midhours[np.argmax(onehot_tod)]

REV_ONEHOT_FUNS = {'td': rev_onehot_td, 'FullScore': rev_simple, 'tod': rev_onehot_tod}  # input is nD, array of 1hot arrays


def posterior_mean_field(kernel_size, bias_size=0, dtype=None):
    with tf.name_scope('post_densevar'):
        n = kernel_size + bias_size
        c = np.log(np.expm1(1.))
        return keras.Sequential([
              tfp.layers.VariableLayer(2 * n, dtype=dtype),
              tfp.layers.DistributionLambda(lambda t: tfd.Independent(
                  # tfd.Bernoulli(probs=t, validate_args=True),
                  tfd.Normal(loc=t[..., :n],
                             scale=1e-5 + tf.nn.softplus(c + t[..., n:])),
                  reinterpreted_batch_ndims=1)),
        ])


def prior_trainable(kernel_size, bias_size=0, dtype=None):
    with tf.name_scope('prior_densevar'):
        n = kernel_size + bias_size
        return keras.Sequential([
              tfp.layers.VariableLayer(n, dtype=dtype),
              tfp.layers.DistributionLambda(lambda t: tfd.Independent(
                  tfd.Normal(loc=t, scale=1),
                  reinterpreted_batch_ndims=1)),
        ])


def inp_of_increasing_dt(base, ddt, nrep, td_ind=0):
    res = np.repeat(np.expand_dims(base, 0), nrep, axis=0)
    dts = np.linspace(start=ddt, stop=ddt*(nrep+1), num=nrep)
    res[:, -1, td_ind] = dts
    return res, dts


def hard_balance(pos_indices, neg_indices, max_desired_neg_ratio=0.75):
    npos = len(pos_indices)
    nneg = len(neg_indices)
    max_desired_nneg = int(npos / (1 - max_desired_neg_ratio) * max_desired_neg_ratio)
    if max_desired_nneg < nneg:
        neg_indices = np.random.choice(neg_indices, max_desired_nneg, replace=False)

    indices = np.concatenate((pos_indices, neg_indices), axis=0)
    np.random.shuffle(indices)
    return indices


def build_model(cfg, inp_dim, out_dim, load_checkpoint=True, force_load=False, model_path='models'):  # data only used for input/output dimension estimation
    layers = cfg['layers']

    # TODO add noisy initial state - FOR THIS you need to define the model as a class,
    #   and provide the init state in the call function to the lstms
    #   https://r2rt.com/non-zero-initial-states-for-recurrent-neural-networks.html
    #   https://www.tensorflow.org/versions/r2.0/api_docs/python/tf/keras/layers/LSTM
    #   ! https://github.com/tensorflow/tensorflow/issues/28761

    with tf.device('/device:GPU:0'):
        rnn_type = CuDNNLSTM  # default
        if 'cpucompat' in cfg['name_suffix']:  # cudnn lstm is not compatible with the cpu
            rnn_type = LSTM
        if 'rnn' in cfg['name_suffix']:
            rnn_type = SimpleRNN
        if 'bidirectional' in cfg['name_suffix']:
            lstm_layers = [Bidirectional(rnn_type(layers[i], input_shape=(cfg['seq_len'], inp_dim), return_sequences=True))
                           for i in range(len(layers) - 1)] + [rnn_type(layers[-1], input_shape=(cfg['seq_len'], inp_dim))]
        else:
            lstm_layers = [rnn_type(layers[i], input_shape=(cfg['seq_len'], inp_dim), return_sequences=True)
                           for i in range(len(layers) - 1)] + [rnn_type(layers[-1], input_shape=(cfg['seq_len'], inp_dim))]

        all_layers = [TimeDistributed(Dense(layers[0], activation=None), input_shape=(cfg['seq_len'], inp_dim))] + lstm_layers
        all_layers = all_layers + [Dense(layers[-1], activation=None)]  # if cfg['type'] != 'var_normal' else all_layers

        if 'dropout' in cfg['name_suffix']:
            all_layers += [Dropout(0.5)]
        if 'batchnorm' in cfg['name_suffix']:
            all_layers += [BatchNormalization()]

        model = Sequential(all_layers)
        # output distribution according to task
        activation_fun = None  # default values

        if 'over' in cfg['task']:
            activation_fun = 'sigmoid'
            output_distr_param_dim = out_dim
            output_distr = lambda t: tfd.Bernoulli(probs=t, validate_args=True)
        else:
            output_distr_param_dim = out_dim * 2
            output_distr = lambda t: tfd.Normal(loc=t[..., :out_dim],
                                                scale=1e-3 + tf.math.softplus(0.01 * t[..., out_dim:]))
        if cfg['type'] == 'regression':
            model.add(Dense(out_dim, activation=activation_fun))
        elif cfg['type'] == 'simple_normal':
            model.add(Dense(output_distr_param_dim, activation=activation_fun))
            model.add(tfp.layers.DistributionLambda(output_distr),)
        elif cfg['type'] == 'var_normal':
            with tf.name_scope('denselol'):
                dense_var = tfp.layers.DenseVariational(min(layers[-1], 256), posterior_mean_field, prior_trainable,
                                                        activation='tanh', kl_weight=1/layers[-1])
            model.add(dense_var)
            model.add(Dense(layers[-1], activation='tanh'))
            model.add(Dense(layers[-1], activation='tanh'))
            model.add(Dense(output_distr_param_dim, activation=activation_fun))
            model.add(tfp.layers.DistributionLambda(output_distr),)
        # TODO tfd.OneHotCategorical

        # load model if exists
        if load_checkpoint and os.path.isfile(f'{model_path}/{model_name(cfg)}.h5'):
            model.load_weights(f'{model_path}/{model_name(cfg)}.h5')
            print('CHECKPOINT LOADED')
        elif force_load:
            raise FileNotFoundError(f'NO MODEL FOUND: {model_path}/{model_name(cfg)}.h5')

    return model


def onehot_dat_col(data, onehot_col, x_cols, categorical_features):
    if len(data) > 0:
        onehot_data, onehot_extra_cols = ONEHOT_FUNS[onehot_col](data[:, :, x_cols.index(onehot_col)])
        is_padding = np.all(data == 0, axis=2)

        if onehot_col in categorical_features:  # then name the columns according to the categorical names
            onehot_extra_cols = sorted([(num_val, name) for name, num_val in categorical_features[onehot_col].items()])
            onehot_extra_cols = ['{}:{}'.format(name, num_val) for num_val, name in onehot_extra_cols]

        onehot_data[is_padding, :] = 0.  # padding should stay 0
        data = np.delete(data, x_cols.index(onehot_col), axis=-1)
        data = np.append(data, onehot_data, axis=-1)

        return data, onehot_extra_cols
    return data, None


def load_ref_data(from_path):
    h5file = tables.open_file(from_path, mode='r')

    train_newid = np.array(h5file.root.train_newid)
    valid_newid = np.array(h5file.root.valid_newid)
    test_newid = np.array(h5file.root.test_newid)

    train_start = np.array(h5file.root.train_start)
    valid_start = np.array(h5file.root.valid_start)
    test_start = np.array(h5file.root.test_start)

    train_end = np.array(h5file.root.train_end)
    valid_end = np.array(h5file.root.valid_end)
    test_end = np.array(h5file.root.test_end)

    train_pred_start = valid_pred_start = test_pred_start = train_pred_end = valid_pred_end = test_pred_end = \
        train_dtms = valid_dtms = test_dtms = None  # previous version of seq - only for post analyze to work with retro

    if hasattr(h5file.root, 'train_pred_start'):
        train_pred_start = np.array(h5file.root.train_pred_start)
        valid_pred_start = np.array(h5file.root.valid_pred_start)
        test_pred_start = np.array(h5file.root.test_pred_start)

        train_pred_end = np.array(h5file.root.train_pred_end)
        valid_pred_end = np.array(h5file.root.valid_pred_end)
        test_pred_end = np.array(h5file.root.test_pred_end)

        train_dtms = np.array(h5file.root.train_dtm)
        valid_dtms = np.array(h5file.root.valid_dtm)
        test_dtms = np.array(h5file.root.test_dtm)

    h5file.close()

    return train_newid, train_start, train_end, valid_newid, valid_start, valid_end, test_newid, test_start, test_end, \
           train_pred_start, valid_pred_start, test_pred_start, train_pred_end, valid_pred_end, test_pred_end,\
           train_dtms, valid_dtms, test_dtms


def load_data(cfg, data_src, load_from_prosp_dict=False, forced_normies=None, shuffle=False):
    onehot_cols = cfg['onehot_fields']
    majority_class_rate = cfg['majority_class_rate']

    if not load_from_prosp_dict:
        h5file = tables.open_file(data_src, mode='r')

        train_data_x = np.array(h5file.root.seq_train_x)
        valid_data_x = np.array(h5file.root.seq_valid_x)
        test_data_x = np.array(h5file.root.seq_test_x)

        train_data_y = np.array(h5file.root.seq_train_y)
        valid_data_y = np.array(h5file.root.seq_valid_y)
        test_data_y = np.array(h5file.root.seq_test_y)

        print('POSITIVE CASE RATIO IN TRAINING SET:', train_data_y.sum() / len(train_data_y), 'N:', train_data_y.sum())

        x_cols = h5file.root.seq_train_x.attrs['columns']
        y_cols = h5file.root.seq_train_y.attrs['columns']
        categorical_features = h5file.root.seq_train_x.attrs['categorical_features']

        h5file.close()

    else:  # dict contains all data, just like the hdf5; possibly only test data though, it is made for prosp testing
        train_data_x = np.array(data_src['train_data_x'])
        valid_data_x = np.array(data_src['valid_data_x'])
        test_data_x = np.array(data_src['test_data_x'])

        train_data_y = np.array(data_src['train_data_y'])
        valid_data_y = np.array(data_src['valid_data_y'])
        test_data_y = np.array(data_src['test_data_y'])

        x_cols = data_src['x_cols']
        y_cols = data_src['y_cols']
        categorical_features = data_src['categorical_features']

    # # show data covariance
    # import seaborn as sn
    # cov_data = []
    # for x, nopad in zip(train_data_x, not_padding(train_data_x)):
    #     cov_data.append(x[nopad])
    # colname_map = {'age': 'Age', 'FullScore': 'MEWS', 'BPNum': 'BP', 'HRNum': 'HR', 'TmprNum': 'Tmpr', 'RespRateNum': 'RR'}
    # print('AAAAAAAAAAAAAAAAAA', cov_data[0].shape)
    # cov_data = np.transpose(np.concatenate(cov_data), (1, 0))
    # print('BBBBBBBBBBBBBBBBBB', cov_data.shape)
    # npcovmx = np.cov(cov_data, bias=False)
    # pprint(npcovmx)
    # df = pd.DataFrame({colname_map[c]: v for c, v in zip(x_cols, cov_data) if c in colname_map}, columns=[c for c in x_cols if c in colname_map])
    # print(df)
    # covmx = df.cov()
    # sn.heatmap(covmx, annot=True, fmt='g')
    # plt.show()
    # exit()

    # hard balance data
    if 'over' in cfg['task']:
        train_data_y = train_data_y.astype(np.int32)
        valid_data_y = valid_data_y.astype(np.int32)
        test_data_y = test_data_y.astype(np.int32)

        pos_indices = np.where(np.squeeze(train_data_y == 1))[0]  # [0] because of where
        neg_indices = np.where(np.squeeze(train_data_y == 0))[0]
        balanced_indices = hard_balance(pos_indices, neg_indices, max_desired_neg_ratio=majority_class_rate)
        train_data_y = train_data_y[balanced_indices]
        train_data_x = train_data_x[balanced_indices]

        # we leave the test cases alone
        print(f'{len(pos_indices) + len(neg_indices) - len(balanced_indices)} negative examples were thrown away to balance outcomes;'
              f'{len(pos_indices)} pos and {len(neg_indices)} neg originally')

    elif cfg['task'] == 'prediction' and 'FullScore' in cfg['predict']:
        fs_y_i = cfg['predict'].index('FullScore')
        pos_indices = np.where(train_data_y[:, fs_y_i] > 6.)[0]
        neg_indices = np.where(train_data_y[:, fs_y_i] < 7.)[0]
        balanced_indices = hard_balance(pos_indices, neg_indices, max_desired_neg_ratio=majority_class_rate)
        train_data_y = train_data_y[balanced_indices, :]
        train_data_x = train_data_x[balanced_indices, :]

        # we leave the test cases alone
        print(len(pos_indices) + len(neg_indices) - len(balanced_indices),
              'negative examples were thrown away to balance outcomes')

    # onehot encode input
    # is_padding_train = is_padding_valid = is_padding_test = np.array([])
    # if len(train_data_x) > 0:
    #     is_padding_train = np.all(train_data_x == 0, axis=2)
    # if len(valid_data_x) > 0:
    #     is_padding_valid = np.all(valid_data_x == 0, axis=2)
    # if len(test_data_x) > 0:
    #     is_padding_test = np.all(test_data_x == 0, axis=2)
    for onehot_col in onehot_cols:
        if onehot_col in x_cols:
            train_data_x, _ = onehot_dat_col(train_data_x, onehot_col, x_cols, categorical_features)
            valid_data_x, _ = onehot_dat_col(valid_data_x, onehot_col, x_cols, categorical_features)
            test_data_x, onehot_extra_cols = onehot_dat_col(test_data_x, onehot_col, x_cols, categorical_features)
            # if len(train_data_x) > 0:
            #     onehot_data_train, onehot_extra_cols = ONEHOT_FUNS[onehot_col](train_data_x[:, :, x_cols.index(onehot_col)])
            # if len(valid_data_x) > 0:
            #     onehot_data_valid, _ = ONEHOT_FUNS[onehot_col](valid_data_x[:, :, x_cols.index(onehot_col)])
            # if len(test_data_x) > 0:
            #     onehot_data_test, _ = ONEHOT_FUNS[onehot_col](test_data_x[:, :, x_cols.index(onehot_col)])

            # if onehot_col in categorical_features:  # then name the columns according to the categorical names
            #     onehot_extra_cols = sorted([(num_val, name) for name, num_val in categorical_features[onehot_col].items()])
            #     onehot_extra_cols = ['{}:{}'.format(name, num_val) for num_val, name in onehot_extra_cols]

            # onehot_data_train[is_padding_train, :] = 0.  # padding should stay 0
            # onehot_data_valid[is_padding_valid, :] = 0.
            # onehot_data_test[is_padding_test, :] = 0.

            # train_data_x = np.delete(train_data_x, x_cols.index(onehot_col), axis=-1)
            # valid_data_x = np.delete(valid_data_x, x_cols.index(onehot_col), axis=-1)
            # test_data_x = np.delete(test_data_x, x_cols.index(onehot_col), axis=-1)

            x_cols.remove(onehot_col)
            x_cols += onehot_extra_cols

            # train_data_x = np.append(train_data_x, onehot_data_train, axis=-1)
            # valid_data_x = np.append(valid_data_x, onehot_data_valid, axis=-1)
            # test_data_x = np.append(test_data_x, onehot_data_test, axis=-1)

    # onehot encode output
    for onehot_col in onehot_cols:  # FIXME TODO implement it like it's done for the input
        if onehot_col in y_cols:
            onehot_data_train, onehot_extra_cols = ONEHOT_FUNS[onehot_col](train_data_y[:, y_cols.index(onehot_col)])
            onehot_data_valid, _ = ONEHOT_FUNS[onehot_col](valid_data_y[:, y_cols.index(onehot_col)])
            onehot_data_test, _ = ONEHOT_FUNS[onehot_col](test_data_y[:, y_cols.index(onehot_col)])

            train_data_y = np.delete(train_data_y, y_cols.index(onehot_col), axis=-1)
            valid_data_y = np.delete(valid_data_y, y_cols.index(onehot_col), axis=-1)
            test_data_y = np.delete(test_data_y, y_cols.index(onehot_col), axis=-1)
            y_cols.remove(onehot_col)
            y_cols += onehot_extra_cols

            train_data_y = np.append(train_data_y, onehot_data_train, axis=-1)
            valid_data_y = np.append(valid_data_y, onehot_data_valid, axis=-1)
            test_data_y = np.append(test_data_y, onehot_data_test, axis=-1)

    # normalize
    columns_to_norm = ['BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'BMINum', 'age', 'AgeNum',
                       'BPScore', 'HRScore', 'TmprScore', 'RespRateScore', 'AVPUScore', 'BMIScore', 'AgeScore',
                       'FullScore', 'dfs', 'varscores']

    if forced_normies is not None:
        normies = forced_normies
    else:  # compute min-max normalization factors for this dataset
        normies = {}  # if norming happens independently, train and test can end up having different norm values
        # get min and max from all datasets and all columns - build a mapping: colname -> (min, max)
        # then apply normalization same as you did, iterating through datasets and columns
        minmaxes = {}
        for data in [train_data_x, valid_data_x, test_data_x]:  # note: dimensions of sample, sequence, then features
            if len(data) > 0:
                avoid_pad = not_padding(data)
                for i, col in enumerate(x_cols):
                    if col in columns_to_norm:
                        if col not in minmaxes:
                            minmaxes[col] = (np.min(data[avoid_pad, i]), np.max(data[avoid_pad, i]))
                        else:  # keep the min/max the min/max
                            minmaxes[col] = (min([np.min(data[avoid_pad, i]), minmaxes[col][0]]),
                                             max([np.max(data[avoid_pad, i]), minmaxes[col][1]]))

        if 'over' not in cfg['task']:  # no need to normalize the {0,1} values
            for data in [train_data_y, valid_data_y, test_data_y]:  # y_cols is the only difference
                if len(data) > 0:
                    for i, col in enumerate(y_cols):
                        if col in columns_to_norm:
                            if col not in minmaxes:
                                minmaxes[col] = (np.min(data[..., i]), np.max(data[..., i]))  # no padding avoidance necessary for y
                            else:
                                minmaxes[col] = (min([np.min(data[..., i]), minmaxes[col][0]]),
                                                 max([np.max(data[..., i]), minmaxes[col][1]]))

        # spawn normies and apply normalization
        for col, minmax in minmaxes.items():
            normies[col] = normie(minmax=minmax)

    # finally normalize
    for col, nrm in normies.items():
        if col in x_cols:
            i = x_cols.index(col)
            for data in [train_data_x, valid_data_x, test_data_x]:
                if len(data) > 0:
                    avoid_pad = not_padding(data)
                    data[avoid_pad, i] = nrm(data[avoid_pad, i])

        if 'over' not in cfg['task']:  # no need to normalize the binary output values
            if col in y_cols:
                i = y_cols.index(col)
                for data in [train_data_y, valid_data_y, test_data_y]:
                    if len(data) > 0:
                        data[..., i] = nrm(data[..., i])

    # check for nans
    isnan = [np.any(np.isnan(train_data_x)), np.any(np.isnan(train_data_y)),
             np.any(np.isnan(test_data_x)), np.any(np.isnan(test_data_y)),
             np.any(np.isnan(valid_data_x)), np.any(np.isnan(valid_data_y))]
    print('nans', isnan)
    assert not np.any(isnan)

    if shuffle:
        train_n, valid_n, test_n = len(train_data_x), len(valid_data_x), len(test_data_x)
        all_x = np.concatenate([train_data_x, valid_data_x, test_data_x])
        del train_data_x, valid_data_x, test_data_x
        all_y = np.concatenate([train_data_y, valid_data_y, test_data_y])
        del train_data_y, valid_data_y, test_data_y

        perm = np.random.permutation(train_n + valid_n + test_n)

        train_data_x, valid_data_x, test_data_x = all_x[perm[:train_n]], all_x[perm[train_n:train_n + valid_n]], \
                                                  all_x[perm[train_n + valid_n:]]
        del all_x
        train_data_y, valid_data_y, test_data_y = all_y[perm[:train_n]], all_y[perm[train_n:train_n + valid_n]], \
                                                  all_y[perm[train_n + valid_n:]]
        del all_y

    return train_data_x, train_data_y, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies


def load_ref_data_prosp(data_src):  # only test, loading from dict
    test_newid = np.array(data_src['test_newid'])
    test_start = np.array(data_src['test_start'])
    test_end = np.array(data_src['test_end'])
    test_pred_start = np.array(data_src['test_pred_start'])
    test_pred_end = np.array(data_src['test_pred_end'])
    test_dtms = np.array(data_src['test_dtm'])

    return test_newid, test_start, test_end, test_pred_start, test_pred_end, test_dtms


# train model
def train(model, cfg, nepochs, patience, train_data_x, train_data_y, test_data_x, test_data_y, x_cols, y_cols, model_path='models'):
    with tf.device('/device:GPU:0'):
        negloglik = lambda y, rv_y: -rv_y.log_prob(y)
        loss = negloglik
        if cfg['type'] == 'regression' and cfg['task'] == 'prediction':
            loss = 'mae'
        elif cfg['type'] == 'regression' and 'over' in cfg['task']:
            loss = 'binary_crossentropy'

        model.compile(optimizer=tf.optimizers.Adam(lr=cfg['lr']), loss=loss)
        # update_name(model, 0)
        print(model.summary())
        # plot_model(model, to_file='img/MODEL_{}.png'.format(model_name(cfg)))

        weight_fname = f'{model_path}/{model_name(cfg)}.h5'
        log_dir = 'logs/log_{}'.format(model_name(cfg))
        # tb = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)  # TODO bugs out in non-eager
        es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=patience*2, restore_best_weights=True)
        lr_reduce = keras.callbacks.ReduceLROnPlateau(factor=0.1, patience=patience, verbose=1)
        chkpointing = keras.callbacks.ModelCheckpoint(weight_fname, monitor='val_loss', verbose=0, save_best_only=True,
                                                      save_weights_only=True, mode='auto')

        model.fit(train_data_x, train_data_y, validation_data=(test_data_x, test_data_y), batch_size=cfg['batch_size'],
                  epochs=nepochs, validation_freq=1, callbacks=[lr_reduce, es, chkpointing],
                  class_weight=cfg['class_weight'], shuffle=True)

    return model


def plot_roc(fpr, tpr, thresholds, model_idx=None, n=None, nneg=None, figsize=(8, 8), ax=None):
    roc_auc = auc(fpr, tpr)
    print('ROC AUC:', roc_auc)

    plt.style.use('seaborn-white')
    plt.rc('font', size=13)
    fig = None
    if ax is None:
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(1, 1, 1)
    ax.plot([0, 100], [0, 100], '--', alpha=0.3, color='grey', zorder=8)
    ax.plot(fpr * 100, tpr * 100, 'C3', zorder=8)
    ax.fill_between(fpr*100, fpr*100, tpr*100, facecolor='C3', alpha=0.04, zorder=5)

    ax.set_xticks(np.arange(0, 101, 10))
    ax.set_yticks(np.arange(0, 101, 10))
    ax.set_xlim(-5, 101)
    ax.set_ylim(-5, 101)

    # if roc_auc:
    #     ax.set_title('AUC of {}'.format(roc_auc))
    ax.set_xlabel('1 - specificity (%)')
    ax.set_ylabel('Sensitivity (%)')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    if model_idx is not None:
        # model points
        for m_fp, m_tp, c in zip(fpr[model_idx], tpr[model_idx], ['α', 'β', 'γ']):
            ax.scatter(m_fp*100, m_tp*100, c='C3', marker='o', s=72,
                       label='{}: {} FP out of 10k nights'.format(c, int(round(m_fp*100))), zorder=10)
            ax.text(m_fp * 100 + 2, m_tp * 100 + 2, c)
        ax.scatter([0], [0], c='C4', marker='P', s=110, label='Current clinical practice', zorder=10)

        # clinically applicable
        # clinically applicable FNrate != clinically applicable 1/10k FN (FP here)
        clinically_applicable = 2/10000 * n / nneg * 100  # 2/10k*n in total, then divide by # of negative to get fprate
        ax.add_patch(Rectangle((-10, -10), clinically_applicable + 10, 105 + 10, alpha=0.15, facecolor='C2', zorder=1,
                               label='Clinically applicable'))

    return fig


def plot_pr(output, ground_truth, mask=None, title=''):
    output = output[mask > 0] if mask is not None else output
    ground_truth = ground_truth[mask > 0] if mask is not None else ground_truth

    prec, rec, ths = precision_recall_curve(ground_truth, output)
    auc_val = auc(rec, prec)
    fig = plt.figure()
    rand_perf = (ground_truth == 1).sum() / len(ground_truth)
    plt.plot([0, 1], [rand_perf, rand_perf], color='gray', linestyle='--', alpha=0.5)
    plt.plot(rec, prec, label=f'ROC (area = {auc_val})')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.legend(loc='lower right')
    plt.title(f'AUC={auc_val}_' + title)
    print(f'AUC: {auc_val}')

    return auc, fig


def plot_tn_vs_fn(n, tns, fns, thresholds, model_idx, logfile, overwrite_results=True, axes=None, legend=False):  # tns ~= tpsinv
    plt.style.use('seaborn-white')
    plt.rc('font', size=13)

    # zoomed out plot
    fig = None
    if axes is None:
        fig = plt.figure(figsize=(8, 8))  # use to be 6.2
        ax = fig.add_subplot(1, 1, 1)
        # ax = fig.add_subplot(1, 2, 1)
    else:
        ax = axes[0]
    ax.plot(fns/n*10000, tns/n*10000, 'C3', zorder=10)
    ax.plot([0, np.max(fns/n*10000)], [0, np.max(tns/n*10000)], '--', color='grey', alpha=0.3, zorder=10)
    ax.fill_between(fns/n*10000, fns/n*10000 / np.max(fns/n*10000) * np.max(tns/n*10000), tns/n*10000,
                    facecolor='C3', alpha=0.04, zorder=5)

    # fnr and tnr calc for the model thresholds
    model_fns = fns[model_idx]
    fn_range = np.array([-0.12, model_fns[-1] + model_fns[-1] * 0.5])

    ax.set_xlabel('Unstable sleeping (per 10k nights)', labelpad=12)
    ax.set_ylabel('Stable sleeping (per 10k nights)', labelpad=12)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.set_ylim(-300, 10000)
    ax.set_xlim(-4, np.max(fns)/n*10000)
    ax.set_yticks(np.arange(0, 10001, 1000))

    # model points
    for m_fn, m_tn, c in zip(model_fns, tns[model_idx], ['α', 'β', 'γ']):
        ax.scatter(m_fn/n*10000, m_tn/n*10000, c='C3', marker='o', s=72,
                   label='{}: {} FP out of 10k nights'.format(c, int(round(m_fn/n*10000))), zorder=10)
        ax.text(m_fn/n*10000 + 3.0, m_tn/n*10000 + 180, c)
    ax.scatter([0], [0], c='C4', marker='P', s=110, label='Current clinical practice', zorder=10)

    # clinically applicable area
    clinically_applicable = 2.  # out of 10k
    ax.add_patch(Rectangle((-10, -300), clinically_applicable + 10.01, 10000 + 300, alpha=0.15, facecolor='C2',
                           label='Clinically applicable'))

    # zoomed in plot
    if axes is None:
        fig2 = plt.figure(figsize=(8, 8))
        ax = fig2.add_subplot(1, 1, 1)
        # ax = fig.add_subplot(1, 2, 2)
    else:
        ax = axes[1]

    ax.plot(fns/n*10000, tns/n*10000, 'C3', zorder=10)
    ax.plot([0, np.max(fns / n * 10000)], [0, np.max(tns / n * 10000)], '--', color='grey', alpha=0.3, zorder=10)
    ax.fill_between(fns / n * 10000, fns / n * 10000 / np.max(fns / n * 10000) * np.max(tns / n * 10000), tns / n * 10000,
                    facecolor='C3', alpha=0.04, zorder=5)
    ax.set_xlim(fn_range[0], max(fn_range[1]/n*10000, 3.0))  # katyvasz
    ax.set_ylim(-360, 10000)
    ax.set_yticks(np.arange(0, 10001, 1000))

    # model points
    ks = [0.5, 1., 2.]
    for m_fn, m_tn, c, k in zip(model_fns, tns[model_idx], ['α', 'β', 'γ'], ks):
        # k = m_fn/n*10000
        out_of = 10 if k > 0.9 else 20
        k = int(round(k)) if k > 0.9 else int(round(k * 2))
        ax.scatter(m_fn/n*10000, m_tn/n*10000, c='C3', marker='o', s=72,
                   label='{}: {} FP out of {}k'.format(c, k, out_of), zorder=10)
        ax.text(m_fn / n * 10000 + 0.04, m_tn / n * 10000 - 500, c)
    ax.scatter([0], [0], c='C4', marker='P', s=110, label='Current clinical practice', zorder=10)

    # clinically applicable area
    clinically_applicable = 2.  # out of 10k
    ax.add_patch(Rectangle((-10, -360), clinically_applicable + 10.01, 10000 + 360, alpha=0.15, facecolor='C2',
                           label='Clinically applicable'))

    ax.set_xlabel('Unstable sleeping (per 10k nights)', labelpad=12)
    ax.set_ylabel('Stable sleeping (per 10k nights)', labelpad=12)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # legend order update
    if legend:
        handles, labels = ax.get_legend_handles_labels()
        legened_order = [1, 2, 3, 4, 0]
        plt.legend([handles[i] for i in legened_order], [labels[i] for i in legened_order],
                   loc='best', framealpha=0.8, frameon=True, ncol=1)

    # logging
    s = 'TN VS FN PLOT DATA: model_fn_ratio {}; tns {}; fns {}; thresholds {}\n'.format(fns[model_idx], tns[model_idx],
                                                                                        model_fns, thresholds[model_idx])
    s += 'TN VS FN PLOT %DATA: model_fn_ratio {}; tns {}; fns {}; thresholds {}\n'.format(fns[model_idx] * 100,
                                                                                          tns[model_idx] / n * 100,
                                                                                          model_fns / n * 100,
                                                                                          thresholds[model_idx])
    s += 'TN VS FN PLOT 1/10k: model_fn_ratio {}; tns {}; fns {}; thresholds {}\n'.format(fns[model_idx] * 10000,
                                                                                          tns[model_idx] / n * 10000,
                                                                                          model_fns / n * 10000,
                                                                                          thresholds[model_idx])
    print(s)
    if overwrite_results:
        logfile.write(s)

    return fig


def predict(model, cfg, test_data_x, test_data_y, max_batch_size=256, nsample=100):
    batch_size = min(cfg['batch_size'], max_batch_size)
    preds = model.predict(test_data_x, batch_size=batch_size)
    preds = np.squeeze(np.concatenate(preds, axis=0))
    desired = np.squeeze(test_data_y).astype(np.int32)

    if not isinstance(preds, Iterable) or len(preds.shape) == 0:  # ndarray of a single element, that's not iterable; bs
        preds = np.array([preds])
        desired = np.array([desired])

    return preds, desired


def clin_appl_idx(fps, n, fp_ratio_clin_appl):
    return np.argmin(np.abs(fps/n - fp_ratio_clin_appl))


def clin_appl_auc(fps, n, fpr, tpr, fp_ratio_clin_appl):
    model_idx = clin_appl_idx(fps, n, fp_ratio_clin_appl)
    if len(fpr[:model_idx]) < 2:
        return 0.
    return auc(fpr[:model_idx], tpr[:model_idx])


def test_sleeping_prosp_linear(model, cfg, cfg_prosp, x_cols, y_cols, normies, folder, overwrite_results, thresholds_from_retro,
                               base_folder_prospective, data_size, mews_prospective):
    # this shit just got complicated because we swap the positives with the negatives
    # so TP becomes the correctly let sleep nights, and FP the incorrectly let sleep unstable nights,
    # so everything gets inverted: true negative == true positive inv, etc.

    prosp_str = '_lin_prosp'
    threshold = thresholds_from_retro[-1]  # 0.01618314

    h5fname = base_folder_prospective + data_file_name(cfg_prosp, data_size)
    with open(h5fname + '_NIGHTS.pickle', 'rb') as f:
        NIGHTS = pickle.load(f)

    r = 0
    all_preds = []
    all_desired = []
    while True:
        print('------- ROUND {} -------'.format(r))
        print('CREATING PROSPECTIVE DATA')
        SAMPLES = create_seq_prosp(cfg_prosp, mews_prospective, NIGHTS, data_size, base_folder_prospective)
        print('SAMPLES size:', len(SAMPLES['test_data_y']))
        if len(SAMPLES['test_data_y']) == 0:
            break

        print('LOAD PROSPECTIVE DATA')
        train_data_x, train_data_y, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies = \
            load_data(cfg, SAMPLES, load_from_prosp_dict=True, forced_normies=normies)  # normies from the training set!
        test_newid, test_start, test_end, test_pred_start, test_pred_end, test_dtms = load_ref_data_prosp(SAMPLES)

        # predict
        flat_test_data_x = test_data_x[:, -1, :]
        flat_test_data_y = test_data_y.ravel()
        preds, desired = model.predict_proba(flat_test_data_x)[..., 1], flat_test_data_y
        all_preds.append(preds)
        all_desired.append(desired)
        sleepers = preds < threshold
        print('PREDS: {}% sleep ({})'.format(sleepers.mean() * 100, sleepers.sum()))

        # update NIGHTS flags  # TODO assume nothing is deleted right before the night that we would use anyways for lin
        # for pred, newid, pred_start, pred_end in zip(preds, test_newid, test_pred_start, test_pred_end):  # FIXME why
        #     newid = newid.decode('ascii')
        #     if pred < threshold:  # let sleep
        #         for i in range(len(NIGHTS[newid])):
        #             if NIGHTS[newid][i][2] and date_overlap(pd.Timestamp(pred_start), pd.Timestamp(pred_end),
        #                                                     NIGHTS[newid][i][0], NIGHTS[newid][i][1]):
        #                 NIGHTS[newid][i][3] = True  # let sleep flag

        r += 1

    preds = np.concatenate(all_preds)
    desired = np.concatenate(all_desired)

    # prediction
    # each separate model (and its threshold) would result in a different ROC
    #   as the predictions likely change given that different nights are removed:
    #   the higher the threshold, the more nights are removed
    # we play safe and predict according to the highest threshold, then put the other models
    #   with their corresponding thresholds on the same ROC curve
    # for threshold_from_retro in thresholds_from_retro:
    # preds, desired = predict_prosp(model, cfg, test_data_x, test_data_y,
    #                                test_newid, test_pred_start, test_pred_end, test_dtms, thresholds_from_retro[-1])
    n = len(preds)

    # compute tpr and fpr, then inverted tpr fpr, which is the same as tnr and fnr technically
    #   but the threshold vector is different sized
    fpr, tpr, thresholds = roc_curve(desired, preds)
    roc_auc = auc(fpr, tpr)
    fprinv, tprinv, thresholdsinv = roc_curve(1 - desired, 1 - preds)
    roc_auc_inv = auc(fprinv, tprinv)

    tpsinv = np.zeros(len(thresholdsinv))
    fpsinv = np.zeros(len(thresholdsinv))
    for i, th in enumerate(thresholdsinv):
        tpsinv[i] = np.sum(((1 - preds) > th) & ((1 - desired) == 1))
        fpsinv[i] = np.sum(((1 - preds) > th) & ((1 - desired) == 0))

    # look out, fpsinv/n != fprinv; fprinv is normalized by the number of fp, not by n (all)
    # fpsinv and fprinv still have to have an index correspondence
    clin_auc = clin_appl_auc(fpsinv, n, fprinv, tprinv, 2 / 10000)
    # spcial measure: tprinv at the edge of the clinically applicable
    clin_idx = clin_appl_idx(fpsinv, n, 2 / 10000)
    tprinv_at_clin_edge = tprinv[clin_idx]

    s = '# of all cases: {}'.format(n)
    print(s)
    if overwrite_results:
        logfile = open('{}log{}.txt'.format(folder, prosp_str), 'wt')
        logfile.write('AUC: {}\n'.format(clin_auc))  # inverted and clinically applicable
        logfile.write('TP@CLIN: {}\n'.format(tprinv_at_clin_edge))
        logfile.write('AUC inverted: {}\n'.format(roc_auc_inv))
        logfile.write('AUC for real: {}\n'.format(roc_auc))
        logfile.write(s + '\n')

    # just to have them
    if overwrite_results:
        df = {'tpsinv': tpsinv, 'fpsinv': fpsinv, 'thresholdsinv': thresholdsinv, 'tprinv': tprinv, 'fprinv': fprinv,
              'predsinv': 1 - preds, 'desiredinv': 1 - desired, 'tprinv@clin': tprinv_at_clin_edge,
              'aucinv': roc_auc_inv, 'clinaucinv': clin_auc}

        with open('{}test{}.pickle'.format(folder, prosp_str), 'wb') as f:
            pickle.dump(df, f)

        # save predictions and desired values
        with open('{}lin_prosp_preds_desired.pickle'.format(folder), 'wb') as f:
            pickle.dump({'preds': preds, 'desired': desired}, f)

    # FIXME prosp plotting is screwed
    # compute model indices according to desired FN rate so we know where to plot those models
    model_idx = np.array([np.argmin(np.abs(thresholdsinv - th)) for th in 1. - thresholds_from_retro])  # prospective style
    nneginv = (desired == 1).sum()

    # plot roc
    fig = plot_roc(fprinv, tprinv, thresholdsinv, model_idx, n, nneginv)
    if overwrite_results:
        fig.savefig('{}auc{}.png'.format(folder, prosp_str))

    # plot tn vs fn, close log, save predictions and such for later plots (plot_test.py)
    fig = plot_tn_vs_fn(n, tpsinv, fpsinv, thresholdsinv, model_idx, logfile)
    if overwrite_results:
        fig.savefig('{}not_auc{}.png'.format(folder, prosp_str))

    if overwrite_results:
        logfile.close()

    return preds, desired


def test_sleeping_prosp(model, cfg, cfg_prosp, x_cols, y_cols, normies, folder, overwrite_results, thresholds_from_retro,
                        base_folder_prospective, data_size, mews_prospective):
    # this shit just got complicated because we swap the positives with the negatives
    # so TP becomes the correctly let sleep nights, and FP the incorrectly let sleep unstable nights,
    # so everything gets inverted: true negative == true positive inv, etc.

    prosp_str = '_prosp'
    threshold = thresholds_from_retro[-1]  # 0.01618314

    h5fname = base_folder_prospective + data_file_name(cfg_prosp, data_size)
    with open(h5fname + '_NIGHTS.pickle', 'rb') as f:
        NIGHTS = pickle.load(f)

    r = 0
    all_preds = []
    all_desired = []
    while True:
        print('------- ROUND {} -------'.format(r))
        print('CREATING PROSPECTIVE DATA')
        SAMPLES = create_seq_prosp(cfg_prosp, mews_prospective, NIGHTS, data_size, base_folder_prospective)
        print('SAMPLES size:', len(SAMPLES['test_data_y']))
        if len(SAMPLES['test_data_y']) == 0:
            break

        print('LOAD PROSPECTIVE DATA')
        train_data_x, train_data_y, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies = \
            load_data(cfg, SAMPLES, load_from_prosp_dict=True, forced_normies=normies)  # normies from the training set!
        test_newid, test_start, test_end, test_pred_start, test_pred_end, test_dtms = load_ref_data_prosp(SAMPLES)



        # predict
        preds, desired = predict(model, cfg_prosp, test_data_x, test_data_y)
        all_preds.append(preds)
        all_desired.append(desired)
        sleepers = preds < threshold
        print('PREDS: {}% sleep ({})'.format(sleepers.mean() * 100, sleepers.sum()))

        # update NIGHTS flags
        for pred, newid, pred_start, pred_end in zip(preds, test_newid, test_pred_start, test_pred_end):  # FIXME why
            newid = newid.decode('ascii')
            if pred < threshold:  # let sleep
                for i in range(len(NIGHTS[newid])):
                    if NIGHTS[newid][i][2] and date_overlap(pd.Timestamp(pred_start), pd.Timestamp(pred_end),
                                                            NIGHTS[newid][i][0], NIGHTS[newid][i][1]):
                        NIGHTS[newid][i][3] = True  # let sleep flag

        r += 1

    preds = np.concatenate(all_preds)
    desired = np.concatenate(all_desired)

    # prediction
    # each separate model (and its threshold) would result in a different ROC
    #   as the predictions likely change given that different nights are removed:
    #   the higher the threshold, the more nights are removed
    # we play safe and predict according to the highest threshold, then put the other models
    #   with their corresponding thresholds on the same ROC curve
    # for threshold_from_retro in thresholds_from_retro:
    # preds, desired = predict_prosp(model, cfg, test_data_x, test_data_y,
    #                                test_newid, test_pred_start, test_pred_end, test_dtms, thresholds_from_retro[-1])
    n = len(preds)

    # compute tpr and fpr, then inverted tpr fpr, which is the same as tnr and fnr technically
    #   but the threshold vector is different sized
    fpr, tpr, thresholds = roc_curve(desired, preds)
    roc_auc = auc(fpr, tpr)
    fprinv, tprinv, thresholdsinv = roc_curve(1 - desired, 1 - preds)
    roc_auc_inv = auc(fprinv, tprinv)

    tpsinv = np.zeros(len(thresholdsinv))
    fpsinv = np.zeros(len(thresholdsinv))
    for i, th in enumerate(thresholdsinv):
        tpsinv[i] = np.sum(((1 - preds) > th) & ((1 - desired) == 1))
        fpsinv[i] = np.sum(((1 - preds) > th) & ((1 - desired) == 0))

    # look out, fpsinv/n != fprinv; fprinv is normalized by the number of fp, not by n (all)
    # fpsinv and fprinv still have to have an index correspondence
    clin_auc = clin_appl_auc(fpsinv, n, fprinv, tprinv, 2/10000)
    # spcial measure: tprinv at the edge of the clinically applicable
    clin_idx = clin_appl_idx(fpsinv, n, 2/10000)
    tprinv_at_clin_edge = tprinv[clin_idx]

    s = '# of all cases: {}'.format(n)
    print(s)
    if overwrite_results:
        logfile = open('{}log{}.txt'.format(folder, prosp_str), 'wt')
        logfile.write('AUC: {}\n'.format(clin_auc))  # inverted and clinically applicable
        logfile.write('TP@CLIN: {}\n'.format(tprinv_at_clin_edge))
        logfile.write('AUC inverted: {}\n'.format(roc_auc_inv))
        logfile.write('AUC for real: {}\n'.format(roc_auc))
        logfile.write(s + '\n')

    # just to have them
    if overwrite_results:
        df = {'tpsinv': tpsinv, 'fpsinv': fpsinv, 'thresholdsinv': thresholdsinv, 'tprinv': tprinv, 'fprinv': fprinv,
              'predsinv': 1 - preds, 'desiredinv': 1 - desired, 'tprinv@clin': tprinv_at_clin_edge,
              'aucinv': roc_auc_inv, 'clinaucinv': clin_auc}

        with open('{}test{}.pickle'.format(folder, prosp_str), 'wb') as f:
            pickle.dump(df, f)

        # save predictions and desired values
        with open('{}prosp_preds_desired.pickle'.format(folder), 'wb') as f:
            pickle.dump({'preds': preds, 'desired': desired}, f)

    # FIXME prosp plotting is screwed
    # compute model indices according to desired FN rate so we know where to plot those models
    model_idx = np.array([np.argmin(np.abs(thresholdsinv - th)) for th in 1. - thresholds_from_retro])  # prospective style
    nneginv = (desired == 1).sum()

    # plot roc
    fig = plot_roc(fprinv, tprinv, thresholdsinv, model_idx, n, nneginv)
    if overwrite_results:
        fig.savefig('{}auc{}.png'.format(folder, prosp_str))

    # plot tn vs fn, close log, save predictions and such for later plots (plot_test.py)
    fig = plot_tn_vs_fn(n, tpsinv, fpsinv, thresholdsinv, model_idx, logfile)
    if overwrite_results:
        fig.savefig('{}not_auc{}.png'.format(folder, prosp_str))

    if overwrite_results:
        logfile.close()

    return preds, desired


def test_sleeping(model, cfg, test_data_x, test_data_y, x_cols, y_cols, normies, folder, overwrite_results, linear_stuff=None):
    # this shit just got complicated because we swap the positives with the negatives
    # so everything gets inverted: true negative == true positive inv, etc.

    logfile = 'ignore_me.txt'
    if overwrite_results:
        logfile = open('{}/log.txt'.format(folder), 'wt')

    # linear model testing
    lin_auc, lin_clin_auc, lin_tprinv_at_clin_edge, lin_thresholds = 0, 0, 0, np.zeros(3)
    if linear_stuff is not None:
        lr_model, flat_test_data_x, flat_test_data_y = linear_stuff['model'], linear_stuff['flat_x'], linear_stuff['flat_y']
        # gather data for special ROC plots
        lr_preds = lr_model.predict_proba(flat_test_data_x)[..., 1]  # take the prob prediction for positive class
        n = len(lr_preds)
        nneginv = (flat_test_data_y == 1).sum()
        fprinv, tprinv, thresholdsinv = roc_curve(1 - flat_test_data_y, 1 - lr_preds)
        tpsinv, fpsinv = np.zeros(len(thresholdsinv)), np.zeros(len(thresholdsinv))
        for i, th in enumerate(thresholdsinv):
            tpsinv[i] = np.sum(((1 - lr_preds) > th) & ((1 - flat_test_data_y) == 1))
            fpsinv[i] = np.sum(((1 - lr_preds) > th) & ((1 - flat_test_data_y) == 0))

        lin_auc = roc_auc_score(1 - flat_test_data_y, 1 - lr_preds)
        lin_clin_auc = clin_appl_auc(fpsinv, n, fprinv, tprinv, 2 / 10000)
        clin_idx = clin_appl_idx(fpsinv, n, 2 / 10000)
        lin_tprinv_at_clin_edge = tprinv[clin_idx]

        model_fn_ratios = np.array([1 / 20000, 1 / 10000, 2 / 10000])
        model_idx = np.array([clin_appl_idx(fpsinv, n, r) for r in model_fn_ratios])  # has to be fpsinv/n

        f1 = plot_roc(fprinv, tprinv, thresholdsinv, model_idx, n, nneginv)
        plt.title(f'ROC INV linear model, AUC={auc(fprinv, tprinv)}')
        auc_val, f2 = plot_pr(1 - lr_preds, 1 - test_data_y, title=f'PR INV linear model\nclin_auc={lin_clin_auc}; '
        f'tprinv_at_clin_edge: {lin_tprinv_at_clin_edge}')
        print(f'PR INV AUC: {auc_val}')

        fpr, tpr, thresholdsinv = roc_curve(flat_test_data_y, lr_preds)
        f3 = plot_roc(fpr, tpr, thresholdsinv)
        plt.title(f'ROC linear model, AUC={auc(fpr, tpr)}')
        auc_val, f4 = plot_pr(lr_preds, flat_test_data_y, title='PR linear model')
        print(f'PR AUC: {auc_val}')

        for i, f in enumerate([f1, f2, f3, f4]):
            f.savefig(f'{folder}/linear_model_{i}.png')

        # plot tn vs fn, close log, save predictions and such for later plots (plot_test.py)
        if overwrite_results:
            df = {'tpsinv': tpsinv, 'fpsinv': fpsinv, 'thresholdsinv': thresholdsinv, 'tprinv': tprinv,
                  'fprinv': fprinv, 'predsinv': 1 - lr_preds, 'desiredinv': 1 - flat_test_data_y,
                  'tprinv@clin': lin_tprinv_at_clin_edge, 'aucinv': lin_auc, 'clinaucinv': lin_clin_auc}

            with open('{}test_lin.pickle'.format(folder), 'wb') as f:
                pickle.dump(df, f)

        lin_thresholds = 1. - np.array([thresholdsinv[i] for i in model_idx])

    # RNN stuff
    # prediction
    preds, desired = predict(model, cfg, test_data_x, test_data_y)
    n = len(preds)

    # compute tpr and fpr, then inverted tpr fpr, which is the same as tnr and fnr technically
    #   but the threshold vector is different sized
    fpr, tpr, thresholds = roc_curve(desired, preds)
    roc_auc = auc(fpr, tpr)
    fprinv, tprinv, thresholdsinv = roc_curve(1 - desired, 1 - preds)
    roc_auc_inv = auc(fprinv, tprinv)

    tpsinv, fpsinv = np.zeros(len(thresholdsinv)), np.zeros(len(thresholdsinv))
    for i, th in enumerate(thresholdsinv):
        tpsinv[i] = np.sum(((1 - preds) > th) & ((1 - desired) == 1))
        fpsinv[i] = np.sum(((1 - preds) > th) & ((1 - desired) == 0))

    tns, fns = np.zeros(len(thresholds)), np.zeros(len(thresholds))
    for i, th in enumerate(thresholds):
        tns[i] = np.sum((preds < th) & (desired == 0))
        fns[i] = np.sum((preds < th) & (desired == 1))

    # look out, fpsinv/n != fprinv; fprinv is normalized by the number of fp, not by n (all)
    # fpsinv and fprinv still have to have an index correspondence
    clin_auc = clin_appl_auc(fpsinv, n, fprinv, tprinv, 2/10000)
    # spcial measure: tprinv at the edge of the clinically applicable
    clin_idx = clin_appl_idx(fpsinv, n, 2/10000)
    tprinv_at_clin_edge = tprinv[clin_idx]

    s = '# of all cases: {}'.format(n)
    print(s)
    if overwrite_results:
        logfile.write('AUC: {}\n'.format(clin_auc))  # inverted and clinically applicable
        logfile.write('TP@CLIN: {}\n'.format(tprinv_at_clin_edge))
        logfile.write('AUC inverted: {}\n'.format(roc_auc_inv))
        logfile.write('AUC for real: {}\n'.format(roc_auc))
        logfile.write('LINEAR AUC inverted: {}\n'.format(lin_clin_auc))
        logfile.write('LINEAR OVERALL AUC inverted: {}\n'.format(lin_auc))
        logfile.write('LINEAR TP@CLIN inverted: {}\n'.format(lin_tprinv_at_clin_edge))
        logfile.write(s + '\n')

    # confusion matrices
    def conf_mx_printout(preds, desired, th):
        predicted = (preds > th).astype(np.int32)
        cm = confusion_matrix(desired, predicted)
        cm_perc = cm / len(preds)
        return 'confusion matrix inv at {}\ntn,fp\nfn,tp\n{}\n{}\n'.format(th, cm, cm_perc)

    # important point
    mth = 1 - 0.015  # due to positive to negative inversion
    s = conf_mx_printout(1 - preds, 1 - desired, mth)
    print(s)
    if overwrite_results:
        logfile.write(s + '\n')
    for th in range(1, 20):
        s = conf_mx_printout(1 - preds, 1 - desired, 1 - th * 0.001)
        print(s)
        if overwrite_results:
            logfile.write(s + '\n')
    s = conf_mx_printout(preds, desired, 0.25)
    print(s)
    if overwrite_results:
        logfile.write(s + '\n')

    # compute model indices according to desired FN rate so we know where to plot those models
    model_fn_ratios = np.array([1 / 20000, 1 / 10000, 2 / 10000])
    model_idx = np.array([clin_appl_idx(fpsinv, n, r) for r in model_fn_ratios])  # has to be fpsinv/n
    model_idx_not_inv = np.array([clin_appl_idx(fns, n, r) for r in model_fn_ratios])  # inverse of the above

    if overwrite_results:
        for mi, mi_not_inv, submodel in zip(model_idx, model_idx_not_inv, ['alpha', 'beta', 'gamma']):
            logfile.write('F1 inverted of {}: {}\n'.format(submodel, f1_score(1 - desired, 1 - preds > thresholdsinv[mi])))
            logfile.write('F1 for real of {}: {}\n'.format(submodel, f1_score(desired, preds > thresholds[mi_not_inv])))
            logfile.write('MCC inverted of {}: {}\n'.format(submodel, matthews_corrcoef(1 - desired, 1 - preds > thresholdsinv[mi])))
            logfile.write('MCC for real of {}: {}\n'.format(submodel, matthews_corrcoef(desired, preds > thresholds[mi_not_inv])))

    # plot roc
    nneginv = (desired == 1).sum()
    fig = plot_roc(fprinv, tprinv, thresholdsinv, model_idx, n, nneginv)
    if overwrite_results:
        fig.savefig('{}roc.png'.format(folder))

    prauc, fig = plot_pr(1 - preds, 1 - desired, title=f'PR w/ positive case being the '
    f'{"majority" if np.sum(1 - desired) > len(desired) / 2 else "minority"}')
    if overwrite_results:
        fig.savefig('{}pr_inv.png'.format(folder))

    prauc, fig = plot_pr(preds, desired, title=f'PR w/ positive case being the '
    f'{"majority" if np.sum(desired) > len(desired) / 2 else "minority"}')
    if overwrite_results:
        fig.savefig('{}pr.png'.format(folder))

    # plot tn vs fn, close log, save predictions and such for later plots (plot_test.py)
    final_thresholds = 1. - np.array([thresholdsinv[i] for i in model_idx])
    if overwrite_results:
        fig = plot_tn_vs_fn(n, tpsinv, fpsinv, thresholdsinv, model_idx, logfile)
        fig.savefig('{}not_auc.png'.format(folder))

        logfile.write(f'THRESHOLDS: {final_thresholds}')
        logfile.write(f'LINEAR THRESHOLDS: {lin_thresholds}')
        logfile.close()

        df = {'tpsinv': tpsinv, 'fpsinv': fpsinv, 'thresholdsinv': thresholdsinv, 'tprinv': tprinv, 'fprinv': fprinv,
              'predsinv': 1 - preds, 'desiredinv': 1 - desired, 'tprinv@clin': tprinv_at_clin_edge,
              'aucinv': roc_auc_inv, 'clinaucinv': clin_auc}

        with open('{}test.pickle'.format(folder), 'wb') as f:
            pickle.dump(df, f)

    return final_thresholds, lin_thresholds  # inverse back


def test_prediction_error_fullscore(model, cfg, test_data_x, test_data_y, x_cols, y_cols, normies, folder):

    # get means of preds, reverse norm, round them to nearest integer
    fs_y_i = y_cols.index('FullScore')
    td_i = x_cols.index('td')
    batch_size = cfg['batch_size']
    nsplit = int(len(test_data_x) / batch_size)
    splits = np.split(test_data_x[:int(nsplit * batch_size)], nsplit)
    preds_mean = []
    preds_std = []
    for split in splits:
        pred = model(split)
        preds_mean.append(pred.mean())
        preds_std.append(pred.stddev())

    # test_x_for_preds = test_data_x[:len(preds_mean)]
    preds_mean = np.squeeze(normies['FullScore'].reverse(np.concatenate(preds_mean, axis=0))[:, fs_y_i])
    preds_std = np.squeeze(normies['FullScore'].reverse(np.concatenate(preds_std, axis=0))[:, fs_y_i])
    desired = np.squeeze(np.round(normies['FullScore'].reverse(test_data_y[:len(preds_mean)])).astype(np.int32))
    round_pred = np.round(preds_mean).astype(np.int32)
    intervals = np.array([[a, b] for a, b in zip(np.arange(-0.5, 15.1, 1.), np.arange(0.5, 15.6, 1.))])
    abs_err = np.abs(desired - round_pred)

    # compute certainty over the interval that holds the desired value
    certainty = np.zeros(len(preds_mean))
    for i, (mean, std, rpred) in enumerate(zip(preds_mean, preds_std, round_pred)):
        certainty[i] = norm.cdf(intervals[rpred][1], mean, std) - norm.cdf(intervals[rpred][0], mean, std)

    # plot err vs certainty
    pd.DataFrame(np.array([certainty, abs_err]).T, columns=['Certainty', 'Error']) \
        .boxplot(by='Error', figsize=(12, 10), fontsize='large')
    plt.savefig(folder + 'fs_certainty_by_err.png', dpi=300)

    # plot td vs certainty and error
    td = test_data_x[:len(preds_mean), -1, td_i]

    plt.figure(figsize=(12, 10))
    pd.DataFrame(np.array([td, abs_err]).T, columns=['Time gap', 'Error']) \
        .boxplot(by='Error', figsize=(12, 10), fontsize='large')
    # plt.scatter(td, abs_err, s=0.8)
    # plt.xlabel('Time gap (h)')
    # plt.ylabel('Absolute error')
    plt.savefig(folder + 'fs_err_by_td.png', dpi=300)

    plt.figure(figsize=(12, 10))
    pd.DataFrame(np.array([preds_std, abs_err]).T, columns=['OverallCertainty', 'Error']) \
        .boxplot(by='Error', figsize=(12, 10), fontsize='large')
    # plt.scatter(td, abs_err, s=0.8)
    # plt.xlabel('Time gap (h)')
    # plt.ylabel('Absolute error')
    plt.savefig(folder + 'fs_err_by_ovcer.png', dpi=300)

    plt.figure(figsize=(12, 10))
    plt.scatter(td, certainty, s=0.8)
    z = np.polyfit(td, certainty, 1)
    p = np.poly1d(z)
    plt.plot(td, p(td), 'r--')
    plt.xlabel('Time gap (h)')
    plt.ylabel('Certainty')
    plt.savefig(folder + 'fs_certainty_by_td.png', dpi=300)


def test_err_distr(model, cfg, test_data_x, test_data_y, x_cols, y_cols, normies, folder):
    # TODO update w/ general version of test_prediction_error_fullscore
    # split so bigger test sets don't overflow the gpu
    batch_size = cfg['batch_size']
    nsplit = int(len(test_data_x) / batch_size)
    splits = np.split(test_data_x[:int(nsplit * batch_size)], nsplit)
    preds = []
    for split in splits:
        pred = model(split)
        if cfg['type'] != 'regression':
            pred = pred.mean()
        preds.append(pred)

    preds = np.concatenate(preds, axis=0)
    test_x_for_preds = test_data_x[:len(preds)]
    desired = test_data_y[:len(preds)]
    if y_cols[0] in normies:
        preds = normies[y_cols[0]].reverse(preds)  # TODO put reverse normal and reverse onehot into one function
        desired = normies[y_cols[0]].reverse(desired)
    err = preds - desired
    abserr = np.abs(err)

    # boxplot
    bins = np.arange(0, 24, 1)
    td_index = x_cols.index('td')
    binned_dt = np.digitize(np.expand_dims(test_x_for_preds[:, -1, td_index], -1), bins=bins, right=False)

    df = pd.DataFrame(np.concatenate([desired, binned_dt, abserr, err], axis=1), columns=['mews', 'dt', 'abserr', 'err'])
    df.boxplot(column='err', by='mews')
    plt.savefig(folder + 'err_by_mews.png')
    df.boxplot(column='abserr', by='mews')
    plt.savefig(folder + 'abserr_by_mews.png')

    df.boxplot(column='err', by='dt')
    plt.savefig(folder + 'err_by_dt.png')
    df.boxplot(column='abserr', by='dt')
    plt.savefig(folder + 'abserr_by_dt.png')


def plot_consecutive_preds(model, cfg, xtest, pred_seq, ts, fig, td_index, tod_index, eos_index, normies, pred_name, f_x_index, f_y_index):
    # multiple plots, starting with 10 input seq, predict ahead until 24h + the last sample
    # plot first the whole seq and output
    # then plot the mean prediction and std connected to the last input point forward until endpoint
    # note: requires that all vars in the output are also present in the input
    last_x_indices = np.arange(cfg['min_seq_len'] + 1, xtest.shape[0])
    dt_step = 2.

    plt.plot(ts, normies[pred_name].reverse(pred_seq), label='real values')
    first_it = True
    plot_alpha = np.linspace(0.15, 0.95, len(last_x_indices))
    avg_loss = 0
    for pp, last_x_i in enumerate(last_x_indices):
        x = xtest[:last_x_i].copy()
        x_real = normies[pred_name].reverse(x[:, f_x_index])
        x = np.pad(x, ((cfg['seq_len'] - x.shape[0], 0), (0, 0)), mode='constant', constant_values=(0, 0))
        last_td = x.copy()[-1, td_index]
        last_tod = x.copy()[-1, tod_index]
        x[-1, :] = 0.
        x[-1, td_index] = last_td
        x[-1, tod_index] = last_tod
        x[-1, eos_index] = 1.
        last_t = ts[last_x_i - 1]

        # compute loss at future points
        fut_val = xtest[last_x_i:, f_x_index]
        fut_td = np.cumsum(xtest[last_x_i:, td_index])
        fut_tod = xtest[last_x_i:, tod_index]
        avg_loss += future_pred_avg_loss(model, x.copy(), fut_val, fut_td, fut_tod, cfg, td_index, tod_index, f_y_index)

        # plot stuff predictions
        x_inc_dt, dts = inp_of_increasing_dt(x, dt_step, (ts[-1] - last_t + 2*dt_step) / dt_step)
        nsample = 1 if cfg['type'] == 'simple_normal' else 100
        yhats = [model(x_inc_dt) for _ in range(nsample)]  # increasing dt yhats
        for i, yhat in enumerate(yhats):
            yhat_mean = np.squeeze(yhat.mean()[:, f_y_index])
            yhat_std = np.squeeze(yhat.stddev()[:, f_y_index])

            if pred_name in normies:
                yhat_mean = normies[pred_name].reverse(yhat_mean)
                yhat_std = normies[pred_name].reverse(yhat_std)
            yhat_mean = np.concatenate([[x_real[-1]], yhat_mean], axis=0)
            yhat_std = np.concatenate([[0], yhat_std], axis=0)
            future_ts = np.concatenate([[last_t], last_t + dts])
            plt.plot(future_ts, yhat_mean, 'r--', linewidth=1., alpha=plot_alpha[pp], label='predicted mean' if first_it else None)
            plt.plot(future_ts, yhat_mean + yhat_std * 2, 'g--', linewidth=0.8, alpha=plot_alpha[pp],
                     label='predicted confidence (2*std)' if first_it else None)
            plt.plot(future_ts, yhat_mean - yhat_std * 2, 'g--', linewidth=0.8, alpha=plot_alpha[pp])
            first_it = False

    plt.legend(loc='upper left', fancybox=True, framealpha=0.5)
    return avg_loss / len(last_x_indices)


def future_pred_avg_loss(model, x, fut_val, fut_td, fut_tod, cfg, td_ind, tod_ind, f_y_index):

    # prepare input, x is already zeroed out and eos flag set at -1
    x = np.repeat(np.expand_dims(x, 0), len(fut_val), axis=0)
    x[:, -1, td_ind] = fut_td
    x[:, -1, tod_ind] = fut_tod

    nsample = 1 if cfg['type'] == 'simple_normal' else 100
    yhats = [model(x) for _ in range(nsample)]  # increasing dt yhats
    avg_loss = 0
    for i, yhat in enumerate(yhats):
        yhat_mean = np.squeeze(yhat.mean()[:, f_y_index])
        yhat_std = np.squeeze(yhat.stddev()[:, f_y_index])
        avg_loss += np.mean(np.abs(yhat_mean - fut_val))
    return avg_loss / len(yhats)


def test_prediction(model, cfg, test_data_x, test_data_y, x_cols, y_cols, normies, folder, nplots):
    # test model, same input but with different deltaT
    td_index = x_cols.index('td')
    eos_index = x_cols.index('EOS')
    if 'tod' in x_cols:
        tod_index = x_cols.index('tod')
    else:  # onehot encoding, gany melo
        tod_index = np.array([i for i, c in enumerate(x_cols) if 'tod' in c])

    # select long sequences
    not_padding = ~np.all(test_data_x == 0, axis=2)
    real_seqlen = np.sum(not_padding, axis=1)
    long_enough = np.argsort(real_seqlen)[::-1][:len(real_seqlen)//4]  # top 25%

    for pred_name in cfg['predict']:
        f_x_index = x_cols.index(pred_name)
        f_y_index = y_cols.index(pred_name)
        f_min = normies[pred_name].reverse(np.min(test_data_x[:, :, f_x_index]))
        f_max = normies[pred_name].reverse(np.max(test_data_x[:, :, f_x_index])) if pred_name != 'FullScore' else 15.

        # select sequences with high variance
        pred_seq = np.concatenate([test_data_x[:, :-1, f_x_index], np.expand_dims(test_data_y[:, f_y_index], -1)], axis=-1)
        pred_var = np.var(pred_seq, axis=1)
        var_enough = np.argsort(pred_var)[::-1][:len(pred_var) // 4]  # top 25%

        # select sequences
        selected_i = np.intersect1d(long_enough, var_enough)
        np.random.shuffle(selected_i)
        if len(selected_i) == 0:
            print('NO SEQUENCE THAT MEETS THE CRITERIA !')

        n = 0
        ext_folder = folder + '{}/'.format(pred_name)
        os.mkdir(ext_folder)
        for j in selected_i:  # np.random.permutation(np.arange(len(test_data_x))):
            xtest = test_data_x[j].copy()
            xtest = xtest[not_padding[j]]
            # ytest = test_data_y[j]
            ts = np.cumsum(xtest[:, td_index], axis=0)
            ps = pred_seq[j]
            ps = ps[not_padding[j]]

            fig = plt.figure(figsize=(18, 14))
            avg_loss = plot_consecutive_preds(model, cfg, xtest, ps, ts, fig, td_index, tod_index, eos_index,
                                              normies, pred_name, f_x_index, f_y_index)

            plt.ylim(f_min - 0.5, f_max + 0.5)
            plt.xlabel('time (h)')
            plt.ylabel(pred_name)
            plt.title('{} prediction #{}'.format(pred_name, j))
            plt.savefig(ext_folder + '{:.2f}_pred_{}.png'.format(avg_loss, j))
            plt.close(fig)

            n += 1
            if n > nplots:
                break


def seqs_of_length(x, length, x_cols):  # uses padding flag (named EOS, because I'm reusing that field)
    padflag = x[:, :, x_cols.index('EOS')]
    return padflag.sum(axis=-1) == length


# TODO td on separate route to distr lambda std for prediction models
# TODO random initial state for lstms, should put the model in a separate class to pass the random state to the call method
# TODO data generator
# TODO implement skipping
# TODO add presence variables
# TODO ? feed into the model the time range fot the next night as relative time from the current t
# TODO ! "real time" animation of who's sleeping, who's in danger etc on the prospective dataset - beds and all

# NOTES:
#  every visit less than 8 long is removed
#  slightly better with td as input
#  slightly worse with hospital as input
#  BMI is rare


def run_mews():
    # config: model and data constants
    cfg = {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight7',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'varscores'],
        # ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'],  # 'BMINum', varscores removed, FullScore
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [32] * 5,  # 256
        'min_seq_len': 8,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.92,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': 'linear_for_website_rnn',
    # 'to_show_off_linear'  # but_regr, w_rnn (order: rnn, dropout, batchnorm, ens), cpucompat
    }
    cfg_prosp = copy.deepcopy(cfg)
    cfg_prosp['test_data_ratio'] = 1.  # all into test and validation

    select_gpu(1)

    nepochs = 1  # TODO 500  # can be high, early stopping is implemented
    load_checkpoint = False
    force_load = load_checkpoint
    patience = 3
    data_size = ''  # mini/small/medium/''  TODO
    # model_path = 'models/'  # by default 'models'
    majority_class_rate = cfg['majority_class_rate']
    batch_size = cfg['batch_size']
    overwrite_results = True

    base_folder = '/mnt/data/mews/seqs/'
    base_folder_prospective = '/mnt/data/mews/seqs_prosp/'
    dsize = '' if data_size == '' else data_size + '_'
    mews_table = '/mnt/data/mews/tables/' + dsize + 'processed_mews2.csv'
    mews_prospective = '/mnt/data/mews/tables/' + dsize + 'processed_mews3.csv'
    dfname = data_file_name(cfg, data_size)
    dfname_prosp = data_file_name(cfg_prosp, data_size)
    if not os.path.isfile(base_folder + dfname):
        print('CREATING DATA')
        create_seq(cfg, mews_table, data_size, base_folder)
    if not os.path.isfile(base_folder_prospective + dfname_prosp):
        print('CREATING PROSPECTIVE DATA')
        create_seq(cfg_prosp, mews_prospective, data_size, base_folder_prospective)

    print('LOADING DATA')
    train_data_x, train_data_y, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies = \
        load_data(cfg, base_folder + dfname)

    # save normies
    with open('normies.pickle', 'wb') as f:
        pickle.dump(normies, f)

    # create results folder
    test_folder = 'results/' + model_name(cfg) + '/'
    if overwrite_results:
        shutil.rmtree(test_folder, ignore_errors=True)
        os.mkdir(test_folder)

    # train a linear regression model: use only last value of the sequence, flatten seq, train and test linear model
    print('TRAINING LINEAR MODEL')
    # after_padding = not_padding_from(train_data_x)
    flat_train_data_x = train_data_x[:, -1, :]  # np.concatenate([x[p:] for x, p in zip(train_data_x, after_padding)])
    # after_padding = not_padding_from(test_data_x)
    flat_test_data_x = test_data_x[:, -1, :]  # np.concatenate([x[p:] for x, p in zip(test_data_x, after_padding)])
    flat_train_data_y, flat_test_data_y = train_data_y.ravel(), test_data_y.ravel()

    # build and train linear model
    from sklearn.linear_model import LogisticRegression
    lr_model = LogisticRegression(class_weight=cfg['class_weight'], n_jobs=4).fit(flat_train_data_x, flat_train_data_y)
    with open(f'models/linear/{cfg["name_suffix"]}.pickle', 'wb') as f:
        pickle.dump(lr_model, f)

    print('BUILDING MODEL')
    inp_dim = train_data_x.shape[2]
    out_dim = train_data_y.shape[1]
    pprint(cfg)
    model = build_model(cfg, inp_dim, out_dim, load_checkpoint, force_load)

    print('TRAINING')
    train(model, cfg, nepochs, patience, train_data_x, train_data_y, valid_data_x, valid_data_y, x_cols, y_cols)

    # print('TESTING BEGINS')
    thresholds, lin_thresholds = test_sleeping(model, cfg, test_data_x, test_data_y, x_cols, y_cols, normies,
                                               test_folder, overwrite_results,
                                               linear_stuff={'model': lr_model, 'flat_x': flat_test_data_x,
                                                             'flat_y': flat_test_data_y})
    print('THRESHOLDS:', thresholds)
    print('LINEAR THRESHOLDS:', lin_thresholds)
    exit()  # TODO

    print('TESTING FOR DIFFERENT SEQUENCE LENGTHS')
    for l in range(cfg['min_seq_len'], cfg['seq_len'] + 1):
        right_length = seqs_of_length(test_data_x, l, x_cols)
        if right_length.sum() > 0:
            x = test_data_x[right_length]
            y = test_data_y[right_length]
            test_folder_l = f'{test_folder}/{l}/'
            os.mkdir(test_folder_l)
            thresholds_l, _ = test_sleeping(model, cfg, x, y, x_cols, y_cols, normies, test_folder_l, overwrite_results)
            print(f'THRESHOLDS at length {l}: {thresholds_l}')

    # print('PROSPECTIVE TESTING BEGINS')  # TODO prospective testing at different sequence lengths
    # test_sleeping_prosp(model, cfg, cfg_prosp, x_cols, y_cols, normies, 'results/' + model_name(cfg) + '/',
    #                     overwrite_results, thresholds, base_folder_prospective, data_size, mews_prospective)

    print('PROSPECTIVE LINEAR TESTING BEGINS')
    test_sleeping_prosp_linear(lr_model, cfg, cfg_prosp, x_cols, y_cols, normies, 'results/' + model_name(cfg) + '/',
                               overwrite_results, lin_thresholds, base_folder_prospective, data_size, mews_prospective)
    # plt.show()


def run_lskl():

    # lskl pipeline: consolidate_lskl.py, preprocess_mews.py, prepare_seq_input.py, then the following

    # config: model and data constants
    cfg = {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight_LSKL_2_samples',
        'input': ['td', 'tod', 'age_days', 'HR', 'TC', 'SBP', 'SPO2',
                  'HR_low', 'HR_high', 'RR_low', 'RR_high', 'TC_low', 'TC_high',
                  'SBP_low', 'SBP_high', 'SPO2_low', 'SPO2_high'],
        'predict': ['HR_high'],  # whatever
        'lr': 0.001,
        'layers': [32] * 5,  # 256
        'min_seq_len': 8,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.9,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 1.},
        'name_suffix': 'lskl',  # (order: rnn, dropout, batchnorm, ens), cpucompat
    }
    cfg_prosp = copy.deepcopy(cfg)
    cfg_prosp['test_data_ratio'] = 1.  # all into test and validation

    select_gpu(0)

    nepochs = 500
    load_checkpoint = False
    force_load = load_checkpoint
    patience = 3
    overwrite_results = True

    lskl_ver = '030521'  # TODO when loading seq, always provide this as prefix to data_file_name()
    data_size = ''  # mini/small/medium/''  TODO
    model_path = 'lskl_models'
    base_folder = '/mnt/data/lskl'
    seqs_folder = f'{base_folder}/seqs/'
    # seqs_folder_prospective = f'{base_folder}/seqs_prosp/'
    dsize = '' if data_size == '' else data_size + '_'
    mews_table = f'{base_folder}/tables/{dsize}processed_lskl_{lskl_ver}.csv'
    # mews_prospective = f'{base_folder}/tables/{dsize}processed_lskl_{lskl_ver}.csv'
    dfname = data_file_name(cfg, data_size, prefix=lskl_ver)
    # dfname_prosp = data_file_name(cfg_prosp, data_size, prefix=lskl_ver)
    if not os.path.isfile(seqs_folder + dfname):
        print('CREATING DATA')
        create_seq(cfg, mews_table, data_size, seqs_folder, prefix=lskl_ver)
    # if not os.path.isfile(seqs_folder_prospective + dfname_prosp):
    #     print('CREATING PROSPECTIVE DATA')
    #     create_seq(cfg_prosp, mews_prospective, data_size, seqs_folder_prospective)

    print('LOADING DATA')
    train_data_x, train_data_y, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies = \
        load_data(cfg, seqs_folder + dfname)

    # save normies
    with open('normies.pickle', 'wb') as f:
        pickle.dump(normies, f)

    # create results folder
    test_folder = 'results/' + model_name(cfg) + '/'
    if overwrite_results:
        shutil.rmtree(test_folder, ignore_errors=True)
        os.mkdir(test_folder)

    # train a linear regression model: use only last value of the sequence, flatten seq, train and test linear model
    print('TRAINING LINEAR MODEL')
    # after_padding = not_padding_from(train_data_x)
    flat_train_data_x = train_data_x[:, -1, :]  # np.concatenate([x[p:] for x, p in zip(train_data_x, after_padding)])
    # after_padding = not_padding_from(test_data_x)
    flat_test_data_x = test_data_x[:, -1, :]  # np.concatenate([x[p:] for x, p in zip(test_data_x, after_padding)])
    flat_train_data_y, flat_test_data_y = train_data_y.ravel(), test_data_y.ravel()

    # build and train linear model
    from sklearn.linear_model import LogisticRegression
    lr_model = LogisticRegression(class_weight=cfg['class_weight'], n_jobs=4).fit(flat_train_data_x, flat_train_data_y)
    with open(f'{model_path}/linear/{cfg["name_suffix"]}.pickle', 'wb') as f:
        pickle.dump(lr_model, f)

    print('BUILDING MODEL')
    inp_dim = train_data_x.shape[2]
    out_dim = train_data_y.shape[1]
    pprint(cfg)
    model = build_model(cfg, inp_dim, out_dim, load_checkpoint, force_load, model_path)

    print('TRAINING')
    train(model, cfg, nepochs, patience, train_data_x, train_data_y, valid_data_x, valid_data_y, x_cols, y_cols, model_path)

    # print('TESTING BEGINS')
    thresholds, lin_thresholds = test_sleeping(model, cfg, test_data_x, test_data_y, x_cols, y_cols, normies,
                                               test_folder, overwrite_results,
                                               linear_stuff={'model': lr_model, 'flat_x': flat_test_data_x,
                                                             'flat_y': flat_test_data_y})
    print('THRESHOLDS:', thresholds)
    print('LINEAR THRESHOLDS:', thresholds)

    # print('TESTING FOR DIFFERENT SEQUENCE LENGTHS')
    # for l in range(cfg['min_seq_len'], cfg['seq_len'] + 1):
    #     right_length = seqs_of_length(test_data_x, l, x_cols)
    #     if right_length.sum() > 0:
    #         x = test_data_x[right_length]
    #         y = test_data_y[right_length]
    #         test_folder_l = f'{test_folder}/{l}/'
    #         os.mkdir(test_folder_l)
    #         thresholds_l, _ = test_sleeping(model, cfg, x, y, x_cols, y_cols, normies, test_folder_l, overwrite_results)
    #         print(f'THRESHOLDS at length {l}: {thresholds_l}')

    # print('PROSPECTIVE TESTING BEGINS')  # TODO prospective testing at different sequence lengths
    # test_sleeping_prosp(model, cfg, cfg_prosp, x_cols, y_cols, normies, 'results/' + model_name(cfg) + '/',
    #                     overwrite_results, thresholds, seqs_folder_prospective, data_size, mews_prospective)

    # print('PROSPECTIVE LINEAR TESTING BEGINS')
    # test_sleeping_prosp_linear(lr_model, cfg, cfg_prosp, x_cols, y_cols, normies, 'results/' + model_name(cfg) + '/',
    #                            overwrite_results, lin_thresholds, seqs_folder_prospective, data_size, mews_prospective)
    # plt.show()


if __name__ == '__main__':
    # run_mews()
    run_lskl()
