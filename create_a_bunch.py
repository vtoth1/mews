import os, shutil, sys, json
from lstm import data_file_name, create_seq


if __name__ == '__main__':

    data_size = ''  # mini/small/medium/''
    test_data_ratio = 0.2

    # config: model and data constants
    with open('bunch.json', 'rt') as f:
        bunch = json.load(f)

    for cfg in bunch:
        base_folder = '/mnt/data/mews/seqs/'
        dfname = base_folder + data_file_name(data_size, cfg['task'], cfg['input'], cfg['predict'], cfg['min_seq_len'],
                                          cfg['seq_len'],
                                          cfg['test_rand_scheme'], test_data_ratio,
                                          cfg['data_augmentation']['step'],
                                          cfg['data_augmentation']['skip'])

        if not os.path.isfile(dfname):
            print('CREATING DATA', dfname)
            create_seq(cfg['min_seq_len'], cfg['input'], cfg['predict'], data_size, cfg['seq_len'], cfg['task'],
                       test_data_ratio, cfg['test_rand_scheme'], cfg['data_augmentation']['step'],
                       cfg['data_augmentation']['skip'])
        else:
            print('ALREADY EXISTS', dfname)
