import numpy as np
import tensorflow as tf
import pandas as pd
import tables
from prepare_seq_input import MORNING_START, NIGHT_START, are_nights
from lstm import load_data, build_model, select_gpu, predict, clin_appl_idx, load_ref_data
from prepare_seq_input import data_file_name
import pickle, os, sys
from sklearn.metrics import confusion_matrix
from pprint import pprint
import matplotlib.pyplot as plt
import seaborn
from scipy import stats
import matplotlib.dates as dates
from matplotlib.patches import Rectangle
from datetime import timedelta
from plot_test import load_test_results


def plot_tn_fn(model_name, confidences_of_interest, anal_fname_template):
    fns = []
    tns = []
    for confidence in confidences_of_interest:
        with open(anal_fname_template.format(model_name, confidence), 'rb') as f:
            anal = pickle.load(f)
            fns.append(anal['nFN'])
            tns.append(anal['nTN'])

    fns_abs = [fn[0] for fn in fns]
    tns_abs = [tn[0] for tn in tns]
    fns_perc = [fn[1] * 100 for fn in fns]
    tns_perc = [tn[1] * 100 for tn in tns]

    plt.rc('font', size=16)

    for fns, tns, fname, yl in [(fns_abs, tns_abs, 'tn_fn_abs.png', ''), (fns_perc, tns_perc, 'tn_fn_perc.png', '(%)')]:
        fig = plt.figure(figsize=(18, 14))
        host = fig.add_subplot(111)
        par = host.twinx()

        host.set_ylim([0, np.max(fns) + np.max(fns) / 5.])
        host.set_ylabel('Unstable Sleeping {}'.format(yl))
        par.set_ylabel('Sleeping Safely {}'.format(yl))
        host.set_xlabel('Confidence to Wake Up')
        host.set_xticks(confidences_of_interest)

        hp, = host.plot(confidences_of_interest, fns, color=(0.5, 0, 0.5))
        host.scatter(confidences_of_interest, fns, color=(0.5, 0, 0.5))
        for i, ann in enumerate(fns):
            host.annotate('{:.4f}'.format(ann), (confidences_of_interest[i], fns[i]))

        pp, = par.plot(confidences_of_interest, tns, color=(0, 0, 1.))
        par.scatter(confidences_of_interest, tns, color=(0, 0, 1.))
        for i, ann in enumerate(tns):
            par.annotate('{:.4f}'.format(ann), (confidences_of_interest[i], tns[i]))

        host.yaxis.label.set_color(hp.get_color())
        par.yaxis.label.set_color(pp.get_color())
        fig.savefig('post_analysis/{}_'.format(model_name) + fname)


model_cfgs = {
    '5+': {
        'type': 'simple_normal',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight5',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'],
    # 'BMINum', varscores removed, FullScore
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [256] * 5,  # 256
        'min_seq_len': 8,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.6,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': ''
    },
    '6+': {
        'type': 'simple_normal',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight6',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'],
    # 'BMINum', varscores removed, FullScore
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [256] * 5,  # 256
        'min_seq_len': 8,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.6,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': ''
    },
    '7+': {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight7',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'],
    # 'BMINum', varscores removed, FullScore
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [256] * 5,  # 256
        'min_seq_len': 8,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.6,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': 'dropout_batchnorm_final_ens0:24'  # but_regr, w_rnn (order: rnn, dropout, batchnorm, ens)
    }
}

FIELD_TO_PLOT = {'age': 'Age', 'td': 'Δtime (h)', 'tod': 'Hour of Day', 'FullScore': 'Risk Score', 'BPNum': 'BP (mmHg)',
                 'HRNum': 'HR (bpm)', 'TmprNum': 'Tmpr (\u00B0C)', 'RespRateNum': 'RR (bpm)', 'dfs': 'ΔMEWS',
                 'varscores': 'Var(Scores)'}

# option for prospective
prospective = False
if prospective:
    model_cfgs['5+']['test_data_ratio'] = 1.
    model_cfgs['6+']['test_data_ratio'] = 1.
    model_cfgs['7+']['test_data_ratio'] = 1.

# params
model_name = sys.argv[1] if len(sys.argv) > 1 else '7+'
cfg = model_cfgs[model_name]
pprint(cfg)

# compute confidence from desired max FN ratios
# confidences_of_interest = [.001, .002, .004, .006, .008, .01]
model_fn_ratios = np.array([1 / 20000, 1 / 10000, 2 / 10000])
res = load_test_results(cfg, prospective=prospective)
_, fpsinv, thresholdsinv, predsinv = res['tpsinv'], res['fpsinv'], res['thresholdsinv'], res['predsinv']
n = len(predsinv)
nfn = np.max(fpsinv)

# model_idx = np.array([clin_appl_idx(fpsinv, len(predsinv), r) for r in model_fn_ratios])
# confidences_of_interest = thresholdsinv[model_idx]
confidences_of_interest = 1. - np.array(
    [0.9949689, 0.99110633, 0.9859783])  # predefined through the results of 20 ensemble models

confidence = confidences_of_interest[-1]
anal_fname_template = 'post_analysis/{}_{}_{}.pickle'
anal_fname = anal_fname_template.format(model_name, confidence, 'prosp' if prospective else 'retro')
base_folder = '/mnt/data/mews/seqs/' if not prospective else '/mnt/data/mews/seqs_prosp/'
mews_cols = ['index', 'ClientVisitGUID', 'AdmitDtm', 'AlertRepositoryGUID',
             'Status', 'Hospital', 'age', 'BPNum', 'HRNum', 'TmprNum',
             'RespRateNum', 'AVPU', 'BMINum', 'AgeNum', 'BPScore', 'HRScore',
             'TmprScore', 'RespRateScore', 'AVPUScore', 'BMIScore', 'AgeScore',
             'FullScore', 'AlertDtm', 'ts', 'td', 'dfs', 'tod', 'relts',
             'newid_flag', 'newid', 'fsgt7', 'last']
col = {name: i for i, name in enumerate(mews_cols)}

# check the remaining x hundred unsafe sleepers
# run this script in console mode
# steps:
#   define analysis parameters: model name (5+/6+/7+), confidence index
#   load post analysis files if present, else generate them:
#     load model, mews table, seq data w/ references and dates
#     run model on test set
#     at each confidence, store # of TN, # of FN, FN sequences and original sequence from mews table
#     for the original sequence, store where the test seq started and ended (indices) according to dates
#     plot and store # of TN and FN as the confidence changes
#   when post analysis files available for given model and confidence, load them and allow easy analysis:
#     can run predefined functions that plot the history of a given case (mews, HR, RR, ... any of them)

# stored anal data format:
#   {nTN: (x, %), nFN: (x, %),
#     FNseq: {x: test_x, x_cols: x_cols, normies: normies},
#     FNdata: {seq: [[mews record indexed as text_x], ...], seq_start_i: [x, ...], seq_end_i: [x, ...]}}

if not os.path.isfile(anal_fname) and __name__ == '__main__':  # generate analysis files then
    select_gpu(0)

    data_size = ''  # mini/small/medium/''
    model_path = 'models/'
    batch_size = 256  # cfg['batch_size']

    print('LOADING DATA')
    dfname = base_folder + data_file_name(cfg, data_size)
    _, _, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies = load_data(cfg, dfname)
    if prospective:
        test_data_x = np.concatenate([valid_data_x, test_data_x])
        test_data_y = np.concatenate([valid_data_y, test_data_y])

    print('BUILDING MODEL')
    inp_dim = test_data_x.shape[2]
    out_dim = test_data_y.shape[1]
    model = build_model(cfg, inp_dim, out_dim, load_checkpoint=True, force_load=True)

    print('LOADING MEWS')
    dsize = '' if data_size == '' else data_size + '_'
    mews_table = 'data/' + dsize + 'processed_mews{}.csv'.format(3 if prospective else 2)
    mews = pd.read_csv(mews_table, parse_dates=['AlertDtm']).sort_values(['newid', 'AlertDtm'])
    must_not_be_nan = list(set(mews.columns.values).intersection(set(cfg['input'] + cfg['predict']))) + ['FullScore']
    mews = mews.dropna(axis=0, how='any', subset=must_not_be_nan)

    print('LOADING REFERENCE')
    _, _, _, _, _, _, test_newid, test_start, test_end, _, _, _, _, _, _, _, _, _ = load_ref_data(dfname)

    print('GEN ANAL DATA')
    if prospective:  # load data from file
        preds, desired = 1. - res['predsinv'], 1. - res['desiredinv']
    else:  # retro
        preds, desired = predict(model, cfg, test_data_x, test_data_y, batch_size)

    for conf in confidences_of_interest:
        predicted = (preds > conf).astype(np.int32)
        fn_i = (predicted == 0) & (desired == 1)
        tn_i = (predicted == 0) & (desired == 0)
        # randomize and cut tn_i because there are too much of them, and we don't need all for the analysis
        tn_i = np.random.choice(np.where(tn_i)[0], min(100000, tn_i.sum()),
                                replace=False)  # 2k should be more than enough

        cm = confusion_matrix(desired, predicted)
        cm_perc = cm / len(preds)
        ntn = (cm[0, 0], cm_perc[0, 0])
        nfn = (cm[1, 0], cm_perc[1, 0])

        fn_seq = test_data_x[
            fn_i]  # FIXME test_data_x and fn_i are not the same size (1 diff) due to how prospective testing is done probably, we lose one sample on the way
        tn_seq = test_data_x[tn_i]

        print('REFER BACK TO ORIGINAL DATA')
        fn_newid = test_newid[fn_i].astype(str)
        fn_start = test_start[fn_i]
        fn_end = test_end[fn_i]
        fn_mews = mews.loc[mews['newid'].isin(fn_newid)]

        tn_newid = test_newid[tn_i].astype(str)
        tn_start = test_start[tn_i]
        tn_end = test_end[tn_i]
        tn_mews = mews.loc[mews['newid'].isin(tn_newid)]

        # build the sequences from the original, according to the order of the test_seq
        fn_data = []
        tn_data = []
        fn_seq_start_i = []
        fn_seq_end_i = []
        tn_seq_start_i = []
        tn_seq_end_i = []

        # FIXME this is kinda ugly
        nfailed_to_load = 0
        for ft_mews, data, seq_start_i, seq_end_i, new_id, start, end in [
            (fn_mews, fn_data, fn_seq_start_i, fn_seq_end_i, fn_newid, fn_start, fn_end),
            (tn_mews, tn_data, tn_seq_start_i, tn_seq_end_i, tn_newid, tn_start, tn_end)]:
            print('ON TN OR FN')
            for i, (newid, start_date, end_date) in enumerate(zip(new_id, start, end)):
                mews_of_newid = ft_mews.loc[ft_mews['newid'] == newid]
                start_date = pd.Timestamp(start_date)
                end_date = pd.Timestamp(end_date)
                start_i = np.sum(
                    mews_of_newid['AlertDtm'].dt.tz_localize(None) < start_date)  # start of predicted night
                end_i = np.sum(mews_of_newid['AlertDtm'].dt.tz_localize(None) <= end_date)  # end of predicted night
                orig_seq = [list(row) for row in mews_of_newid.iterrows()]

                if start_i == 0 or end_i == 0 or len(orig_seq) == 0:
                    print('WARNING: newid {}, start_i {}, end_i {}, l(mews)={}'.format(newid, start_i, end_i,
                                                                                       len(mews_of_newid)))
                    nfailed_to_load += 1

                data.append(orig_seq)
                seq_start_i.append(start_i)
                seq_end_i.append(end_i)

                if i % 1000 == 0:
                    print('still alive:', i)

            print('NFAILEDTOLOAD:', nfailed_to_load)

        with open(anal_fname, 'wb') as f:
            obj = {'nTN': ntn, 'nFN': nfn,
                   'FNseq': {'x': fn_seq, 'x_cols': x_cols, 'normies': normies},
                   'FNdata': {'seq': fn_data, 'seq_start_i': fn_seq_start_i, 'seq_end_i': fn_seq_end_i},
                   'TNseq': {'x': tn_seq, 'x_cols': x_cols, 'normies': normies},
                   'TNdata': {'seq': tn_data, 'seq_start_i': tn_seq_start_i, 'seq_end_i': tn_seq_end_i}}
            pickle.dump(obj, f)

        print('NFAILEDTOLOAD ALL:',
              nfailed_to_load)  # FIXME TODO LOOK INTO HOW THIS IS POSSIBLE: WARNING: newid 2114657600295_109372, start_i 0, end_i 51, l(mews)=131
        print('STORED {}'.format(anal_fname))


# plot_tn_fn(model_name, confidences_of_interest, anal_fname_template)

def extract_hist_and_night(anal, col, which):
    fndata = anal[which]
    extracted = []
    for seq, seq_start, seq_end in zip(fndata['seq'], fndata['seq_start_i'], fndata['seq_end_i']):
        night_len = 0
        night_found = False
        for i in range(seq_end, len(seq)):
            if not night_found and (seq[i][1][col['tod']] >= NIGHT_START or seq[i][1][col['tod']] < MORNING_START):
                night_found = True
            elif night_found and not (seq[i][1][col['tod']] >= NIGHT_START or seq[i][1][col['tod']] < MORNING_START):
                break
            night_len += 1
        seq_no_index = [s[1] for s in seq]
        extracted.append((np.array(seq_no_index[:seq_end]), np.array(seq_no_index[seq_end:seq_end + night_len]),
                          np.array(seq_no_index[seq_end + night_len:]), seq_start, seq_end))
    return np.array(extracted)


def fn_w_mews_at_night_gt(hist_and_night, col, threshold):
    # for i, s in enumerate(hist_and_night[:,1]):
    #     if np.any(np.array(s[:, col['FullScore']]) >= threshold):
    #         print(i)
    return np.array([hist_and_night[i] for i, s in enumerate(hist_and_night[:, 1]) if
                     len(s) > 0 and np.any(np.array(s[:, col['FullScore']]) >= threshold)])
    # return [hist_and_night[i] for i in indices]


def get_f_values_where_mews_peaks(hist_and_night, col, field):
    peak_f_values = []
    for visit in hist_and_night:
        night_mews = visit[1][:, col['FullScore']]
        night_f = visit[1][:, col[field]]
        peak_f_values.append(night_f[np.argmax(night_mews)])
    return np.array(peak_f_values)


def get_field_values(hist_and_night, field, col, at='all'):
    if at == 'history':
        data = [hist_and_night[:, 0]]
    elif at == 'night':
        data = [hist_and_night[:, 1]]
    elif at == 'after':
        data = [hist_and_night[:, 2]]
    else:
        data = [hist_and_night[:, 0], hist_and_night[:, 1], hist_and_night[:, 2]]

    field_data = []
    for d in data:
        for seq in d:
            if len(seq) > 0:
                field_data.append(np.max(seq[:, col[field]]))  # max only for now
    return np.array(field_data)  # np.concatenate(field_data)


def hist_it(arr, bins=15, field=''):
    plt.rc('font', size=20)
    plt.figure()
    plt.hist(arr, bins=bins)
    plt.title(field)
    plt.show()


def hist_that_mews(mews_vals, density=False, ax=None):
    x = np.arange(int(np.min(mews_vals)), int(np.max(mews_vals)) + 1)
    y = np.zeros(x.shape)
    for v in mews_vals:
        y[int(v - x[0])] += 1
    y = y / np.sum(y) if density else y

    plt.rc('font', size=12)
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(6.2, 6.2))
    ax = seaborn.barplot(x, y, palette=['C4', 'C4', 'C4', 'C4'], alpha=.6, ax=ax)
    # for patch in ax.artists:
    #     r, g, b, a = patch.get_facecolor()
    #     patch.set_facecolor((r, g, b, .1))
    ax.set_xlabel('Risk score')
    ax.set_ylabel('Number of misclassified\nas stable nights')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    ticks = np.arange(0, np.max(y) + 1, 50)
    ticks = ticks[:-1]  # remove 100, too crowded y axis
    if density:
        ticks = np.arange(0, np.max(y) + .1, .25)
    ticks = np.sort(np.unique(np.concatenate([ticks, y])))
    ax.set_yticks(ticks[1:])  # remove 0


def hist_all_that_mews(mews_valss, hr_threshold_txts, density=False, ax=None):
    df = pd.DataFrame(columns=['Risk score', 'Number of missed nights', 'HR threshold'])
    xs, ys = [], []
    for mews_vals, hr_threshold_txt in zip(mews_valss, hr_threshold_txts):
        x = np.arange(int(np.min(mews_vals)), int(np.max(mews_vals)) + 1)
        y = np.zeros(x.shape)
        for v in mews_vals:
            y[int(v - x[0])] += 1
        y = y / np.sum(y) if density else y
        xs.append(x)
        ys.append(y)

        for x_i, y_i in zip(x, y):
            # df = df.append([x_i, y_i, hr_threshold_txt])
            df = df.append([{'Risk score': x_i, 'Number of missed nights': y_i, 'HR threshold': hr_threshold_txt}])

    plt.rc('font', size=12)
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(6.2, 6.2))
    dark_col = (148/255, 103/255, 189/255)
    light_col = (222/255, 209/255, 235/255)
    inbetween_col = ((dark_col[0] + light_col[0]) / 2, (dark_col[1] + light_col[1]) / 2, (dark_col[2] + light_col[2]) / 2)
    seaborn.catplot(x='Risk score', y='Number of missed nights', hue='HR threshold', data=df, kind='bar', ax=ax,
                    legend=False, palette=[dark_col, inbetween_col, light_col])
    #palette=['C1', 'C2', 'C3'])  # , palette=['C4', 'C4', 'C4', 'C4'], alpha=.6, ax=ax)
    ax.legend(loc='upper right')
    # for patch in ax.artists:
    #     r, g, b, a = patch.get_facecolor()
    #     patch.set_facecolor((r, g, b, .1))
    # ax.set_xlabel('Risk Score')
    # ax.set_ylabel('Number of Missed Nights')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)  # TODO check how these work for seaborn

    x = np.concatenate(xs)
    y = np.concatenate(ys)
    ticks = np.arange(0, np.max(y) + 1, 50)
    ticks = ticks[:-1]  # remove 100, too crowded y axis
    if density:
        ticks = np.arange(0, np.max(y) + .1, .25)
    ticks = np.sort(np.unique(np.concatenate([ticks, y])))
    # ax.set_yticks(ticks[1:])  # remove 0


def plot_fn_trajectory(hist_and_night_instance, field, col):
    hist = hist_and_night_instance[0]
    night = hist_and_night_instance[1]
    after = hist_and_night_instance[2]

    plt.figure()
    plt.rc('font', size=20)
    plt.plot(hist[:, col['AlertDtm']], hist[:, col[field]], 'o-', label='history')
    plt.plot(night[:, col['AlertDtm']], night[:, col[field]], 'o-', label='night')
    plt.plot(after[:, col['AlertDtm']], after[:, col[field]], 'o-', label='after')
    plt.xlabel('Time')
    plt.ylabel(field)
    plt.legend(loc='upper left', fancybox=True, framealpha=0.5)
    plt.show()


def plot_multiple_trajectories(hist_and_night, field, col, ax=None):
    if ax is None:
        fig = plt.figure(figsize=(10, 8))
        ax = fig.axes[0]
    plt.rc('font', size=14)
    shared_traj_len = np.min([len(t) for t in hist_and_night[:, 0]])

    # find highest delta time (in hist from night) that's still present in all trajectories
    earlies = []
    for hist_and_night_instance in hist_and_night:
        hist = hist_and_night_instance[0][-shared_traj_len:]
        night = hist_and_night_instance[1]

        night_t = night[len(night) // 2, col['AlertDtm']]
        hist_rel_t = np.array([delta.total_seconds() for delta in (hist[:, col['AlertDtm']] - night_t)]) / 3600
        earlies.append(hist_rel_t[0])
    # earliest = np.max(earlies)  # also present in all trajectories
    earliest = -72  # h

    all_traj = []
    all_traj_t = []
    for hist_and_night_instance in hist_and_night:
        hist = hist_and_night_instance[0][-shared_traj_len:]
        night = hist_and_night_instance[1]

        night_t = night[len(night) // 2, col['AlertDtm']]
        hist_rel_t = np.array([delta.total_seconds() for delta in (hist[:, col['AlertDtm']] - night_t)]) / 3600.
        night_rel_t = np.array([delta.total_seconds() for delta in (night[:, col['AlertDtm']] - night_t)]) / 3600.

        # all_rel_t = np.concatenate([hist_rel_t, night_rel_t])
        # all_f_val = np.concatenate([hist[:, col[field]], night[:, col[field]]])
        # tsplot(all_rel_t, all_f_val)
        # plt.show()

        to_include = hist_rel_t >= earliest
        night_max = np.max(night[:, col[field]])
        night_max_t = night_rel_t[np.argmax(night[:, col[field]])]
        traj = np.concatenate([hist[to_include, col[field]], [night_max]])
        traj_t = np.concatenate([hist_rel_t[to_include], [night_max_t]])

        # jitter
        jitter_amount = 0.15 if field == 'FullScore' else 0
        traj = traj + (np.random.random(len(traj)) - 0.5) * (np.max(traj) - np.min(traj)) * jitter_amount
        traj_t = traj_t + (np.random.random(len(traj_t)) - 0.5) * (np.max(traj_t) - np.min(traj_t)) * jitter_amount

        if (field == 'RespRateNum' and np.any(traj > 40)) or (field == 'HRNum' and np.any(traj > 250)):
            continue

        all_traj.append(traj)
        all_traj_t.append(traj_t)

        ax.plot(traj_t, traj, 'o-', alpha=0.08, color='grey', markersize=4, zorder=5)
        # plt.plot([hist_rel_t[-1], night_rel_t[0]], [hist[-1, col[field]], night[0, col[field]]], '--', alpha=0.3, color='grey')
        # plt.plot(night_rel_t, night[:, col[field]], 'o-', alpha=0.1, color='C3', markersize=4)

    # plot mean
    # comb dates and values together before computing moving avg
    all_traj = sorted([(t, v) for traj_t, traj in zip(all_traj_t, all_traj) for t, v in zip(traj_t, traj)])
    all_traj_t = np.array([t for t, v in all_traj])
    all_traj = np.array([v for t, v in all_traj])

    mean_bins = np.linspace(earliest, np.max(all_traj_t) + 1, 11)

    mean_traj = []
    mean_traj_t = []
    std_traj = []
    for i in range(1, len(mean_bins)):
        bin_indices = np.digitize(all_traj_t, mean_bins) == i
        mean_traj.append(np.mean(all_traj[bin_indices]))
        mean_traj_t.append(np.mean(all_traj_t[bin_indices]))
        std_traj.append(np.std(all_traj[bin_indices]))
    mean_traj = np.array(mean_traj)
    mean_traj_t = np.array(mean_traj_t)
    std_traj = np.array(std_traj)

    # # get the means first for those that have more elements than the min
    # min_traj_len = np.min([len(m) for m in mean_traj])
    # leftover_traj = []
    # leftover_traj_t = []
    # for l in range(min_traj_len + 1, np.max([len(m) for m in mean_traj]) + 1):
    #     leftover_traj.append([m[len(m) - l] for m in mean_traj if len(m) >= l])
    #     leftover_traj_t.append([m[len(m) - l] for m in mean_traj_t if len(m) >= l])
    # leftover_traj = np.mean(np.array(leftover_traj), axis=1)
    # leftover_traj_t = np.mean(np.array(leftover_traj_t), axis=1)
    #
    # mean_traj = [m[len(m) - min_traj_len:] for m in mean_traj]
    # mean_traj_t = [m[len(m) - min_traj_len:] for m in mean_traj_t]
    # mean_traj = np.mean(np.array(mean_traj), axis=0)
    # mean_traj_t = np.mean(np.array(mean_traj_t), axis=0)
    #
    # plt.plot(np.concatenate([leftover_traj_t, mean_traj_t]), np.concatenate([leftover_traj, mean_traj]), 'C3', alpha=1., linewidth=5.)

    # plot mean and std
    upper_std = mean_traj + std_traj
    lower_std = mean_traj - std_traj
    ax.fill_between(mean_traj_t, upper_std, lower_std, facecolor='C3', alpha=0.15, zorder=10)
    ax.plot(mean_traj_t, mean_traj, 'C3', alpha=1., linewidth=5., zorder=11)  # , solid_capstyle='round')

    # plot night band
    y_range = (ax.axis()[2], ax.axis()[3])
    ax.add_patch(Rectangle((-5, y_range[0]), 10, y_range[1], alpha=0.3, facecolor='C4', zorder=1))

    ax.set_xlim(earliest, 10)
    ax.set_xlabel('Hours relative to predicted night')
    ax.set_ylabel(FIELD_TO_PLOT[field])
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    if field == 'FullScore':
        ax.set_yticks(range(0, 11, 1))
    if field == 'TmprNum':
        ax.set_yticks(range(int(round(np.min(all_traj))), int(round(np.max(all_traj))) + 1, 1))


FIELD_TO_YPAD = {'FullScore': 25, 'BPNum': 16, 'HRNum': 16, 'TmprNum': 25, 'RespRateNum': 25, 'y': 43}
FIELD_TO_NORMAL_SCORE = {'FullScore': [0, 7], 'BPNum': [101, 199], 'HRNum': [51, 100], 'TmprNum': [35, 38.4],
                         'RespRateNum': [9, 14]}


def plot_stem(hnn, fields, col):  # anal_fname = 'post_analysis/7+_old/7+_0.01.pickle'
    # take the night and the one before
    hist = hnn[0]
    night = hnn[1]

    # find day/night barriers
    is_night = are_nights(hist[:, col['tod']])
    prev_night_end = np.where(is_night)[0][-1] + 1  # not inclusive
    prev_night_start = prev_night_end - 1
    while is_night[prev_night_start] or prev_night_start < 0:
        prev_night_start -= 1
    prev_night_start += 1

    day_len = len(hist) - prev_night_end  # second day's len
    prev_day_start = max(0, prev_night_start - day_len)

    # copy first element of all 2 hours later; make HR dotted line stem
    quasi_imputed = hist[prev_day_start]

    print('night len:', len(night), '| prev night from {} to {}'.format(prev_night_start, prev_night_end))
    print('day len:', day_len, '| prev day from {} to {}'.format(prev_day_start, prev_night_start))

    # plot
    plt.style.use('seaborn-white')
    plt.rc('font', size=14)
    ylabel_font_size = 13
    # fig = plt.figure(figsize=(12, 2), dpi=100)
    fig, axes = plt.subplots(len(fields) + 1, 1, sharex=True, figsize=(12.4, 8.4))

    t_hist = hist[:, col['AlertDtm']].astype(pd.Timestamp)
    # FIXME for the old dataset the night contains one more element in the beginning (extra element from the left);
    #   check if this is true for new datsets! though this function is used mainly to plot for the paper from old data
    t_night = night[:, col['AlertDtm']].astype(pd.Timestamp)
    imputed_t = quasi_imputed[col['AlertDtm']] + timedelta(hours=2)  # already timestamp

    for ax, field in zip(axes[:-1], fields):
        f_hist = hist[:, col[field]]
        f_night = night[:, col[field]]
        f_all = np.concatenate([f_hist, f_night])
        imputed = quasi_imputed[col[field]]

        # night band
        maxval = np.max(f_all) + 0.1 * (f_all.max() - f_all.min())
        prev_night_obj_start = t_hist[prev_night_start].replace(hour=NIGHT_START, minute=0, second=0)
        prev_night_obj_end = t_hist[prev_night_end].replace(hour=MORNING_START, minute=0, second=0)
        night_obj_start = t_night[1].replace(hour=NIGHT_START, minute=0, second=0)  # FIXME t_night[0]
        night_obj_end = t_night[-1].replace(hour=MORNING_START, minute=0, second=0)
        ax.add_patch(Rectangle((prev_night_obj_start, 0), prev_night_obj_end - prev_night_obj_start, maxval, alpha=0.3, facecolor='C4'))
        ax.add_patch(Rectangle((night_obj_start, 0), night_obj_end - night_obj_start, maxval, alpha=0.3, facecolor='C4'))

        # normal band
        if field == 'FullScore':
            band_h = FIELD_TO_NORMAL_SCORE[field][1] - FIELD_TO_NORMAL_SCORE[field][0]
            band_x = (t_hist[prev_day_start] - timedelta(hours=2), t_night[-1].replace(hour=MORNING_START, minute=0, second=0))
            ax.add_patch(Rectangle((band_x[0], FIELD_TO_NORMAL_SCORE[field][0]), band_x[1] - band_x[0], band_h, alpha=0.3, facecolor='C2'))
            ax.plot([band_x[0], band_x[1]], [FIELD_TO_NORMAL_SCORE[field][0], FIELD_TO_NORMAL_SCORE[field][0]], '-', color='C2', alpha=0.6)
            ax.plot([band_x[0], band_x[1]], [FIELD_TO_NORMAL_SCORE[field][1], FIELD_TO_NORMAL_SCORE[field][1]], '-', color='C2', alpha=0.6)

        # imputed (only HR field gets dashed stem shafts)
        # shaft = 'C3--' if field == 'HRNum' else 'C3'
        shaft = 'C3'
        markerline, stemlines, baseline = ax.stem([imputed_t], [imputed], shaft, 'C3o', ' ')
        plt.setp(baseline, visible=False)
        ax.margins(x=0., tight=True)

        # prev day and night
        markerline, stemlines, baseline = ax.stem(t_hist[prev_day_start:prev_night_start],
                                                  f_hist[prev_day_start:prev_night_start], 'C3', 'C3o', ' ', alpha=0.9)
        plt.setp(baseline, visible=False)
        ax.margins(x=0., tight=True)
        markerline, stemlines, baseline = ax.stem(t_hist[prev_night_start:prev_night_end],
                                                  f_hist[prev_night_start:prev_night_end], 'C3', 'C3o', ' ', alpha=0.9)
        plt.setp(baseline, visible=False)
        ax.margins(x=0., tight=True)

        # day and night
        markerline, stemlines, baseline = ax.stem(t_hist[prev_night_end:], f_hist[prev_night_end:], 'C3', 'C3o', ' ', alpha=0.9)
        plt.setp(baseline, visible=False)
        markerline, stemlines, baseline = ax.stem(t_night, f_night, 'C3', 'C3o', ' ', alpha=0.9)
        plt.setp(baseline, visible=False)

        # save x axis range before
        # x_range = (plt.axis()[0], plt.axis()[1])
        x_range = (736698.3695023148, 736700.25)  # derived from testing, only works with a particular visit (see below)
        ax.set_xlim(x_range)

        # ax.stem([t_hist[prev_night_start - 1], t_hist[prev_night_end]], [0, 0], ' ', ' ', ' ')
        # ax.stem([t_hist[prev_night_end - 1], t_night[0]], [0, 0], ' ', ' ', ' ')
        # ax.stem([t_hist[-1], t_night[0]], [0, 0], ' ', ' ', ' ')

        ax.set_ylim([f_all.min() - 0.1 * (f_all.max() - f_all.min()), np.max(f_all) + 0.1 * (f_all.max() - f_all.min())])
        # ax.margins(y=0.5)
        ax.set_ylabel(FIELD_TO_PLOT[field], labelpad=FIELD_TO_YPAD[field], fontsize=ylabel_font_size, horizontalalignment='center')
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        # ax.spines['bottom'].set_visible(False)
        # plt.tight_layout(pad=0.8, w_pad=0.5)

        # if field == 'FullScore':
        #     ax.plot([t_hist[prev_day_start], t_night[-1]], [7, 7], '-', color='C2', alpha=0.6)
        if field == 'age':
            ax.set_yticks([60, 80])
        elif field == 'RespRateNum':
            ax.set_ylim(10, 32)
            ax.set_yticks([15, 25])

    # y variable: 0 until predicted night, then 1
    ax = axes[-1]

    # ax.plot([t_hist[prev_day_start], t_night[0]], [0, 0], 'grey', alpha=0.3)
    # ax.plot([t_hist[prev_night_start], t_hist[prev_night_end-1]], [0, 0], 'C4')
    # ax.plot([t_night[0], t_night[0]], [0, 1], 'C4')
    # ax.plot([t_night[0], t_night[-1]], [1, 1], 'C4')
    # ax.plot([t_night[-1], t_night[-1]], [1, 0], 'C4')
    # ax.set_ylim([-0.2, 1.2])

    # night labeling
    prev_night_obj_start = t_hist[prev_night_start].replace(hour=NIGHT_START, minute=0, second=0)
    prev_night_obj_end = t_hist[prev_night_end].replace(hour=MORNING_START, minute=0, second=0)
    night_obj_start = t_night[1].replace(hour=NIGHT_START, minute=0, second=0)  # t_night[0] ?
    night_obj_end = t_night[-1].replace(hour=MORNING_START, minute=0, second=0)
    ax.add_patch(Rectangle((prev_night_obj_start, 0), prev_night_obj_end - prev_night_obj_start, 1., alpha=0.3, facecolor='C4'))
    ax.add_patch(Rectangle((night_obj_start, 0), night_obj_end - night_obj_start, 1., alpha=0.3, facecolor='C4'))
    ax.text(prev_night_obj_start + (prev_night_obj_end - prev_night_obj_start) / 6.5, .4, 'LET SLEEP', fontsize=18, fontweight='bold')
    ax.text(night_obj_start + (night_obj_end - night_obj_start) / 5, .4, 'WAKE UP', fontsize=18, fontweight='bold')

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    # ax.get_yaxis().set_visible(False)
    ax.set_ylabel('Label', labelpad=FIELD_TO_YPAD['y'], fontsize=ylabel_font_size, horizontalalignment='center')
    ax.set_yticks(ticks=[])
    ax.xaxis.set_minor_locator(dates.HourLocator(byhour=[6, 2, 10, 14, 18, 22]))  # every 4 hours
    ax.xaxis.set_minor_formatter(dates.DateFormatter('%-I %p'))  # hours and minutes

    ax.tick_params(axis='x', which='major', bottom=False, top=False, labelbottom=False)
    # ax.xaxis.set_major_locator(dates.DayLocator(interval=1))  # every day
    # ax.xaxis.set_major_formatter(dates.DateFormatter('\nDay %d'))
    # ax.tick_params(which='major', labelsize=16)

    fig.tight_layout()


def tsplot(x, y, n=20, percentile_min=1, percentile_max=99, color='r', plot_mean=True, plot_median=False,
           # TODO rm and find other method
           line_color='k', **kwargs):
    # calculate the lower and upper percentile groups, skipping 50 percentile
    perc1 = np.percentile(y, np.linspace(percentile_min, 50, num=n, endpoint=False), axis=0)
    perc2 = np.percentile(y, np.linspace(50, percentile_max, num=n + 1)[1:], axis=0)

    if 'alpha' in kwargs:
        alpha = kwargs.pop('alpha')
    else:
        alpha = 1 / n
    # fill lower and upper percentile groups
    for p1, p2 in zip(perc1, perc2):
        plt.fill_between(x, p1, p2, alpha=alpha, color=color, edgecolor=None)

    if plot_mean:
        plt.plot(x, np.mean(y, axis=0), color=line_color)

    if plot_median:
        plt.plot(x, np.median(y, axis=0), color=line_color)

    return plt.gca()


def plot_cmp_hist(hist_and_night, field, col):
    vals_fn = get_f_values_where_mews_peaks(hist_and_night, col, field)  # only taken at the mews peaks

    vals_hist = hist_and_night[:, 0]
    vals_fn_hist = []
    for v in vals_hist:
        vals_fn_hist.append(v[:, col[field]])
    vals_fn_hist = np.concatenate(vals_fn_hist).astype(np.float32)

    vals_night = hist_and_night[:, 1]
    vals_fn_night = []
    for v in vals_night:
        vals_fn_night.append(v[:, col[field]])
    vals_fn_night = np.concatenate(vals_fn_night).astype(np.float32)

    vals_other_nights = np.concatenate([hist_and_night[:, 0], hist_and_night[:, 2]])  # hist and future
    vals_fn_onights = []
    for v in vals_other_nights:
        if len(v) == 0:
            continue
        vnight = v[are_nights(v[:, col['tod']])]
        vals_fn_onights.append(vnight[:, col[field]])
    vals_fn_onights = np.concatenate(vals_fn_onights).astype(np.float32)

    night_vals = vals_fn_night
    other_vals = vals_fn_onights  # to compare to

    tt, tp = stats.ttest_ind(other_vals, night_vals)
    print('{} ttest day vs night {} p={}'.format(field, tt, tp))

    plt.style.use('seaborn-white')
    plt.figure()
    plt.hist(other_vals, bins=20, density=True, alpha=0.5, color='C3')
    plt.hist(night_vals, bins=20, density=True, alpha=0.5, color='C4')
    plt.title('{} when sleeping unsafe (purple), values at other nights (orange)'.format(field))

    plt.show()


def plot_cmp_box(hist_and_night_fn, hist_and_night_tn, field, col, ax=None):
    vals_fn_night = []
    for v in hist_and_night_fn[:, 1]:
        vals_fn_night.append(v[:, col[field]])
    vals_fn_night = np.concatenate(vals_fn_night).astype(np.float32)

    vals_tn_night = []
    for v in hist_and_night_tn[:, 1]:
        vals_tn_night.append(v[:, col[field]])
    vals_tn_night = np.concatenate(vals_tn_night).astype(np.float32)

    night_vals = vals_fn_night
    other_vals = vals_tn_night  # to compare to

    # t-test
    tt, tp = stats.ttest_ind(night_vals, other_vals)
    print('{} CMP: FN({}) - TN({})'.format(field, len(night_vals), len(other_vals)))
    print('fp (M={}, STD={}), tp (M={}, STD={})'.format(night_vals.mean(), night_vals.std(), other_vals.mean(),
                                                        other_vals.std()))
    print('{} ttest day vs night {} p={}'.format(field, tt, tp))

    # kruskal-wallis h-test
    kt, kp = stats.kruskal(night_vals, other_vals)
    print('fp (Med={}, IQR={}), tp (Med={}, IQR={})'.format(np.median(night_vals), stats.iqr(night_vals),
                                                            np.median(other_vals), stats.iqr(other_vals)))
    print('{} kruskal-wallice h-test day vs night {} p={}'.format(field, kt, kp))

    plt.style.use('seaborn-white')
    plt.rc('font', size=12)
    if ax is None:
        fig = plt.figure(figsize=(6.2, 6.2))

    # create pandas dataframe so seaborn can plot the data
    # 2 columns: value, stability {Stable Sleeping|Unstable Sleeping}
    df = pd.DataFrame(zip(other_vals, ['Correctly classified\nas stable'] * len(other_vals)), columns=['value', 'stability'])
    df = df.append(
        pd.DataFrame(zip(night_vals, ['Misclassified\nas stable'] * len(night_vals)), columns=['value', 'stability']))
    if field == 'RespRateNum':
        df = df[df['value'] < 40]
    elif field == 'HRNum':
        df = df[df['value'] < 250]  # there was one too high lol
    elif field == 'TmprNum':
        df = df[df['value'] > 34]
        df = df[df['value'] < 46]
    # ax = seaborn.violinplot(x='stability', y='value', data=df, palette=['C3', 'C4'], scale='area')
    ax = seaborn.boxplot(x='stability', y='value', data=df, palette=['C3', 'C4'], width=0.3, fliersize=0.5,
                         boxprops=dict(alpha=.4), ax=ax, showfliers=False)

    # significance annotation
    if tp < 0.05:
        x1, x2 = 0, 1
        leg_len = np.max(df['value'].max() - df['value'].min()) * 0.08
        y, h, col = df['value'].max() + leg_len, leg_len, 'k'
        ax.plot([x1, x1, x2, x2], [y, y + h, y + h, y], lw=1.5, c=col)
        ax.text((x1 + x2) * .5, y + h, '*', ha='center', va='bottom', color=col, fontsize=14)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.set_xlabel('')
    ax.set_ylabel(FIELD_TO_PLOT[field])
    if field == 'FullScore':
        ax.set_yticks(np.arange(0, 11, 1))

    if ax is None:
        plt.show()

    return ax


def hr_threshold_plot(hist_and_night_fn, hist_and_night_tn, ax):
    gt7_hr_at_night = get_field_values(hist_and_night_fn, 'HRNum', col, at='night')
    tn_hr_at_night = get_field_values(hist_and_night_tn, 'HRNum', col, at='night')
    hr_thresholds = np.arange(90, 161, 10)
    fn_saved, tn_woken = [], []
    for th in hr_thresholds:
        fn_saved.append(np.sum(gt7_hr_at_night >= th) / len(gt7_hr_at_night))
        tn_woken.append(np.sum(tn_hr_at_night >= th) / len(tn_hr_at_night))
    fn_saved = np.array(fn_saved) * 100
    tn_woken = np.array(tn_woken) * 100

    # ax2 = ax.twinx()
    lns1 = ax.plot(hr_thresholds, fn_saved, 'C4o-', label='Awaken on potentially unstable nights (%)')
    lns2 = ax.plot(hr_thresholds, tn_woken, 'C3o-', label='Awaken on stable nights (%)')
    for th, fn_s, tn_w in zip(hr_thresholds, fn_saved, tn_woken):
        ax.annotate('{0:.2f}'.format(fn_s), (th + 1, fn_s + 1))
        ax.annotate('{0:.2f}'.format(tn_w), (th + 1, tn_w + 1))
    plt.xlim([85, 165])
    ax.set_ylim([-5, 100])
    # ax2.set_ylim([-5, 100])
    ax.set_xlabel('HR wakeup threshold (bpm)')
    ax.spines['top'].set_visible(False)
    # ax2.spines['top'].set_visible(False)

    ax.set_ylabel('Percentage awaken')
    # ax2.set_ylabel('Awaken on Stable Nights (%)')
    # ax.tick_params(axis='y', labelcolor='C4')
    # ax2.tick_params(axis='y', labelcolor='C3')

    lns = lns1 + lns2
    labs = [l.get_label() for l in lns]
    ax.legend(lns, labs, bbox_to_anchor=(0, 1.02, 1, 0.2), loc="lower left", mode='expand')
    # ax.legend(loc=0)
    # ax2.legend(loc=0)

    # ax.tick_params(axis='y', which='both', bottom=False, top=False, labelbottom=False)
    # ax2.tick_params(axis='y', which='both', bottom=False, top=False, labelbottom=False)
    # ax.set_yticks([])
    # ax2.set_yticks([])

    # ax.spines['left'].set_visible(False)
    # ax2.spines['right'].set_visible(False)
    ax.spines['right'].set_visible(False)
    # ax2.spines['left'].set_visible(False)


if __name__ == '__main__':
    # anal_fname = 'post_analysis/7+_old/7+_0.01.pickle'  # gt10[11] for stimplot (or [0], [8])
    with open(anal_fname, 'rb') as f:
        anal = pickle.load(f)

    hist_and_night_fn = extract_hist_and_night(anal, col, 'FNdata')
    gt7 = fn_w_mews_at_night_gt(hist_and_night_fn, col, 7)  # same as hist_and_night for 7+ classification obviously
    print(len(gt7))
    gt7_fs = get_field_values(gt7, 'FullScore', col, at='all').astype(np.float32)
    gt10 = fn_w_mews_at_night_gt(hist_and_night_fn, col, 10)
    gt9 = fn_w_mews_at_night_gt(hist_and_night_fn, col, 9)
    gt8 = fn_w_mews_at_night_gt(hist_and_night_fn, col, 8)
    gt7_fs_at_night = get_field_values(hist_and_night_fn, 'FullScore', col, at='night').astype(np.int32)

    # plot_fn_trajectory(gt7[0], 'FullScore', col)

    # ! stem plot - check couple lines above
    # plot_stem(gt10[11], ['HRNum', 'RespRateNum', 'TmprNum', 'BPNum', 'FullScore'], col)
    # plt.show()

    # ! jump plots
    # plt.rc('font', size=13)
    # fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(2, 2, figsize=(12.4, 12.4))
    # fig.tight_layout(pad=4.0)
    #
    # plot_multiple_trajectories(gt7, 'HRNum', col, ax=ax0)
    # plot_multiple_trajectories(gt7, 'RespRateNum', col, ax=ax1)
    # plot_multiple_trajectories(gt7, 'TmprNum', col, ax=ax2)
    # plot_multiple_trajectories(gt7, 'FullScore', col, ax=ax3)
    #
    # for lab, ax in zip(['A', 'B', 'C', 'D', 'E', 'F'], [ax0, ax1, ax2, ax3]):
    #     ax.text(-0.07, 1.07, lab, transform=ax.transAxes, size=24, weight='bold')
    #
    # plt.show()

    # plot_multiple_trajectories(gt7, 'BPNum', col)

    # TN needed for rest of the plots
    hist_and_night_tn = extract_hist_and_night(anal, col, 'TNdata')

    # cmp plots
    # plt.style.use('seaborn-white')
    # plt.rc('font', size=13)
    # fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(2, 2, figsize=(8.6, 8.6))
    # fig.tight_layout(pad=4.0)
    #
    # hist_that_mews(gt7_fs_at_night, density=False, ax=ax0)
    # plot_cmp_box(hist_and_night_fn, hist_and_night_tn, 'RespRateNum', col, ax=ax1)
    # plot_cmp_box(hist_and_night_fn, hist_and_night_tn, 'TmprNum', col, ax=ax2)
    # plot_cmp_box(hist_and_night_fn, hist_and_night_tn, 'HRNum', col, ax=ax3)
    #
    # for lab, ax in zip(['A', 'B', 'C', 'D', 'E', 'F'], [ax0, ax1, ax2, ax3]):
    #     ax.text(-0.07, 1.07, lab, transform=ax.transAxes, size=24, weight='bold')
    #
    # plt.show()

    # find out above what HR values we can wake up the unstable sleepers
    plt.rc('font', size=13)
    fig, axes = plt.subplots(2, 1, figsize=(6.2, 12.4))
    fig.tight_layout(pad=4.0)

    hr_threshold_plot(hist_and_night_fn, hist_and_night_tn, axes[0])
    fn_hr = get_field_values(hist_and_night_fn, 'HRNum', col, at='night')
    fn_rr = get_field_values(hist_and_night_fn, 'RespRateNum', col, at='night')

    # use HR thresholds of 110 and 120 and plot mews histogram with FP removed with those thresholds
    hr_thresholds = [120, 110]
    rr_thresholds = [20]
    mews_wo_hr_check = [get_field_values(hist_and_night_fn, 'FullScore', col, at='night')]
    mews_after_hr_check = [get_field_values(hist_and_night_fn[fn_hr < th], 'FullScore', col, at='night') for th in hr_thresholds]
    hist_all_that_mews(mews_wo_hr_check + mews_after_hr_check,
                       ['No threshold'] + ['{} bpm'.format(th) for th in hr_thresholds], ax=axes[1])
    print('all FN cases:', len(hist_and_night_fn))
    print('HR>=than:', [(th, (fn_hr >= th).sum()) for th in hr_thresholds])
    print('RR>=than:', [(th, (fn_rr >= th).sum()) for th in rr_thresholds])

    for lab, ax in zip(['A', 'B', 'C', 'D', 'E', 'F'], axes):
        ax.text(-0.07, 1.07, lab, transform=ax.transAxes, size=24, weight='bold')

    plt.show()
