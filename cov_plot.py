import numpy as np
import tables
from tensorflow.python.keras import Sequential
from tensorflow.python.keras.layers import Dense, LSTM, CuDNNLSTM, TimeDistributed, Dropout, BatchNormalization, SimpleRNN, Bidirectional
import matplotlib.pyplot as plt
import pandas as pd
import os, shutil
from prepare_seq_input import data_file_name, create_seq
from pprint import pprint
from sklearn.metrics import roc_curve, auc, confusion_matrix, precision_recall_curve, f1_score, matthews_corrcoef, roc_auc_score
from scipy.stats import norm
import copy
from matplotlib.patches import Rectangle
import pickle
from prepare_seq_input_prosp import create_seq_prosp, date_overlap
from collections.abc import Iterable
import seaborn as sn


dsize = 'small_'
mews_table = '/mnt/data/mews/tables/' + dsize + 'processed_mews2.csv'
input_fields = ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores']

mews = pd.read_csv(mews_table, parse_dates=['AlertDtm']).sort_values(['newid', 'AlertDtm'])
must_not_be_nan = list(set(mews.columns.values).intersection(set(input_fields)))
mews = mews.dropna(axis=0, how='any', subset=must_not_be_nan).reset_index(drop=True)

colname_map = {'age': 'Age', 'FullScore': 'MEWS', 'BPNum': 'BP', 'HRNum': 'HR', 'TmprNum': 'Tmpr', 'RespRateNum': 'RR'}
mews = mews[[c for c in colname_map]]
mews = mews.rename(columns=colname_map)

covmx = mews.cov()
sn.heatmap(covmx, annot=True, fmt='g')
plt.show()

print(mews)
