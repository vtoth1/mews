import shap
import numpy as np
import matplotlib.pyplot as plt
from lstm import *


if __name__ == '__main__':

    # config: model and data constants
    cfg = {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight7',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'],  # 'BMINum', varscores removed, FullScore
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [128] * 5,  # 256
        'min_seq_len': 8,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.6,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': 'w_dropout'  # but_regr
    }
    cfg_prosp = copy.deepcopy(cfg)
    cfg_prosp['test_data_ratio'] = 1.  # all into test and validation

    select_gpu(1)

    nepochs = 0
    patience = 3
    data_size = ''  # mini/small/medium/''
    model_path = 'models/'
    majority_class_rate = cfg['majority_class_rate']
    batch_size = cfg['batch_size']
    load_checkpoint = True
    overwrite_results = False

    base_folder = '/mnt/data/mews/seqs/'
    base_folder_prospective = '/mnt/data/mews/seqs_prosp/'
    dsize = '' if data_size == '' else data_size + '_'
    mews_table = 'data/' + dsize + 'processed_mews2.csv'
    mews_prospective = 'data/' + dsize + 'processed_mews3.csv'
    dfname = data_file_name(cfg, data_size)
    dfname_prosp = data_file_name(cfg_prosp, data_size)

    print('LOADING DATA')
    train_data_x, train_data_y, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies =\
        load_data(cfg, base_folder + dfname, majority_class_rate)
    _, _, valid_data_x_p, valid_data_y_p, test_data_x_p, test_data_y_p, _, _, _ = \
        load_data(cfg_prosp, base_folder_prospective + dfname_prosp, majority_class_rate)

    prosp_test_x = np.concatenate([valid_data_x_p, test_data_x_p])
    prosp_test_y = np.concatenate([valid_data_y_p, test_data_y_p])

    print('BUILDING MODEL')
    inp_dim = train_data_x.shape[2]
    out_dim = train_data_y.shape[1]
    pprint(cfg)
    model = build_model(cfg, inp_dim, out_dim, load_checkpoint)

    print('TRAINING')
    train(model, cfg, nepochs, patience, train_data_x, train_data_y, valid_data_x, valid_data_y, x_cols, y_cols)

    # actual SHAP analysis TODO test
    background = train_data_x[np.random.choice(train_data_x.shape[0], 1000, replace=False)]
    e = shap.DeepExplainer(model, background)
    shap_values = e.shap_values(test_data_x[1:50])
    shap.summary_plot(shap_values, test_data_x[1:50], plot_type="bar")
    shap.force_plot(e.expected_value, shap_values[0, :], test_data_x.iloc[0, ...])
    plt.show()

    # TODO same with prospective?
