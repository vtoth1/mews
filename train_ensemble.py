from lstm import *
import sys


if __name__ == '__main__':

    gpu_id = int(sys.argv[1])
    select_gpu(gpu_id)

    model_id = int(sys.argv[2])
    model_id_offset = int(sys.argv[3]) if len(sys.argv) >= 4 else 0
    more_suffix = sys.argv[4] + '_' if len(sys.argv) >= 5 else ''
    majority_class_rate = float(sys.argv[5]) if len(sys.argv) >= 6 else 0.9  # FIXME arbitrary, but needed for now
    min_seq_len = int(sys.argv[6]) if len(sys.argv) >= 7 else 8  # FIXME same

    # config: model and data constants
    cfg = {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight7',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'],  # 'BMINum', varscores removed, FullScore
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [32] * 5,  # 256
        'min_seq_len': min_seq_len,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': majority_class_rate,  # maximum allowed proportion of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': more_suffix + 'ens{}:{}'.format(gpu_id, model_id_offset + model_id)  # contains model id and gpu number
    }

    cfg_prosp = copy.deepcopy(cfg)
    cfg_prosp['test_data_ratio'] = 1.  # all into test and validation

    pprint(cfg)

    nepochs = 500
    patience = 3
    data_size = ''  # mini/small/medium/''
    model_path = 'models/'
    majority_class_rate = cfg['majority_class_rate']
    batch_size = cfg['batch_size']
    load_checkpoint = False
    overwrite_results = True

    base_folder = '/mnt/data/mews/seqs/'
    base_folder_prospective = '/mnt/data/mews/seqs_prosp/'
    dsize = '' if data_size == '' else data_size + '_'
    mews_table = '/mnt/data/mews/tables/' + dsize + 'processed_mews2.csv'
    mews_prospective = '/mnt/data/mews/tables/' + dsize + 'processed_mews3.csv'
    dfname = data_file_name(cfg, data_size)
    dfname_prosp = data_file_name(cfg_prosp, data_size)
    if not os.path.isfile(base_folder + dfname):
        print('CREATING DATA')
        create_seq(cfg, mews_table, data_size, base_folder)
    if not os.path.isfile(base_folder_prospective + dfname_prosp):
        print('CREATING PROSPECTIVE DATA')
        create_seq(cfg_prosp, mews_prospective, data_size, base_folder_prospective)

    print('LOADING DATA')
    train_data_x, train_data_y, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies = \
        load_data(cfg, base_folder + dfname, shuffle=True)  # normies are needed for testing, no other reason to load this data tho

    # prospective
    # _, _, valid_data_x_p, valid_data_y_p, test_data_x_p, test_data_y_p, _, _, _ = \
    #     load_data(cfg_prosp, base_folder_prospective + dfname_prosp)
    #
    # prosp_test_x = np.concatenate([valid_data_x_p, test_data_x_p])
    # prosp_test_y = np.concatenate([valid_data_y_p, test_data_y_p])

    # train a linear regression model: use only last value of the sequence, flatten seq, train and test linear model
    print('TRAINING LINEAR MODEL')
    flat_train_data_x = train_data_x[:, -1, :]
    flat_test_data_x = test_data_x[:, -1, :]
    flat_train_data_y, flat_test_data_y = train_data_y.ravel(), test_data_y.ravel()

    # build and train linear model
    from sklearn.linear_model import LogisticRegression
    lr_model = LogisticRegression(class_weight=cfg['class_weight'], n_jobs=4).fit(flat_train_data_x, flat_train_data_y)

    print('BUILDING MODEL')
    inp_dim = 27  # train_data_x.shape[2]
    out_dim = 1  # train_data_y.shape[1]
    pprint(cfg)
    model = build_model(cfg, inp_dim, out_dim, load_checkpoint, force_load=False)

    print('TRAINING')
    train(model, cfg, nepochs, patience, train_data_x, train_data_y, valid_data_x, valid_data_y, x_cols, y_cols)

    print('TESTING BEGINS')
    test_folder = 'results/' + model_name(cfg) + '/'
    if overwrite_results:
        shutil.rmtree(test_folder, ignore_errors=True)
        os.mkdir(test_folder)
    thresholds_, lin_thresholds = test_sleeping(model, cfg, test_data_x, test_data_y, x_cols, y_cols, normies, test_folder, overwrite_results,
                                                linear_stuff={'model': lr_model, 'flat_x': flat_test_data_x, 'flat_y': flat_test_data_y})

    # get thresholds from ensemble_testing
    thresholds = 1. - np.array([0.9949689, 0.99110633, 0.9859783])  # reverse it back
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! thresholds diff:', thresholds - thresholds_)
    print('THRESHOLDS:', thresholds)

    print('PROSPECTIVE TESTING BEGINS')
    test_sleeping_prosp(model, cfg, cfg_prosp, x_cols, y_cols, normies, 'results/' + model_name(cfg) + '/',
                        overwrite_results, thresholds, base_folder_prospective, data_size, mews_prospective)

    print('PROSPECTIVE LINEAR TESTING BEGINS')
    test_sleeping_prosp_linear(lr_model, cfg, cfg_prosp, x_cols, y_cols, normies, 'results/' + model_name(cfg) + '/',
                               overwrite_results, lin_thresholds, base_folder_prospective, data_size, mews_prospective)

