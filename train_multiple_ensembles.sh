#!/bin/bash


if [ $# -lt 1 ]; then
    echo 1>&2 "$0: not enough arguments"
    exit 2
fi

gpu_id="$1"

./train_ensemble.sh $1 10 0
./train_ensemble.sh $1 10 0 dropout
./train_ensemble.sh $1 10 0 batchnorm
./train_ensemble.sh $1 10 0 droupout_batchnorm
./train_ensemble.sh $1 10 0 rnn
./train_ensemble.sh $1 10 0 rnn_dropout
./train_ensemble.sh $1 10 0 rnn_dropout_batchnorm
