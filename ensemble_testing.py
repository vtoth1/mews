import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc, confusion_matrix
import pickle
from plot_test import load_test_results
import copy
from lstm import clin_appl_auc, clin_appl_idx, plot_roc, plot_tn_vs_fn, model_name
import sys, os
import scipy.stats as st


if __name__== '__main__':

    cfg = {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight7',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'],  # 'BMINum', varscores removed, FullScore
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [256] * 5,  # 256
        'min_seq_len': 8,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.6,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': 'dropout_batchnorm_final_ens{}:{}'  # specify suffix below
    }

    cfg_prosp = copy.deepcopy(cfg)
    cfg_prosp['test_data_ratio'] = 1.  # all into test and validation

    n_gpu = 2
    n_model = 100  # per gpu FIXME
    model_fn_ratios = np.array([1 / 20000, 1 / 10000, 2 / 10000])

    # get predictions from all models
    all_preds = []
    all_desired = []
    all_prosp_preds = []
    all_prosp_desired = []
    failed_to_load = 0
    for gpu_i in range(n_gpu):
        for model_i in range(n_model):
            cfg_i = copy.deepcopy(cfg)
            cfg_prosp_i = copy.deepcopy(cfg_prosp)
            cfg_i['name_suffix'] = cfg_i['name_suffix'].format(gpu_i, model_i)
            cfg_prosp_i['name_suffix'] = cfg_prosp_i['name_suffix'].format(gpu_i, model_i)

            # if not os.path.isfile('results/{}/test{}.pickle'.format(model_name(cfg_i), '')) or \
            #     not os.path.isfile('results/{}/test{}.pickle'.format(model_name(cfg_prosp_i), '_prosp'))

            print('MODEL', cfg_i['name_suffix'])

            try:
                res = load_test_results(cfg_i, prospective=False)
                resp = load_test_results(cfg_prosp_i, prospective=True)

                all_preds.append(res['predsinv'])
                all_desired.append(res['desiredinv'])
                all_prosp_preds.append(resp['predsinv'])
                all_prosp_desired.append(resp['desiredinv'])
            except:
                print('Could not load {}/{}'.format(gpu_i, model_i))
                failed_to_load += 1

    print('FAILED TO LOAD:', failed_to_load)
    print('N MODELS:', len(all_preds))

    # assert that desired is the same for all resultsnp.array(all_preds)
    assert np.all([np.all(all_desired[i] == all_desired[i + 1]) for i in range(len(all_desired) - 1)])
    assert np.all([np.all(all_prosp_desired[i] == all_prosp_desired[i + 1]) for i in range(len(all_prosp_desired) - 1)])

    # statistics about assembly predictions
    all_preds = np.array(all_preds)
    std = np.std(all_preds, axis=0)
    print('STD of prediction between models:', std.mean())

    # confidence interval on AUC
    aucs = []
    for predsinv in all_preds:
        fprinv, tprinv, thresholdsinv = roc_curve(all_desired[0], predsinv)
        aucs.append(auc(fprinv, tprinv))
    aucs = np.sort(aucs)
    conf_interval = st.t.interval(0.95, len(aucs) - 1, loc=np.mean(aucs), scale=st.sem(aucs))
    print("Retrospective confidence interval/mean:", conf_interval, '/', np.mean(aucs))

    # avg across model predictions
    predsinv = all_preds.mean(axis=0)
    desiredinv = all_desired[0]  # worth it to gather a thousand of this, right
    n = len(predsinv)

    # false positives, true positives; copied from lstm.py/test_sleep
    fprinv, tprinv, thresholdsinv = roc_curve(desiredinv, predsinv)  # inverted preds and desired
    roc_auc_inv = auc(fprinv, tprinv)

    tpsinv = np.zeros(len(thresholdsinv))
    fpsinv = np.zeros(len(thresholdsinv))
    for i, th in enumerate(thresholdsinv):
        tpsinv[i] = np.sum(((predsinv) > th) & ((desiredinv) == 1))  # removed "1 - " from here,
        fpsinv[i] = np.sum(((predsinv) > th) & ((desiredinv) == 0))  # it's already inverse preds and desired

    # look out, fpsinv/n != fprinv; fprinv is normalized by the number of fp, not by n (all)
    # fpsinv and fprinv still have to have an index correspondence
    # clin_auc = clin_appl_auc(fpsinv, n, fprinv, tprinv, 2 / 10000)
    # special measure: tprinv at the edge of the clinically applicable
    # clin_idx = clin_appl_idx(fpsinv, n, 2 / 10000)
    # tprinv_at_clin_edge = tprinv[clin_idx]

    # create main figure
    fig, axes = plt.subplots(2, 3, figsize=(12.4, 8.))

    # from here, just copied plot_test.py
    nfpinv = np.max(fpsinv)  # at max

    model_fn_ratios = np.array([1 / 20000, 1 / 10000, 2 / 10000])  # not really false ratio, as it is the ratio of the whole
    model_idx = np.array([np.argmin(np.abs(fpsinv / len(predsinv) - r)) for r in model_fn_ratios])

    print('clinical auc:', clin_appl_auc(fpsinv, n, fprinv, tprinv, model_fn_ratios[-1]))
    print('regular auc:', auc(fprinv, tprinv))
    print('THRESHOLDS:', [th for th in thresholdsinv[model_idx]])
    print('retro fpr at models:', fprinv[model_idx])

    plot_roc(fprinv, tprinv, thresholdsinv, model_idx, n, nfpinv, ax=axes[0, 0])
    plot_tn_vs_fn(len(predsinv), tpsinv, fpsinv, thresholdsinv, model_idx, sys.stdout, axes=[axes[0, 1], axes[0, 2]], legend=True)

    # prospective
    # confidence interval on AUC
    retro_thresholdsinv = thresholdsinv  # need it below
    aucs = []
    for predsinv in all_prosp_preds:
        fprinv, tprinv, thresholdsinv = roc_curve(all_prosp_desired[0], predsinv)
        aucs.append(auc(fprinv, tprinv))
    aucs = np.sort(aucs)
    conf_interval = st.t.interval(0.95, len(aucs) - 1, loc=np.mean(aucs), scale=st.sem(aucs))
    print("Prospective confidence interval/mean:", conf_interval, '/', np.mean(aucs))

    predsinv = np.array(all_prosp_preds).mean(axis=0)
    desiredinv = all_prosp_desired[0]
    n = len(predsinv)

    # false positives, true positives
    fprinv, tprinv, thresholdsinv = roc_curve(desiredinv, predsinv)  # inverted preds and desired
    roc_auc_inv = auc(fprinv, tprinv)

    tpsinv = np.zeros(len(thresholdsinv))
    fpsinv = np.zeros(len(thresholdsinv))
    for i, th in enumerate(thresholdsinv):
        tpsinv[i] = np.sum(((predsinv) > th) & ((desiredinv) == 1))
        fpsinv[i] = np.sum(((predsinv) > th) & ((desiredinv) == 0))

    nfpinv = np.max(fpsinv)

    # you need to use the same thresholds as for the models above
    # find the closest thresholds in the prospective thresholds array
    model_idx_prosp = np.array([np.argmin(np.abs(thresholdsinv - th)) for th in retro_thresholdsinv[model_idx]])

    plot_roc(resp['fprinv'], resp['tprinv'], resp['thresholdsinv'], model_idx_prosp, n, nfpinv, ax=axes[1, 0])
    plot_tn_vs_fn(len(resp['predsinv']), resp['tpsinv'], resp['fpsinv'], resp['thresholdsinv'], model_idx_prosp,
                  sys.stdout, axes=[axes[1, 1], axes[1, 2]])

    plt.show()
