import os, shutil, sys, json
from lstm import *
from bunch import CONFIGS
import copy
from glob import glob
import traceback


if __name__ == '__main__':

    bunch = CONFIGS  # only 7+

    gpu_id = 0
    if len(sys.argv) > 1:
        gpu_id = int(sys.argv[1])

    select_gpu(gpu_id)

    bunch_to_go_through = bunch[gpu_id::2] if gpu_id >= 0 else bunch
    for cfg in bunch_to_go_through:  # assumes 2 gpus only

        results_path = 'results/' + model_name(cfg) + '/'

        if os.path.isdir(results_path) and len([f for f in os.listdir(results_path)]) > 0:
            print('SKIPPED:', model_name(cfg))
            continue  # already trained and tested

        print('\n\n\n-------------------------------------------------------------------------------')
        try:
            cfg_prosp = copy.deepcopy(cfg)
            cfg_prosp['test_data_ratio'] = 1.  # all into test and validation

            nepochs = 50
            patience = 3
            data_size = ''  # mini/small/medium/''
            model_path = 'models/'
            majority_class_rate = cfg['majority_class_rate']
            batch_size = cfg['batch_size']
            load_checkpoint = False
            overwrite_results = True

            if os.path.isfile('models/{}.h5'.format(model_name(cfg))):
                print('TRAINING SKIPPED:', model_name(cfg))
                nepochs = 0

            base_folder = '/mnt/data/mews/seqs/'
            base_folder_prospective = '/mnt/data/mews/seqs_prosp/'
            dsize = '' if data_size == '' else data_size + '_'
            mews_table = 'data/' + dsize + 'processed_mews2.csv'
            mews_prospective = 'data/' + dsize + 'processed_mews3.csv'
            dfname = data_file_name(cfg, data_size)
            dfname_prosp = data_file_name(cfg_prosp, data_size)
            if not os.path.isfile(base_folder + dfname):
                print('CREATING DATA')
                create_seq(cfg, mews_table, data_size, base_folder)
            if not os.path.isfile(base_folder_prospective + dfname_prosp):
                print('CREATING PROSPECTIVE DATA')
                create_seq(cfg_prosp, mews_prospective, data_size, base_folder_prospective)

            print('LOADING DATA')
            train_data_x, train_data_y, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies = \
                load_data(cfg, base_folder + dfname)

            print('BUILDING MODEL')
            inp_dim = train_data_x.shape[2]
            out_dim = train_data_y.shape[1]
            pprint(cfg)
            model = build_model(cfg, inp_dim, out_dim, load_checkpoint)

            print('TRAINING')
            train(model, cfg, nepochs, patience, train_data_x, train_data_y, valid_data_x, valid_data_y, x_cols, y_cols)

            print('TESTING BEGINS')
            test_folder = 'results/' + model_name(cfg) + '/'
            if overwrite_results:
                shutil.rmtree(test_folder, ignore_errors=True)
                os.mkdir(test_folder)
            thresholds, lin_thresholds = test_sleeping(model, cfg, test_data_x, test_data_y, x_cols, y_cols, normies,
                                                       test_folder, overwrite_results)
            print('THRESHOLDS:', thresholds)

            print('PROSPECTIVE TESTING BEGINS')
            test_sleeping_prosp(model, cfg, cfg_prosp, x_cols, y_cols, normies, 'results/' + model_name(cfg) + '/',
                                overwrite_results, thresholds, base_folder_prospective, data_size, mews_prospective)
            plt.show()

        except BaseException as e:
            print('ERROR:', e)
            traceback.print_exc()
            # break  FIXME


