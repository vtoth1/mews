from lstm import *
from sklearn.metrics import average_precision_score, roc_auc_score, roc_curve
import csv
import pandas
import gc
from numba import cuda
from tensorflow.python.keras.backend import set_session
from tensorflow.python.keras.backend import clear_session
from tensorflow.python.keras.backend import get_session
import tensorflow as tf
import sys


def assess_performance(cfg, preds, desired):
    # returns with different performance measurements - loss, precision, auc
    loss = np.mean(np.power(desired - preds, 2))  # l2
    # acc = balanced_accuracy_score(desired, preds)  # for now
    prec = average_precision_score(desired, preds)
    roc_auc = roc_auc_score(desired, preds)
    # fpr, tpr, t = roc_curve(desired, preds)
    # plt.plot(fpr, tpr)
    # plt.show()

    return [loss, prec, roc_auc]


# def reset_keras(model, gpu_id):
#     try:
#         del model
#     except:
#         pass
#     print('COLLECT', gc.collect())


if __name__ == '__main__':

    cfg = {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight7',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'],  # 'BMINum', varscores removed, FullScore
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [256] * 5,  # 256
        'min_seq_len': 8,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.6,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': ''  # here in input importance, leave this empty, provide suffixes in input_importance.sh
    }

    gpu_id = -1
    select_gpu(gpu_id)

    # we need to run this script multiple times, otherwise GPU memory gets filled up
    R = int(sys.argv[1]) if len(sys.argv) > 1 else None  # run id, each id corresponds to an input to test
    ens_gpu = int(sys.argv[2]) if len(sys.argv) > 2 else None
    ens_num = int(sys.argv[3]) if len(sys.argv) > 3 else None
    cfg['name_suffix'] = sys.argv[4] + '_' if len(sys.argv) > 4 else ''  # copied from train_ensemble

    ntests_per_run = 2  # number of times each input gets randomized (in one run of this script); 1 or 2, max 3
    fresh_start = False  # brand new csv to store performance, append otherwise

    # select a model from the ensemble
    use_ensemble = True
    if use_ensemble:
        ens_suffix = 'ens{}:{}'.format(ens_gpu, ens_num)
        # ens suffix is always at the end, separated by a '_' if anything else is before it (not really)
        # cfg['name_suffix'] = ens_suffix if cfg['name_suffix'] == '' else cfg['name_suffix'] + '_' + ens_suffix
        # the model_name() function returns model names like __ens{}:{} when no other suffix
        cfg['name_suffix'] += ens_suffix
    print(model_name(cfg))

    cfg_prosp = copy.deepcopy(cfg)
    cfg_prosp['test_data_ratio'] = 1.  # all into test and validation

    base_folder = '/mnt/data/mews/seqs/'
    data_size = ''  # mini/small/medium/''
    model_path = 'models/'
    majority_class_rate = 0.6  # maximum allowed amount of the majority class

    base_folder_prospective = '/mnt/data/mews/seqs_prosp/'
    dsize = '' if data_size == '' else data_size + '_'
    mews_table = 'data/' + dsize + 'processed_mews2.csv'
    mews_prospective = 'data/' + dsize + 'processed_mews3.csv'
    dfname = data_file_name(cfg, data_size)
    dfname_prosp = data_file_name(cfg_prosp, data_size)
    if not os.path.isfile(base_folder + dfname):
        print('CREATING DATA')
        create_seq(cfg, mews_table, data_size, base_folder)
    if not os.path.isfile(base_folder_prospective + dfname_prosp):
        print('CREATING PROSPECTIVE DATA')
        create_seq(cfg_prosp, mews_prospective, data_size, base_folder_prospective)

    print('LOADING DATA')
    train_data_x, train_data_y, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies = \
        load_data(cfg, base_folder + dfname)
    _, _, valid_data_x_p, valid_data_y_p, test_data_x_p, test_data_y_p, _, _, _ = \
        load_data(cfg_prosp, base_folder_prospective + dfname_prosp)

    test_data_x_p = np.concatenate([valid_data_x_p, test_data_x_p])
    test_data_y_p = np.concatenate([valid_data_y_p, test_data_y_p])

    print('BUILDING MODEL')
    inp_dim = train_data_x.shape[2]
    out_dim = train_data_y.shape[1]
    pprint(cfg)
    model = build_model(cfg, inp_dim, out_dim, load_checkpoint=True, force_load=True)

    # first order analysis: go through each input variable, randomize values in that column, test the model
    csv_path = 'importance/{}.csv'.format(model_name(cfg))

    # handle onehot
    regular_cols = [c for c in x_cols if 'onehot' not in c]
    onehot_cols = [c for c in x_cols if 'onehot' in c]
    first_digits = [[char_i for char_i, char in enumerate(c) if char.isdigit()][0] for c in x_cols if 'onehot' in c]
    revised_cols = {c: [x_cols.index(c)] for c in regular_cols}  # 'col name': [corresponding indices]
    for onehot, dig_i in zip(onehot_cols, first_digits):
        if onehot[:dig_i] not in revised_cols:
            revised_cols[onehot[:dig_i]] = []
        revised_cols[onehot[:dig_i]].append(x_cols.index(onehot))
    revised_cols = {col: np.array(idx) for col, idx in revised_cols.items()}  # turn to numpy

    # remove EOS
    if 'EOS' in revised_cols:
        del revised_cols['EOS']

    # to check the number of runs that assess_inp_importance.sh requires to run (use 'small' data for this, it's faster)
    if R is None:
        print('#RUNS: ', int(len(revised_cols) + 1))  # +1 is baseline
        exit(0)
    r = 0  # run iterator

    csv_exists = os.path.isfile(csv_path)
    fmode = 'wt' if R == 0 and fresh_start else 'a'
    with open(csv_path, fmode) as logfile:
        writer = csv.writer(logfile)
        if fmode == 'wt' or not csv_exists:
            writer.writerow(['input', 'loss', 'prec', 'auc', 'prosp_loss', 'prosp_prec', 'prosp_auc'])

        if r == R:
            # first predict without randomization to have a baseline
            print('BASELINE')
            for t in range(1):  # it prints the same results, no stochasticity here
                print('TEST #{}'.format(t))

                # retrospective
                preds, desired = predict(model, cfg, test_data_x, test_data_y, 128)
                preds = 1 - preds  # post fix, flipping FN to FP
                desired = 1 - desired
                retro = assess_performance(cfg, preds, desired)

                # prospective
                preds, desired = predict(model, cfg, test_data_x_p, test_data_y_p, 128)
                preds = 1 - preds  # post fix, flipping FN to FP
                desired = 1 - desired
                prosp = assess_performance(cfg, preds, desired)

                writer.writerow(['BASELINE'] + retro + prosp)

        r += 1

        # compute the end of padding, so we don't shuffle stuff into it (or shuffle zeroes in elsewhere)
        no_pad_test = not_padding_from(test_data_x)
        no_pad_prosp = not_padding_from(test_data_x_p)

        # predict for each column, randomizing their values
        for col, idx in revised_cols.items():
            # so the shuffling below is performed on the right axis
            # for onehot, idx len > 1, and we shuffle
            idx = idx if len(idx) > 1 else idx[0]

            if r == R:
                print('COLUMN:', col)
                # copying once per input var is enough
                cp_test_data_x = np.copy(test_data_x)
                cp_test_data_x_p = np.copy(test_data_x_p)

                for t in range(ntests_per_run):
                    print('TEST #{}'.format(t))

                    # shuffle data, but only the non-padding part
                    # cp_test_data_x[:, :, idx] = np.random.random(cp_test_data_x[:, :, idx].shape)
                    # cp_test_data_x_p[:, :, idx] = np.random.random(cp_test_data_x_p[:, :, idx].shape)
                    if col == 'age':
                        # special case, just randomize one value per visit and overwrite the age
                        # this way the age value is random, yet it's not jumping up-and-down
                        for s, p in zip(cp_test_data_x[:, :, idx], no_pad_test):
                            s[p:] = np.random.random(1)  # replace with a single value
                        for s, p in zip(cp_test_data_x_p[:, :, idx], no_pad_prosp):
                            s[p:] = np.random.random(1)  # slicing and overwriting s works here, but not below
                    else:  # shuffle for all the others
                        # if idx refers to multiple (onehot) indices, then onehot rows are mixed = onehot nature remains
                        # because shuffle() shuffles only the major/outer axis
                        # but it turns out that the outer axis is the time (if sliced like cp_test_data_x[i, p:, idx])
                        # s[p:] slicing would give the right dimensions, but it cannot be overwritten
                        # so cp_test_data_x has to be transposed first, then back after permutation
                        # s[p:] works for single columns, not onehot ones
                        for i, (s, p) in enumerate(zip(cp_test_data_x[:, :, idx], no_pad_test)):
                            cp_test_data_x[i, p:, idx] = np.random.permutation(cp_test_data_x[i, p:, idx].T).T
                            # s[p:] = np.random.permutation(s[p:])  # this won't work, don't even try
                        for i, (s, p) in enumerate(zip(cp_test_data_x_p[:, :, idx], no_pad_prosp)):
                            cp_test_data_x_p[i, p:, idx] = np.random.permutation(cp_test_data_x_p[i, p:, idx].T).T
                            # np.random.shuffle(s[p:])  # nah-ah

                    # retrospective
                    preds, desired = predict(model, cfg, cp_test_data_x, test_data_y, 128)
                    preds = 1 - preds  # post fix, flipping FN to FP
                    desired = 1 - desired
                    retro = assess_performance(cfg, preds, desired)

                    # prospective
                    preds, desired = predict(model, cfg, cp_test_data_x_p, test_data_y_p, 128)
                    preds = 1 - preds  # post fix, flipping FN to FP
                    desired = 1 - desired
                    prosp = assess_performance(cfg, preds, desired)

                    writer.writerow([col] + retro + prosp)

            r += 1

    # TODO second order: go through each pair of input variables ...
