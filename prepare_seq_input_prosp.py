import tables
import pandas as pd
import numpy as np
from preprocess_mews import npgroupby
import matplotlib.pyplot as plt
import copy
import pickle
# from lstm import *
import time


def data_file_name(cfg, data_size):

    min_seq_len = cfg['min_seq_len']
    input_fields = cfg['input']
    predicted_fields = cfg['predict']
    max_seq_len = cfg['seq_len']
    task = cfg['task']
    test_data_ratio = cfg['test_data_ratio']
    rand_scheme = cfg['test_rand_scheme']
    augm_step = cfg['data_augmentation']['step']
    augm_skip = cfg['data_augmentation']['skip']

    dsize = '' if data_size == '' else data_size + '_'
    augm = '{}-{}-{}-{}'.format(augm_step[0], augm_step[1], augm_skip[0], augm_skip[1])
    return '{}{}_{}_{}_{}_test{}_augm{}_{}-{}seqs.h5'.format(dsize, task, rand_scheme, '+'.join(input_fields),
                                                             '+'.join(predicted_fields), test_data_ratio, augm,
                                                             min_seq_len, max_seq_len)


def date_overlap(ds1, de1, ds2, de2):  # ds1 or de1 in range [ds2, de2)
    return ds2.tz_localize(None) <= ds1.tz_localize(None) < de2.tz_localize(None) or \
           ds2.tz_localize(None) <= de1.tz_localize(None) < de2.tz_localize(None)


# rand_scheme can be: 1) rand choice within samples, 2) rand choose last samples in each visit, 3) choose whole visits
#   1) 'samples', 2) 'last_sample', 3) 'visits'
# returns bool for each sample, True means test
def rand_choose_test(nsample, rand_scheme, test_ratio):
    if rand_scheme == 'samples':
        return np.random.rand(nsample) < test_ratio
    elif rand_scheme == 'last_sample':
        ret = np.zeros(nsample, dtype=bool)
        ret[-1] = np.random.rand() < test_ratio
        return ret
    elif rand_scheme == 'visits':
        ret = np.zeros(nsample, dtype=bool)
        return (ret + (1 if np.random.rand() < test_ratio else 0)).astype(bool)
    raise ValueError


# training set history should not include test set predictions
# obviously predictions should not overlap either, but that should already have been dealt with
# test history can contain anything before the prediction, train history only back until the prev test prediction
def adjust_history(are_test, pred_boundaries, hist_boundaries):
    latest_test_boundary_end = 0
    for i, (is_test, pred_boundary) in enumerate(zip(are_test, pred_boundaries)):
        if is_test:
            latest_test_boundary_end = pred_boundary[1]
        else:
            hist_boundaries[i][0] = latest_test_boundary_end

    return hist_boundaries


MORNING_START, NIGHT_START = 6, 22
OVER_PRED_AHEAD = (MORNING_START - NIGHT_START) % 24  # when using daytime data to train, the amount of hours to predict over


def are_nights(tod):  # np array of time of day values
    return (tod >= NIGHT_START) | (tod < MORNING_START)


def when_last_night(sample, col, min_seq_len):
    tod = sample[:, col['tod']]

    # find last night
    nights = are_nights(tod)
    if not np.any(nights):
        return None, None

    end_of_last_night = max(loc for loc, val in enumerate(tod) if val >= NIGHT_START or val < MORNING_START)
    if end_of_last_night < min_seq_len:
        return None, None

    start_of_last_night = end_of_last_night
    while nights[start_of_last_night - 1] and start_of_last_night > 0:
        start_of_last_night -= 1
    if start_of_last_night < min_seq_len:
        return None, None

    return start_of_last_night, end_of_last_night


def when_all_nights(tod):
    nights = are_nights(tod)
    if not np.any(nights):
        return []

    night_boundaries = []
    prev_is_night = False
    for i, is_night in enumerate(nights):
        if is_night and not prev_is_night:  # dawn
            night_boundaries.append([i, i + 1])
        elif is_night and prev_is_night:  # still night
            night_boundaries[-1][1] += 1
        prev_is_night = is_night

    return night_boundaries


def when_all_nights_and_days(tod):
    nights = are_nights(tod)
    if not np.any(nights):
        return [], []

    day_boundaries = []
    night_boundaries = []
    prev_is_night = False
    for i, is_night in enumerate(nights):
        if is_night and not prev_is_night:  # dusk
            night_boundaries.append([i, i + 1])
        elif is_night and prev_is_night:  # still night
            night_boundaries[-1][1] += 1

        # any daytime start a new pair of boundaries, close it after OVER_PRED_AHEAD hours
        if not is_night:
            # daytime sequence starts at i now
            seq_t = 0  # accumulated length of the sequence
            for j in range(i + 1, len(tod)):
                dt = (tod[j] - tod[i]) % 24  # mod 24 works only if the gaps are not higher than 24 (should be true)
                seq_t += dt
                if seq_t > OVER_PRED_AHEAD:  # exceeded the amount of time to predict ahead
                    # finish and don't include record j (upper boundary is non-inclusive)
                    day_boundaries.append([i, j])
                    break
            # note: if the future sequence time does not exceed OVER_PRED_AHEAD, no day sample is added

        prev_is_night = is_night

    return night_boundaries, day_boundaries


# to standardize across sample extractor functions, how a sample should look like
# add newid (visitid) and sequence start and end dates as extra parameters to the tuple
# end dates are offset by 1 minute so they can be used as non-inclusive upper boundary (x < end_date)
# assemble + sample = assample; smart huh?
def assample(visit, col, start_i, end_i, pred_start_i, pred_end_i, hist, pred, interest, is_test):
    return hist, pred, interest, is_test, visit[0, col['newid']],\
           visit[start_i, col['AlertDtm']], visit[end_i - 1, col['AlertDtm']] + pd.DateOffset(minutes=1),\
           visit[pred_start_i, col['AlertDtm']], visit[pred_end_i - 1, col['AlertDtm']] + pd.DateOffset(minutes=1)


def regular_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    predicted_field_indices = [col[f] for f in predicted_fields]

    # determine the samples (prev values), then which is train/test, then adjust history
    pred_boundaries = np.arange(min_seq_len - 1, len(visit))
    pred_boundaries = [[s, e] for s, e in zip(pred_boundaries, pred_boundaries + 1)]
    are_test = rand_choose_test(len(pred_boundaries), rand_scheme, test_data_ratio)

    # by default from beginning until pred (including pred, later masking out everything but time variables)
    hist_boundaries = [[0, pred_s + 1] for pred_s, pred_e in pred_boundaries]
    hist_boundaries = adjust_history(are_test, pred_boundaries, hist_boundaries)

    samples = []
    except_time_mask = np.ones(len(col), dtype=bool)
    except_time_mask[[col[c] for c in ['td', 'tod']]] = False
    for i, (is_test, pred_boundary, hist_boundary) in enumerate(zip(are_test, pred_boundaries, hist_boundaries)):
        if hist_boundary[1] - hist_boundary[0] < min_seq_len:
            continue
        sample = visit[hist_boundary[0]:hist_boundary[1]].copy()
        pred = np.array(sample[-1, predicted_field_indices], dtype=np.float32)  # first take the predicted values, then mask
        interest = int(sample[-1, col['FullScore']] > 6. and sample[-2, col['FullScore']] < 7.)
        sample[-1, except_time_mask] = 0.  # zero out all but time
        hist = sample
        samples.append(assample(visit, col, hist_boundary[0], hist_boundary[1], pred_boundary[0], pred_boundary[1],
                                hist, pred, interest, is_test))

    return samples


def overnightX_samples(threshold, visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    tod = visit[:, col['tod']]

    # find nights
    pred_boundaries = when_all_nights(tod)
    if len(pred_boundaries) == 0:
        return []
    # history before night at least min_seq_len long
    pred_boundaries = [[s, e] for s, e in pred_boundaries if s >= min_seq_len]
    if len(pred_boundaries) == 0:
        return []

    # choose test samples, cover history
    are_test = rand_choose_test(len(pred_boundaries), rand_scheme, test_data_ratio)
    hist_boundaries = [[0, pred_s] for pred_s, pred_e in pred_boundaries]
    hist_boundaries = adjust_history(are_test, pred_boundaries, hist_boundaries)

    samples = []
    for i, (is_test, pred_boundary, hist_boundary) in enumerate(zip(are_test, pred_boundaries, hist_boundaries)):
        hist = visit[hist_boundary[0]:hist_boundary[1], :].copy()
        pred = np.array([float(np.any(visit[pred_boundary[0]:pred_boundary[1], col['FullScore']] >= threshold))], dtype=np.float32)
        interest = int(pred[0] == 1.)
        samples.append(assample(visit, col, hist_boundary[0], hist_boundary[1], pred_boundary[0], pred_boundary[1],
                                hist, pred, interest, is_test))

    return samples


def overdayNnightX_samples(threshold, visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme, NIGHTS):

    # training on everything, testing only on night prediction
    tod = visit[:, col['tod']]
    newid = visit[0, col['newid']]

    # find nights
    night_boundaries, day_boundaries = when_all_nights_and_days(tod)
    for pred_boundaries in [night_boundaries, day_boundaries]:
        if len(pred_boundaries) == 0:
            return []
        # history before night/day at least min_seq_len long
        pred_boundaries = [[s, e] for s, e in pred_boundaries if s >= min_seq_len]
        if len(pred_boundaries) == 0:
            return []

    # choose test samples, cover history
    # all day samples are in training
    # simply multiply the test_data_ratio by 2.5, assuming that day samples are a somewhat overrepresented
    night_are_test = rand_choose_test(len(night_boundaries), rand_scheme, test_data_ratio * 2.5)
    day_are_test = np.array([False] * len(day_boundaries))  # all are training samples
    # comb together day n night with their test flag
    # adjust history needs sorted boundaries, so sort them
    night_boundaries = [(b[0], b[1], is_test) for b, is_test in zip(night_boundaries, night_are_test)]
    # day_boundaries = [(b[0], b[1], is_test) for b, is_test in zip(day_boundaries, day_are_test)]
    pred_boundaries = sorted(night_boundaries)  # NO NEED FOR DAY FOR PROSPECTIVE  # + day_boundaries)
    are_test = np.array([is_test for _, _, is_test in pred_boundaries])  # extract test flag
    pred_boundaries = [[pred_s, pred_e] for pred_s, pred_e, _ in pred_boundaries]  # remove test flag

    hist_boundaries = [[0, pred_s] for pred_s, pred_e in pred_boundaries]
    hist_boundaries = adjust_history(are_test, pred_boundaries, hist_boundaries)

    # find the index of NIGHTS after skipping the already predicted nights
    # TODO see here if NIGHTS[newid] has the same night boundaries

    samples = []
    for i, (is_test, pred_boundary, hist_boundary) in enumerate(zip(are_test, pred_boundaries, hist_boundaries)):
        hist = visit[hist_boundary[0]:hist_boundary[1], :].copy()
        pred = np.array([float(np.any(visit[pred_boundary[0]:pred_boundary[1], col['FullScore']] >= threshold))], dtype=np.float32)
        interest = int(pred[0] == 1.)
        if len(hist) < min_seq_len:
            continue

        # return sample straight away, check if it has been predicted
        sample = assample(visit, col, hist_boundary[0], hist_boundary[1], pred_boundary[0], pred_boundary[1],
                          hist, pred, interest, is_test)
        overlap = False
        selected_night_i = -1
        for night_i in range(len(NIGHTS[newid])):
            night = NIGHTS[newid][night_i]
            if night[2] and date_overlap(night[0], night[1], sample[7], sample[8]):
                overlap = True  # predicted already
                break
            elif not night[2] and date_overlap(night[0], night[1], sample[7], sample[8]):
                selected_night_i = night_i
                break
        if overlap:
            continue
        else:  # not predicted yet = gonna be predicted next, so let's set the predicted flag true
            if selected_night_i == -1:
                raise ValueError('either already predicted overlapping or selected')
            NIGHTS[newid][selected_night_i][2] = True

        samples.append(sample)
        break

    return samples


def overnight7_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    return overnightX_samples(7., visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme)


def overnight5_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    return overnightX_samples(5., visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme)


def overnight6_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    return overnightX_samples(6., visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme)


def overdayNnight7_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme, NIGHTS):  # only daynnight implemented the prospective magic
    return overdayNnightX_samples(7., visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme, NIGHTS)


def overdayNnight5_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme, NIGHTS):
    return overdayNnightX_samples(5., visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme, NIGHTS)


def overdayNnight6_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme, NIGHTS):
    return overdayNnightX_samples(6., visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme, NIGHTS)


def overnightD_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    tod = visit[:, col['tod']]

    # find nights
    pred_boundaries = when_all_nights(tod)
    if len(pred_boundaries) == 0:
        return []
    # history before night at least min_seq_len long
    pred_boundaries = [[s, e] for s, e in pred_boundaries if s >= min_seq_len]
    if len(pred_boundaries) == 0:
        return []

    # cover history
    are_test = rand_choose_test(len(pred_boundaries), rand_scheme, test_data_ratio)
    hist_boundaries = [[0, pred_s] for pred_s, pred_e in pred_boundaries]
    hist_boundaries = adjust_history(are_test, pred_boundaries, hist_boundaries)

    samples = []
    for i, (is_test, pred_boundary, hist_boundary) in enumerate(zip(are_test, pred_boundaries, hist_boundaries)):
        hist = visit[hist_boundary[0]:hist_boundary[1], :].copy()
        pred = np.array([float(np.any(visit[pred_boundary[0]:pred_boundary[1], col['dfs']] >= 3.))], dtype=np.float32)
        interest = int(pred[0] == 1.)
        samples.append(assample(visit, col, hist_boundary[0], hist_boundary[1], pred_boundary[0], pred_boundary[1],
                                hist, pred, interest, is_test))

    return samples


# data prep functions create multiple samples (history and pred value pairs),
#   out of which the data augmentation function make actual (oversampled) sequences
# data prep functions further separate the dataset into train and test sets,
#   and determine whether the sample is 'interesting' = more of a candidate for oversampling
# subsequent data augmentation func needs all the history columns regardless of input fields,
#   but predicted fields are selected for
# params: visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme
DATA_PREP_FUNS = {'prediction': regular_samples, 'overnight7': overnight7_samples, 'overnight5': overnight5_samples,
                  'overnight6': overnight6_samples, 'overnightD': overnightD_samples,
                  'overdayNnight7': overdayNnight7_samples, 'overdayNnight5': overdayNnight5_samples,
                  'overdayNnight6': overdayNnight6_samples}


def delta_feature(feature, mews):
    mews['delta' + feature] = mews[feature].diff()
    mews.at[0, 'delta' + feature] = 0.  # otherwise it would be NaN
    return mews  # this requires mews to be sorted by ['newid', 'AlertDtm']


def var_scores_feature(feature, mews):
    score_cols = [c for c in mews.columns.values if 'Score' in c]
    var = mews[score_cols].var(axis=1, numeric_only=True)
    mews[feature] = var
    return mews


# list of features and corresponding functions to extract them from the mews table
# if any feature is included in the input/predict fields, they are extracted and added to the table
FEATURES = {'delta': delta_feature, 'varscores': var_scores_feature}


# augmentation by shifting history (cutting off from the left/earlier part) or by rand skipping from history
# has to return the original samples as well
# when shifting the first element's delta values (td, dfs) has to be updated to zero
# when skipping the delta values have to be updated for those whose left neighbor is removed
# interest (as in its name) is an int {0,1}, indexing step_size and skip, so interesting samples are can get oversampled
# nskip is the number of skippy samples that should be generated for not interesting and interesting
# also zeroes out first delta elements for all, including the original samples
# also cuts history if too long
# also :D removes history (input) fields that are not needed
def augment_samples(samples, col, step_size, nskip, min_seq_len, max_seq_len, input_fields, augment_test=False):
    augmented_samples = []
    input_field_indices = [col[f] for f in input_fields]

    # skipping
    # TODO rand the number of elements to remove and then which ones
    #  high probability for sequences containing almost all elements
    #  update delta values for first elements and every other where prev element was removed
    # TODO https://stackoverflow.com/questions/3679694/a-weighted-version-of-random-choice
    if nskip[0] != 0 or nskip[1] != 0:
        raise NotImplementedError

    # stepping
    for sample in samples:
        hist, pred, interest, is_test, newid, start_date, end_date, pred_start_date, pred_end_date = sample

        hist = hist[-max_seq_len:]  # cut history
        hist[0, col['td']] = 0.  # zero deltas out
        hist[0, col['dfs']] = 0.  # not actually necessary, just to be safe
        augmented_samples.append((hist[:, input_field_indices], pred, interest, is_test, newid,
                                  start_date, end_date, pred_start_date, pred_end_date,
                                  hist[:, col['AlertDtm']]))
        # pred contains pred fields only already

        if step_size[interest] == 0 or (is_test and not augment_test):  # don't augment test set
            continue

        hist_steps = np.arange(step_size[interest], len(hist) - min_seq_len + 1, step_size[interest])
        for step in hist_steps:
            extra_hist = hist[step:].copy()
            extra_hist[0, col['td']] = 0.
            extra_hist[0, col['dfs']] = 0.
            augmented_samples.append((extra_hist[:, input_field_indices], pred, interest, is_test, newid,
                                      start_date, end_date, pred_start_date, pred_end_date,
                                      hist[:, extra_hist['AlertDtm']]))
            # as there's no longer access to the visit, start dates of the sequence can be off

    return augmented_samples  # contains the original samples as well


def create_seq_prosp(cfg, mews_table, NIGHTS, data_size='', base_dir='/mnt/data/mews/seqs/'):

    # extract vars from config
    min_seq_len = cfg['min_seq_len']
    input_fields = cfg['input']
    predicted_fields = cfg['predict']
    max_seq_len = cfg['seq_len']
    task = cfg['task']
    test_data_ratio = cfg['test_data_ratio']
    rand_scheme = cfg['test_rand_scheme']
    augm_step = cfg['data_augmentation']['step']
    augm_skip = cfg['data_augmentation']['skip']

    # constants
    dtype = tables.Float32Atom()
    npdtype = np.float32
    static_seq_len = min_seq_len == max_seq_len

    # load mews, drop records with nans
    # dsize = '' if data_size == '' else data_size + '_'
    # mews_table = 'data/' + dsize + 'processed_mews2.csv'
    mews = pd.read_csv(mews_table, parse_dates=['AlertDtm']).sort_values(['newid', 'AlertDtm'])
    must_not_be_nan = list(set(mews.columns.values).intersection(set(input_fields + predicted_fields))) + ['FullScore']
    mews = mews.dropna(axis=0, how='any', subset=must_not_be_nan).reset_index(drop=True)

    # drop rows according to NIGHTS
    # go id by id, expand criteria stored in NIGHTS by only removing the night of the last prediction/id
    #   as there is always only one prediction per id in one round
    print('REALDEAL4')
    if 'criteria' not in NIGHTS:
        NIGHTS['criteria'] = []

    tstart = time.time()
    criteria = [NIGHTS['criteria']]
    criteriaold = [NIGHTS['criteria']]  # FIXME
    n_nights_dropped = 0

    # pandas groupby newid, keep numerical indices, check dates
    # get index offset for each newid, because the criterion night search is within groups,
    #   the resulting indices have to be offset so they index the whole mews table
    mews_grp = mews.groupby(by='newid')
    accum = 0
    newid_offsets = {}
    for newid, grp in mews_grp:
        newid_offsets[newid] = accum
        accum += grp.shape[0]

    for newid, nights_of_visit in NIGHTS.items():
        if newid == 'criteria':
            continue
        for night_of_visit in reversed(nights_of_visit):  # reversed order to find latest predicted night only
            if night_of_visit[2]:
                if night_of_visit[3]:  # predicted and let sleep (see NIGHTS def in prepare_seq_input.py)
                    grp = mews_grp.get_group(newid)
                    criterion = np.where((night_of_visit[0] <= grp['AlertDtm']) & (grp['AlertDtm'] < night_of_visit[1]))[0] + newid_offsets[newid]
                    # criterionold = np.where((mews['newid'] == newid) & (night_of_visit[0] <= mews['AlertDtm']) & (mews['AlertDtm'] < night_of_visit[1]))[0]
                    criteria.append(criterion)
                    # criteriaold.append(criterionold)
                    n_nights_dropped += 1
                break  # stop here, the latest prediction is found already

    NIGHTS['criteria'] = np.concatenate(criteria)
    mews.drop(index=NIGHTS['criteria'], inplace=True)
    print('n nights removed: {}; n records dropped: {}; time: {}'.format(n_nights_dropped, len(NIGHTS['criteria']), time.time() - tstart))

    # feature extraction
    extra_features = set(input_fields + predicted_fields) - set(mews.columns.values)
    delta_extra_features = [f for f in extra_features if 'delta' in f]
    for feature in extra_features:
        if 'delta' in feature:
            src_feature = feature[5:]
            mews = FEATURES['delta'](src_feature, mews)
        elif feature in FEATURES:
            mews = FEATURES[feature](feature, mews)
        else:
            print('WARNING: can\'t find field/feature: {}'.format(feature))

    # convert categorical features to integers
    categorical_features = set(['Hospital'])
    feature_mappings = {}
    for feature in categorical_features.intersection(input_fields):
        feature_mapping = {unique_val: i for i, unique_val in enumerate(np.unique(mews[feature]))}
        mews[feature] = mews[feature].map(feature_mapping).astype(np.int32)
        feature_mappings[feature] = feature_mapping

    # recompute delta values (after removing rows because of nans in them)
    # then group data by id
    col = {v: i for i, v in enumerate(mews.columns.values)}
    rmews = np.array(mews)
    td = np.concatenate([np.array([0]), np.diff(rmews[:, col['ts']])])
    dfs = np.concatenate([np.array([0]), np.diff(rmews[:, col['FullScore']])])
    rmews[:, col['td']] = td
    rmews[:, col['dfs']] = dfs
    rvisits = npgroupby(rmews, col['newid'])

    seq_len = min_seq_len if static_seq_len else min(max_seq_len, max([len(v) for v in rvisits]))

    # return samples instead of hdf5
    SAMPLES = {'train_data_x': [], 'train_data_y': [], 'valid_data_x': [], 'valid_data_y': [], 'test_data_x': [],
               'test_data_y': [], 'x_cols': input_fields + ['EOS'], 'y_cols': predicted_fields,
               'categorical_features': feature_mappings, 'test_newid': [], 'test_dtm': [], 'test_start': [],
               'test_end': [], 'test_pred_start': [],
               'test_pred_end': []}  # has x, y, newid, and the rest you would store in the hdf5

    # go visit-by-visit
    ngen_samples = 0
    naugmented_samples = 0
    eos_flags = np.expand_dims(np.array(([0.] * (seq_len - 1)) + [1]), axis=-1)
    for visit_i in range(len(rvisits)):
        visit = rvisits[visit_i]
        if len(visit) < min_seq_len:
            continue

        # finish up updated delta values and update 'last' column
        for feature in ['td', 'dfs'] + delta_extra_features:
            visit[0, col[feature]] = 0.
        visit[-1, col['last']] = 1.

        # extract samples
        samples = DATA_PREP_FUNS[task](visit, col, input_fields, predicted_fields, min_seq_len, seq_len,
                                       test_data_ratio, rand_scheme, NIGHTS)
        ngen_samples += len(samples)

        assert len(samples) == 1 or len(samples) == 0

        # augment and cut according to (max_)seq_len
        # also removes unnecessary fields
        samples = augment_samples(samples, col, augm_step, augm_skip, min_seq_len, seq_len, input_fields)
        naugmented_samples += len(samples)

        # gather one (test!) sample per visit, return samples;
        #   then the prediction side overwrites the let sleep flags and the next set of samples are asked for

        # pad, add EOS then place it into hdf5
        # end of sequence (EOS) as an extra scalar, 1 when present, 0 when not
        for sample in samples:  # samples should be 1 long
            hist, pred, interest, is_test, newid, start_date, end_date, pred_start_date, pred_end_date, dtms = sample

            if not is_test:
                raise ValueError('can only have test samples at this point')

            if not static_seq_len:
                pad_len = seq_len - hist.shape[0]
                hist = np.pad(hist, ((pad_len, 0), (0, 0)), mode='constant', constant_values=(0, 0))
                dtms = np.array([d.value for d in dtms])  # prepare for pandas
                dtms = np.pad(dtms, (pad_len, 0), mode='constant', constant_values=(0, 0))  # 1D
            hist = np.array(np.append(hist, eos_flags, axis=1), dtype=npdtype)

            # check for nans here, for some reason they appear in the last iteration of prospective testing
            if np.any(np.isnan(hist)):
                print('HIST:', hist)
                print(newid, start_date, end_date, pred_start_date, pred_end_date)
                print(dtms)
                continue  # don't add to SAMPLES

            SAMPLES['test_data_x'].append(hist)
            SAMPLES['test_data_y'].append(pred)
            # some training/validation data is necessary so lstm.load_data() doesn't die when processing (onehot)
            if np.random.random() < .0000005:
                SAMPLES['train_data_x'].append(hist)
                SAMPLES['train_data_y'].append(pred)
                SAMPLES['valid_data_x'].append(hist)
                SAMPLES['valid_data_y'].append(pred)

            SAMPLES['test_newid'].append(newid.encode('ascii'))
            SAMPLES['test_dtm'].append(dtms)
            SAMPLES['test_start'].append(start_date.value)
            SAMPLES['test_end'].append(end_date.value)
            SAMPLES['test_pred_start'].append(pred_start_date.value)
            SAMPLES['test_pred_end'].append(pred_end_date.value)

        if visit_i % 5000 == 0:
            print('{}/{}'.format(visit_i, len(rvisits)))

    print('samples augmented {} out of {}'.format(naugmented_samples - ngen_samples, naugmented_samples))
    print('total visits:', len(rvisits))

    return SAMPLES


if __name__ == '__main__':
    # TESTING IN test_prosp.py
    pass

