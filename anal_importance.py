import pandas as pd
import numpy as np
from scipy import stats
from lstm import model_name
import matplotlib.pyplot as plt
import seaborn as sb
from glob import glob


if __name__ == '__main__':

    FIELD_TO_PLOT = {'age': 'Age', 'td': 'Δtime (h)', 'tod': 'Hour of Day', 'FullScore': 'MEWS', 'BPNum': 'BP (mmHg)',
                     'HRNum': 'HR (bpm)', 'TmprNum': 'Tmpr (\u00B0C)', 'RespRateNum': 'RR (bpm)', 'dfs': 'ΔMEWS',
                     'varscores': 'Var(Sub Scores)'}

    cfg = {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight7',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'],  # 'BMINum', varscores removed, FullScore
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [256] * 5,  # 256
        'min_seq_len': 8,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.6,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': 'dropout_ens'  # FIXME like in input_importance.py
    }

    dfs = []
    print('importance/{}*.csv'.format(model_name(cfg)))
    for f in glob('importance/{}*.csv'.format(model_name(cfg))):
        # imp_f = 'importance/{}.csv'.format(model_name(cfg))
        try:
            dfs.append(pd.read_csv(f))
        except:
            pass
    df = pd.concat(dfs)
    print('N CSV:', len(dfs))
    print('N: {}; N BASELINE: {}; N PER CLASS: {}'.format(len(df), len(df.loc[df['input'] == 'BASELINE']),
                                                          len(df.loc[df['input'] != 'BASELINE']) // len(np.unique(df['input']))))

    print(df.columns.values)
    baseline = df.loc[df['input'] == 'BASELINE']
    print(np.mean(baseline, axis=0))
    # print(baseline.describe())

    inps = np.unique(df['input'])
    inps = inps[inps != 'BASELINE']

    plt.style.use('seaborn-white')
    plt.rc('font', size=13)

    df['input'] = df['input'].rename(FIELD_TO_PLOT)
    ninput = len(np.unique(df['input']))
    perf_measures = set(df.columns.values) - set(['input'])
    for col in perf_measures:
        # establish order
        df = df.sort_values([col, 'input']).reset_index(drop=True)
        # means = df.groupby('input', group_keys=True)
        # indices = means.T.keys()
        # # TODO [np.mean(m[1]) for m in means]
        # sorted_indices = sorted([(m, i) for m, i in zip(means, indices)])
        # sorted_indices = [i[1] for i in sorted_indices]
        # print(sorted_indices)

        plt.figure(figsize=(8., 6.2))
        ax = sb.barplot(x=df[col], y=df['input'], color='C3', ci=95, alpha=.6) #, order=sorted_indices)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        plt.xlim(np.min(df[col]) - 0.01, np.max(df[col]))
        # plt.plot([np.max(df[col]), np.max(df[col])], [-.4, ninput - .6], c='C4', linewidth=5)
        plt.title(col)

        plt.show()

    for inp in inps:
        print('---------- {} ----------'.format(inp))
        inp_d = df.loc[df['input'] == inp]
        print(np.mean(inp_d, axis=0))
        # print(inp_d.describe())

        for col in set(inp_d.columns.values) - set(['input']):
            t, p = stats.ttest_ind(baseline[col], inp_d[col])
            print('{}: t({}), p={}'.format(col, t, p))
            # plt.figure()
            # plt.hist(baseline[col], bins=100)
            # plt.hist(inp_d[col], bins=100)
            # plt.show()

    # TODO do pair-wise t-test between baseline instances and removed instances
    #  for each ensamble model (100) test the baseline once and then with each noised input
    #  then pair up the baseline with the noised perf and pair-wise compare
    # TODO but what about different random numbers in case of the noised performances?

    # TODO need more models (ensamble), because the baseline values are the same, they don't have no variance, hence t-test is not applicable
    #  and this https://thestatsgeek.com/2013/09/28/the-t-test-and-robustness-to-non-normality/
