#!/bin/bash

# first run input_importance.py without any parameters to get the number of runs

if [ $# -lt 4 ]; then
    echo 1>&2 "$0: not enough arguments"
    exit 2
fi

ngpu=2  # always just 2 gpus (models within an ensemble are indexed by the gpu number and the ensemble id)
nrepeats="$1"  # can be whatever >=1
nens="$2"  # number of models within the ensemble (per gpu) to test (other model params are in input_importance.py)
nruns="$3"  # depends on the number of input variables, run input_importance.py w/o params to figure it out
suffix="$4"  # model name suffix

for i in $(seq 0 $(expr $nrepeats - 1));
do
    echo "=========================== $i / $nrepeats REPEAT =============================="

    for g in $(seq 0 $(expr $ngpu - 1));
    do
        echo "================ $g / $ngpu GPU ================"

        for e in $(seq 0 $(expr $nens - 1));
        do
            echo "-------------- $e / $nens ENSEMBLE ----------------"

            for r in $(seq 0 $(expr $nruns - 1));
            do
                echo "-------------------------- $r / $nruns RUN -------------------------------"
                python3.6 input_importance.py $r $g $e "$suffix"
            done
        done
    done
done
