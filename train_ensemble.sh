#!/bin/bash


if [ $# -lt 6 ]; then
    echo 1>&2 "$0: not enough arguments"
    exit 2
fi

gpu_id="$1"
nmodels="$2"
model_id_offset="$3"
more_suffix="$4"  # dropout_batchnorm
maj_class_rate="$5"
min_seq_len="$6"

for r in $(seq $model_id_offset $(expr $model_id_offset + $nmodels - 1));
do
    echo "======================== $r/$nmodels =========================="
    python3.6 train_ensemble.py $gpu_id $r $model_id_offset "$more_suffix" "$maj_class_rate" "$min_seq_len"
done
