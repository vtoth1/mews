from lstm import plot_roc, plot_tn_vs_fn, model_name, clin_appl_auc, clin_appl_idx
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc, confusion_matrix
import sys
import pickle
import copy
import plotly.graph_objects as go


def load_test_results(cfg, prospective, lin=False):
    name = model_name(cfg)
    prosp = '_prosp' if prospective else ''
    linstr = f'_lin' if lin else ''
    with open('results/{}/test{}{}.pickle'.format(name, linstr, prosp), 'rb') as f:
        d = pickle.load(f)
    return d


def get_roc_and_all(preds, desired):
    # how many woke up / did not wake up thanks to me
    # ntps = np.sum((preds > threshold) & (desired == 1))  # wake-up
    # nfps = np.sum((preds > threshold) & (desired == 0))
    # ntns = np.sum((preds < threshold) & (desired == 0))  # let sleep
    # nfns = np.sum((preds < threshold) & (desired == 1))
    # print('at {} certainty (pos is to wake up): tps {} fps {} tpsinv {} fpsinv {}'.format(threshold,
    #                                                                                       ntps, nfps, ntns, nfns))

    # compute tpr and fpr, then inverted tpr fpr, which is the same as tnr and fnr technically
    #   but the threshold vector is different sized
    n = len(preds)
    fpr, tpr, thresholds = roc_curve(desired, preds)
    roc_auc = auc(fpr, tpr)
    fprinv, tprinv, thresholdsinv = roc_curve(1 - desired, 1 - preds)
    roc_auc_inv = auc(fprinv, tprinv)

    tpsinv = np.zeros(len(thresholdsinv))
    fpsinv = np.zeros(len(thresholdsinv))
    for i, th in enumerate(thresholdsinv):
        tpsinv[i] = np.sum(((1 - preds) > th) & ((1 - desired) == 1))
        fpsinv[i] = np.sum(((1 - preds) > th) & ((1 - desired) == 0))

    # look out, fpsinv/n != fprinv; fprinv is normalized by the number of fp, not by n (all)
    # fpsinv and fprinv still have to have an index correspondence
    clin_auc = clin_appl_auc(fpsinv, n, fprinv, tprinv, 2 / 10000)
    # spcial measure: tprinv at the edge of the clinically applicable
    clin_idx = clin_appl_idx(fpsinv, n, 2 / 10000)
    tprinv_at_clin_edge = tprinv[clin_idx]

    return fprinv, tprinv, thresholdsinv, roc_auc_inv, tpsinv, fpsinv, clin_auc, clin_idx, tprinv_at_clin_edge


if __name__== '__main__':
    # cfg = {
    #     'type': 'regression',  # regression, simple_normal, var_normal, gaussian
    #     'task': 'overdayNnight7',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
    #     'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'],
    # # 'BMINum', varscores removed, FullScore
    #     'predict': ['FullScore'],
    #     'lr': 0.0001,
    #     'layers': [256] * 5,  # 256
    #     'min_seq_len': 8,
    #     'seq_len': 42,  # max_seq_len
    #     'batch_size': 512,
    #     'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
    #     'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
    #     'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
    #     'test_data_ratio': 0.3,
    #     'majority_class_rate': 0.6,  # maximum allowed amount of the majority class
    #     'class_weight': {0: 1., 1: 8.},
    #     'name_suffix': 'dropout_batchnorm_final_ens0:24'
    #     # dropout_batchnorm_cpucompat, dropout_batchnorm_final_ens0:24
    #     #   but_regr, w_rnn (order: rnn, dropout, batchnorm, ens), cpucompat
    # }

    cfg = {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight7',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'],
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [32] * 5,  # 256
        'min_seq_len': 8,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.92,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': 'to_show_off_linear',  # 'to_show_off_linear'  # but_regr, w_rnn (order: rnn, dropout, batchnorm, ens), cpucompat
    }

    # # sankey diagram of data
    # discarded = 1042232
    # train_pos, train_neg = 144538, 2820858 - discarded
    # valid_pos, valid_neg = 13935, 884549
    # test_pos, test_neg = 14177, 879961
    # prosp_pos, prosp_neg = 1282 + 1267, 85924 + 87145
    # prosp_train_pos, prosp_train_neg = 5909, 24203
    # retro = train_pos + train_neg + valid_pos + valid_neg + test_pos + test_neg + discarded  # 4758018
    # prosp = prosp_pos + prosp_neg  # + prosp_train_pos + prosp_train_neg  # 205730
    #
    # gray_node = 'rgba(100, 100, 100, 1.0)'
    # pos_node = 'rgba(175, 150, 199, 1.0)'
    # neg_node = 'rgba(213, 145, 166, 1.0)'
    # retro_node = 'rgba(255, 189, 103, 1.0)'
    # prosp_node = 'rgba(103, 180, 189, 1.0)'
    # sub_retro_node = 'rgba(255, 189, 103, 0.7)'
    # sub_prosp_node = 'rgba(103, 180, 189, 0.7)'
    #
    # gray = 'rgba(150, 150, 150, 0.5)'
    # gray_disc = 'rgba(150, 150, 150, 0.3)'
    # poscol = 'rgba(175, 83, 183, 0.6)'  # unstable
    # negcol = 'rgba(183, 83, 83, 0.3)'  # stable
    # retrocol = 'rgba(255, 189, 103, 0.3)'
    # retrocol_uns = 'rgba(255, 189, 103, 0.7)'
    # prospcol = 'rgba(103, 180, 189, 0.9)'
    #
    # fig = go.Figure(data=[go.Sankey(
    #     arrangement='perpendicular',
    #     orientation='h',
    #     node=dict(
    #         pad=15,
    #         thickness=20,
    #         line=dict(color="black", width=0.),
    #         label=['Data', 'Retrospective', 'Prospective', 'Discarded', 'Training', 'Validation', 'Test', 'Unstable', 'Stable'],
    #         color=[gray_node, retro_node, prosp_node, gray_node, sub_retro_node, sub_retro_node, sub_retro_node, pos_node, neg_node]
    #     ),
    #     link=dict(
    #         source=[0, 0, 4, 1, 1, 1,  3, 4, 4, 5, 5, 6, 6, 2, 2],  # indexing labels
    #         target=[1, 2, 3, 4, 5, 6,  8, 7, 8, 7, 8, 7, 8, 7, 8],
    #         value=[retro, prosp, discarded, train_pos + train_neg + discarded, valid_pos + valid_neg, test_pos + test_neg,
    #                discarded, train_pos, train_neg, valid_pos, valid_neg, test_pos, test_neg, prosp_pos, prosp_neg],  # TODO
    #         color=[retrocol, prospcol, retrocol, retrocol, retrocol, retrocol,
    #                gray_disc, retrocol_uns, retrocol, retrocol_uns, retrocol, retrocol_uns, retrocol, prospcol, prospcol]
    #     ))])
    #
    # fig.update_layout(autosize=False, width=1000, height=800, font_size=18)
    # fig.show()
    # # exit()

    # roc and all
    linear = False
    res = load_test_results(cfg, prospective=False, lin=linear)
    n = len(res['predsinv'])
    nfpinv = (res['desiredinv'] == 0).sum()  # at max

    plt.rc('font', size=13)
    fig, ((ax0, ax1, ax2), (ax3, ax4, ax5)) = plt.subplots(2, 3, figsize=(6.2 * 3, 6.2 * 2))
    fig.tight_layout(pad=4.0)

    model_fn_ratios = np.array([1/20000, 1/10000, 2/10000])  # not really false ratio, as it is the ratio of the whole
    model_idx = np.array([np.argmin(np.abs(res['fpsinv'] / len(res['predsinv']) - r)) for r in model_fn_ratios])

    print('clinical auc:', clin_appl_auc(res['fpsinv'], n, res['fprinv'], res['tprinv'], model_fn_ratios[-1]))
    print('regular auc:', auc(res['fprinv'], res['tprinv']))

    plot_roc(res['fprinv'], res['tprinv'], res['thresholdsinv'], model_idx, n, nfpinv, ax=ax0)
    plot_tn_vs_fn(len(res['predsinv']), res['tpsinv'], res['fpsinv'], res['thresholdsinv'], model_idx, sys.stdout, axes=[ax1, ax2])

    # prospective
    print('----------- PROSPECTIVE -----------')
    resp = load_test_results(cfg, prospective=True, lin=linear)  # prospective results
    n = len(resp['predsinv'])
    nfpinv = np.max(resp['fpsinv'])

    print('clinical auc:', clin_appl_auc(resp['fpsinv'], n, resp['fprinv'], resp['tprinv'], model_fn_ratios[-1]))
    print('regular auc:', auc(resp['fprinv'], resp['tprinv']))

    # you need to use the same thresholds as for the models above
    # find the closest thresholds in the prospective thresholds array
    model_thresholds = res['thresholdsinv'][model_idx]
    model_idx_prosp = np.array([np.argmin(np.abs(resp['thresholdsinv'] - th)) for th in model_thresholds])

    print('model idx:', model_idx)
    print('model idx prosp:', model_idx_prosp)
    print('thresholds:', model_thresholds)
    print('prosp thresholds:', resp['thresholdsinv'][model_idx_prosp])

    plot_roc(resp['fprinv'], resp['tprinv'], resp['thresholdsinv'], model_idx_prosp, n, nfpinv, ax=ax3)
    plot_tn_vs_fn(len(resp['predsinv']), resp['tpsinv'], resp['fpsinv'], resp['thresholdsinv'], model_idx_prosp, sys.stdout, axes=[ax4, ax5], legend=True)

    for lab, ax in zip(['A', 'B', 'C', 'D', 'E', 'F'], [ax0, ax1, ax2, ax3, ax4, ax5]):
        ax.text(-0.07, 1.07, lab, transform=ax.transAxes, size=24, weight='bold')

    plt.show()
