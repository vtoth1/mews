from glob import glob
from pprint import pprint
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats


def gather_perf_meas(logs, trylinear=False):
    auc_map, auc_full_map, clin_tpr_map = {}, {}, {}  # last is % sleeping at clinical appl edge

    for log in logs:
        with open(log, 'rt') as f:
            lines = [l for l in f]

        auc = lines[0].replace(' ', '')
        tpr = lines[1].replace(' ', '')
        auc_full = lines[2].replace(' ', '')

        if trylinear and 'linear' in log:
            auc = lines[4].replace(' ', '')
            auc_full = lines[5].replace(' ', '')
            tpr = lines[6].replace(' ', '')
            print('done')

        try:
            auc = float(auc[auc.index(':') + 1:])
            tpr = float(tpr[tpr.index(':') + 1:])
            auc_full = float(auc_full[auc_full.index(':') + 1:])
            if auc > 1 or tpr > 1 or auc_full > 1 or auc_full < .93:
                raise ValueError

            auc_map[log] = auc  # clinically relevant
            clin_tpr_map[log] = tpr
            auc_full_map[log] = auc_full
        except:
            pass

    return auc_map, auc_full_map, clin_tpr_map


def cmp_perf_meas(mapping, tocmp):

    for l, v in mapping.items():
        for cmp in tocmp:
            if cmp in l:
                tocmp[cmp].append(v)

    return {l: np.array(v) for l, v in tocmp.items()}


def plot_diff(cmp_map, bins=20, labels=None, xlab=''):
    plt.figure()
    for k, v in cmp_map.items():
        lab = k if labels is None else labels[k]
        print(np.nanvar(v))
        plt.hist(v, label=lab, bins=bins, alpha=.7)  # int(np.std(v) * 3000)
    plt.xlabel(xlab)
    plt.legend()
    plt.show()


def ttest_diff(cmp_map):
    k1 = list(cmp_map.keys())[0]
    k2 = list(cmp_map.keys())[1]  # only for the comparison of two classes
    tt, tp = stats.ttest_ind(cmp_map[k1], cmp_map[k2])
    pprint('T-test {} vs {}: {}, p={}'.format(k1, k2, tt, tp))


def random_perm_diff(cmp_map, nsamples=1000):
    k1 = list(cmp_map.keys())[0]
    k2 = list(cmp_map.keys())[1]
    diff = []
    for _ in range(nsamples):
        r1 = np.random.randint(len(cmp_map[k1]))
        r2 = np.random.randint(len(cmp_map[k2]))
        diff.append(cmp_map[k1][r1] > cmp_map[k2][r2])
    return np.mean(diff)


def auc_confidence(cmp_map):
    k1 = list(cmp_map.keys())[0]
    k2 = list(cmp_map.keys())[1]
    print(f'confidence intervals (5-95): {k1} ({np.mean(cmp_map[k1])}): {np.percentile(cmp_map[k1], (0, 95))};'
          f'{k2} ({np.mean(cmp_map[k2])}): {np.percentile(cmp_map[k2], (0, 95))}')


if __name__ == '__main__':

    base_v_dropout = {'__': [], 'dropout_e': []}
    base_v_batchnorm = {'__': [], '42_batchnorm_e': []}  # assumes max seq len of 42
    base_v_dropout_batchnorm = {'__': [], 'droupout_batchnorm': []}  # note extra 'u' in droupout
    dropout_v_batchnorm = {'dropout_e': [], '42_batchnorm_e': []}
    batchnorm_v_dropout_batchnorm = {'42_batchnorm_e': [], 'droupout_batchnorm': []}
    big_v_small = {'256-256_td+tod_8-42_dropout_batchnorm': [], '32-32_td+tod_8-42_dropout_batchnorm': []}
    majority_balance = {'maj90': [], 'maj70': []}
    min_seq_len = {'maj90': [], 'minseqlenof4': []}
    rnn_vs_lin = {'maj90': [], 'justforlinear': []}
    whateva = {'batchnorm_final': [], 'ens': []}

    pprint('-')
    pprint('--------------------------------------------')
    pprint('RETROSPECTIVE')
    pprint('--------------------------------------------')

    trylinear = False
    logs = glob('results/*/log.txt')  # glob('results/*/log_prosp.txt')  # for checking prospective results
    auc_map, auc_full_map, clin_tpr_map = gather_perf_meas(logs, trylinear)

    selected_cmp = big_v_small
    selected_map, map_name = auc_full_map, 'Clinically relevant true positive rate'
    # 'Overall AUC', 'Clinically relevant AUC', 'Clinically relevant true positive rate'
    labels = {'maj90': 'RNN', 'justforlinear': 'Linear'}
    # {'maj90': '92% hard balance', 'minseqlenof4': '70% hard balance'}
    # {'maj90': '92% hard balance', 'maj70': '70% hard balance'}
    # {'maj90': 'RNN', 'justforlinear': 'Linear'}

    cmp_map = cmp_perf_meas(selected_map, selected_cmp)
    ttest_diff(cmp_map)
    auc_confidence(cmp_map)

    pprint(f'random_perm_diff: {random_perm_diff(cmp_map)}; {[(k, len(cmp_map[k])) for k in cmp_map]}')
    plot_diff(cmp_map, bins=20)#, labels=labels, xlab=map_name)

    auc_list = sorted([(v, k) for k, v in auc_map.items()], reverse=True)
    tpr_list = sorted([(v, k) for k, v in clin_tpr_map.items()], reverse=True)
    pprint('========= AUC ==========')
    pprint(auc_list)
    pprint('========= TPR ==========')
    pprint(tpr_list)

    pprint('-')
    pprint('--------------------------------------------')
    pprint('PROSPECTIVE')
    pprint('--------------------------------------------')

    logs = glob('results/*/log_prosp.txt')
    auc_map, auc_full_map, clin_tpr_map = gather_perf_meas(logs)

    selected_cmp = majority_balance
    selected_map = auc_full_map

    cmp_map = cmp_perf_meas(clin_tpr_map, selected_cmp)
    ttest_diff(cmp_map)
    plot_diff(cmp_map)

    auc_list = sorted([(v, k) for k, v in auc_map.items()], reverse=True)
    tpr_list = sorted([(v, k) for k, v in clin_tpr_map.items()], reverse=True)
    pprint('========= AUC ==========')
    pprint(auc_list)
    pprint('========= TPR ==========')
    pprint(tpr_list)

    # 0.75, 8.0: 48.0
    #  results/model_simple_normal_overdayNnight7_visits_0-0-0-0_0.75_0:1.0-1:8.0_td+tod+age+FullScore+BPNum+HRNum+TmprNum+RespRateNum+dfs+varscores_FullScore_256-256-256-256-256_td+tod_8-42_/log.txt
    # 0.60, 8.0: 48.4
    #  results/model_simple_normal_overdayNnight7_visits_0-0-0-0_0.6_0:1.0-1:8.0_td+tod+age+FullScore+BPNum+HRNum+TmprNum+RespRateNum+dfs+varscores_FullScore_256-256-256-256-256_td+tod_8-42_/log.txt
