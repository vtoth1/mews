import pandas as pd
import swifter
import numpy as np
from parse import parse, compile


p_sbp = ['Systolic B/P{}as{val}({}) (+{score})',
         'Systolic B/P{}as{val}({})',
         'Systolic B/P{}as{val}']
p_hr = ['Heart Rate{}as{val}beats/min ({}) (+{score})',
        'Heart Rate{}as{val}beats/min ({})',
        'Heart Rate{}as{val}beats/min']
p_tmpr = ['Temperature{}as{val}C ({}) (+{score})',
          'Temperature{}as{val}C ({})',
          'Temperature{}as{val}C']
p_rr = ['Respiratory Rate{}as{val}breaths/min ({}) (+{score})',
        'Respiratory Rate{}as{val}breaths/min({}) (+{score})',
        'Respiratory Rate{}as{val}breaths/min ({})',
        'Respiratory Rate{}as{val}breaths/min']
p_avpu = ["AVPU{}as '{val}' ({}) (+{score})",
          "AVPU{}as '{val}' ({})",
          "AVPU{}as '{val}'"]
p_bmi = ['BMI:{val}(+{score})',
         'BMI:{val}']
p_age = ['Age:{val}(+{score})',
         'Age:{val}']
p_fs = compile('Score:{score}')

column_names = ['BP', 'HR', 'Tmpr', 'RespRate', 'AVPU', 'BMI', 'Age']  # fullscore handled separately
measurements = ['Systolic B/P', 'Heart Rate', 'Temperature', 'Respiratory Rate', 'AVPU', 'BMI', 'Age']
measurements = [m.replace(' ', '') for m in measurements]
parsers = [p_sbp, p_hr, p_tmpr, p_rr, p_avpu, p_bmi, p_age]
parsers = [[compile(pp.replace(' ', '')) for pp in p] for p in parsers]
parsers = {measurements[i]: parsers[i] for i in range(len(parsers))}


def parse_full_alert(x):

    # error
    xo = x
    res = [np.NaN] * (len(measurements) * 2 + 1)  # first values then scores + fullscore
    if 'execution error' in x or 'ATTENTION' in x or 'You do not have the privilege' in x:
        return res
    x = x.replace('<row/>', '')
    x = x.replace(' ', '')
    if 'Score' not in x:
        x += 'Score:0'
    if "AVPUdocumentedas''" in x:
        x = x.replace("AVPUdocumentedas''", "AVPUdocumentedas'none'")

    # break text into segments, each containing info about one type of measurement
    separators = measurements + ['TotalScore', 'Score']
    separators = [s.replace(' ', '') for s in separators]
    indices = [x.find(sep) for sep in separators]
    sep_ind = sorted(zip(indices, separators))
    ranges = {}
    for i in range(len(sep_ind) - 1):  # -1 for not including ['Total Score', 'Score']
        if sep_ind[i][0] != -1:
            ranges[sep_ind[i][1]] = (sep_ind[i][0], sep_ind[i+1][0])
    ranges['TotalScore'] = (sep_ind[-1][0], len(x))

    # run parser on segmented text
    for i, meas in enumerate(measurements):
        if not (meas.replace(' ', '') not in x or meas.replace(' ', '') + 'notdocumented' in x.replace(' ', '')
                or (meas == 'BMI' and 'missing' in x)):
            text = x[ranges[meas][0]:ranges[meas][1]]  #.strip().replace(' ', '')
            res[len(measurements) + i] = 0  # default score
            pres = None
            for p_i, p in enumerate(parsers[meas]):
                pres = p.parse(text)
                if pres is not None:
                    break
            res[i] = float(pres['val']) if meas != 'AVPU' else pres['val']
            default_score = 0. if (meas != 'AVPU' and not np.isnan(res[i])) or (meas == 'AVPU' and res[i] != '') else np.NaN
            res[len(measurements) + i] = int(pres['score']) if 'score' in pres else default_score

    # full score
    if 'Score' not in xo:
        res[-1] = sum([please_not_nan for please_not_nan in res[len(measurements):] if not np.isnan(please_not_nan)])
    else:
        text = x[ranges['TotalScore'][0]:ranges['TotalScore'][1]]  #.strip()
        res[-1] = int(p_fs.parse(text)['score'])

    # ss = [c + 'Num' for c in column_names] + [c + 'Score' for c in column_names] + ['FullScore']
    # ss[ss.index('AVPUNum')] = 'AVPU'
    # for s in ["mews['{}'], ".format(s) for s in ss]:
    #     print(s, end='')
    # print(xo)
    # print({s: v for s, v in zip(ss, res)}, end='\n\n')

    return res


if __name__ == '__main__':

    # parse table with scores and values stored in a text
    data_size = ''  # ''/small_
    mews_table = 'data/MEWS_v2.csv'
    output_table = 'data/' + data_size + 'mews2.csv'
    # mews_table = 'data/MEWS_part3.csv'
    # output_table = 'data/' + data_size + 'mews3.csv'
    mews = pd.read_csv(mews_table, parse_dates=['AdmitDtm', 'AlertEntered']).sort_values(['ClientVisitGUID', 'AlertEntered'])
    mews = mews.dropna(axis=0, subset=['Full_Alert'])
    if data_size == 'small_':
        mews = mews.iloc[:200000]

    # value_columns = [c + 'Num' for c in column_names]
    # score_columns = [c + 'Score' for c in column_names]
    mews['BPNum'], mews['HRNum'], mews['TmprNum'], mews['RespRateNum'], mews['AVPU'], mews['BMINum'], mews['AgeNum'],\
    mews['BPScore'], mews['HRScore'], mews['TmprScore'], mews['RespRateScore'], mews['AVPUScore'], mews['BMIScore'],\
    mews['AgeScore'], mews['FullScore'] = zip(*mews['Full_Alert'].swifter.apply(parse_full_alert))

    mews['AlertDtm'] = mews['AlertEntered']
    mews = mews.drop(['AlertEntered', 'Full_Alert'], axis=1)
    mews.to_csv(output_table)

    print(mews['FullScore'].count(), len(mews))
    print(mews)
