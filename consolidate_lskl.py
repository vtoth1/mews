import numpy as np
import pandas as pd


# todo not just 2 recordings but each recording should have at least 2 unstable vitals
# todo +-10percentile stuff


if __name__ == '__main__':

    data_ver = '030521'
    tables_path = '/mnt/data/lskl/tables'
    base_dir = f'{tables_path}/processed_PALS_{data_ver}/'
    use_norm_minmax_abnorm = True  # takes the norm_min, norm_max values to redefine abnormal_low and abnormal_high

    use_perc_based_abnorm = True
    abnorm_perc = {'HR': (10, 90), 'RR': (10, 90), 'SBP': (10, 100), 'DBP': (10, 100)}

    # load vitals, remove duplicate rows and columns
    to_drop = ['BirthDtm', 'age_days', 'age_bin', 'norm_min', 'norm_max', 'abnormal']
    rename_template = lambda vname: {'ObsValue': f'{vname}', 'ObsDtm': f'AlertDtm',
                                     'abnormal_low': f'{vname}_low', 'abnormal_high': f'{vname}_high',
                                     'VisitID': 'ClientVisitGUID'}

    # redefine abnormal_low/high according to norm_min/max
    def update_abnorm(x):  # runs before renaming the columns
        x.abnormal_low = x.ObsValue < x.norm_min
        x.abnormal_high = x.ObsValue > x.norm_max
        x.abnormal = x.abnormal_low or x.abnormal_high
        return x

    if not use_norm_minmax_abnorm:
        update_abnorm = lambda x: x

    # percentile based abnormal definition
    def update_abnorm_perc(df, vname):  # runs after update_abnorm()
        col_names = rename_template(vname)

        if vname in abnorm_perc:
            bot_perc, top_perc = np.percentile(df[col_names['ObsValue']], abnorm_perc[vname])
            df[col_names['abnormal_low']] &= df[col_names['ObsValue']] < bot_perc[0]
            df[col_names['abnormal_high']] &= df[col_names['ObsValue']] > top_perc[1]
            df[col_names['abnormal']] = df[col_names['abnormal_low']] | df[col_names['abnormal_high']]

        return df

    if not use_perc_based_abnorm:
        update_abnorm_perc = lambda x, y: x

    # load visit lvl info
    ccmc = pd.read_csv(f'{base_dir}/ccmc_visits.csv').rename(columns={'VisitID': 'ClientVisitGUID'})
    locs = pd.read_csv(f'{base_dir}/ccmc_visit_locations.csv').rename(columns={'VisitID': 'ClientVisitGUID'})

    # prepare locs to be similar in structure to other tables
    locs = locs.rename(columns={'TransferRequestEndDtm': 'AlertDtm'}) \
        .drop(columns=['TransferRequestDtm', 'AdmitDate'])

    # prepare/clean each vital table separately
    hr = pd.read_csv(f'{base_dir}/hr_{data_ver}.csv').apply(update_abnorm, axis=1) \
        .rename(columns=rename_template('HR')).drop_duplicates(['ClientVisitGUID', 'AlertDtm'], keep='first') \
        .sort_values(['ClientVisitGUID', 'AlertDtm']).reset_index(drop=True).drop(columns=['abnormal', 'norm_min', 'norm_max'])
    # don't use to_drop here, so BirthDtm, age_days and age_bin remains at least in one dataset
    hr = update_abnorm_perc(hr, 'HR')

    rr = pd.read_csv(f'{base_dir}/rr_{data_ver}.csv').apply(update_abnorm, axis=1) \
        .rename(columns=rename_template('RR')).drop_duplicates(['ClientVisitGUID', 'AlertDtm'], keep='first') \
        .sort_values(['ClientVisitGUID', 'AlertDtm']).reset_index(drop=True).drop(columns=to_drop)
    rr = update_abnorm_perc(rr, 'RR')

    tc = pd.read_csv(f'{base_dir}/tc_{data_ver}.csv') \
        .rename(columns=rename_template('TC')).drop_duplicates(['ClientVisitGUID', 'AlertDtm'], keep='first') \
        .sort_values(['ClientVisitGUID', 'AlertDtm']).reset_index(drop=True).drop(columns=to_drop)
    tc = update_abnorm_perc(tc, 'TC')

    sbp = pd.read_csv(f'{base_dir}/sbp_{data_ver}.csv') \
        .rename(columns=rename_template('SBP')).drop_duplicates(['ClientVisitGUID', 'AlertDtm'], keep='first') \
        .sort_values(['ClientVisitGUID', 'AlertDtm']).reset_index(drop=True).drop(columns=to_drop, errors='ignore')
    sbp = update_abnorm_perc(sbp, 'SBP')

    spo2 = pd.read_csv(f'{base_dir}/spo2_{data_ver}.csv') \
        .rename(columns=rename_template('SPO2')).drop_duplicates(['ClientVisitGUID', 'AlertDtm'], keep='first') \
        .sort_values(['ClientVisitGUID', 'AlertDtm']).reset_index(drop=True).drop(columns=to_drop)
    spo2 = update_abnorm_perc(spo2, 'SPO2')

    # combine datasets, forward fill nan values, then only keep the HR records
    print('combination starts')

    hr['aligner'] = True
    combined = pd.concat([hr, rr, tc, sbp, spo2, locs], axis=0, ignore_index=True)
    combined.dropna(subset=['ClientVisitGUID', 'AlertDtm'], inplace=True)  # somehow some nan visit ids survived this far
    combined['aligner'] = combined['aligner'].fillna(False)
    combined.sort_values(['ClientVisitGUID', 'AlertDtm'], inplace=True, ignore_index=True)
    combined.reset_index(drop=True, inplace=True)

    # forward fill per group then fill LocationName separately with a backfilled FirstLocation
    # when filling nans no limit given, so pretty old measurements can stick
    combined_f = combined.groupby('ClientVisitGUID', as_index=False, sort=False).transform(lambda x: x.fillna(method='ffill'))
    combined = combined_f.assign(ClientVisitGUID=combined['ClientVisitGUID'])  # as_index=False does not work above with transform...
    combined['LocationName'] = combined.groupby('ClientVisitGUID', as_index=False, sort=False) \
        .apply(lambda x: x['LocationName'].fillna(x['FirstLocation'].fillna(method='bfill'))).reset_index(drop=True)
    combined = combined.loc[combined['aligner']].reset_index(drop=True)

    # join visit level info to recordings (removes those that are not in ccmc)
    combined = combined.merge(ccmc, 'inner', on='ClientVisitGUID').reset_index(drop=True)

    visit_id_to_check = 1024498  # should not have this
    assert not np.any(combined['ClientVisitGUID'] == visit_id_to_check)  # should not be here

    # save whole and mini dataset
    combined.to_csv(f'{tables_path}/lskl_{data_ver}.csv')
    combined.iloc[:20000].to_csv(f'{tables_path}/mini_lskl_{data_ver}.csv')
    print('combined dataset saved')
