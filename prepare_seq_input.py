import tables
import pandas as pd
import numpy as np
from preprocess_mews import npgroupby
import copy
import pickle


# gather statistics on input by outcome  # FIXME rm
INPUTS = []
OUTCOMES = []
COL = None
def gather_input_outcome_stats(night_vals, pred, threshold, col):
    global INPUTS, OUTCOMES, COL
    COL = col
    OUTCOMES.append(pred)
    if pred == 0:
        INPUTS.append(night_vals[0])
    else:
        unstable_point = np.where(night_vals[:, col['FullScore']] >= threshold)[0][0]
        INPUTS.append(night_vals[unstable_point])


def data_file_name(cfg, data_size, prefix=''):  # should be named seq_file_name
    min_seq_len = cfg['min_seq_len']
    input_fields = cfg['input']
    predicted_fields = cfg['predict']
    max_seq_len = cfg['seq_len']
    task = cfg['task']
    test_data_ratio = cfg['test_data_ratio']
    rand_scheme = cfg['test_rand_scheme']
    augm_step = cfg['data_augmentation']['step']
    augm_skip = cfg['data_augmentation']['skip']

    dsize = '' if data_size == '' else data_size + '_'
    augm = '{}-{}-{}-{}'.format(augm_step[0], augm_step[1], augm_skip[0], augm_skip[1])
    return '{}{}{}_{}_{}_{}_test{}_augm{}_{}-{}seqs.h5'.format(prefix, dsize, task, rand_scheme, '+'.join(input_fields),
                                                               '+'.join(predicted_fields), test_data_ratio, augm,
                                                               min_seq_len, max_seq_len)


# rand_scheme can be: 1) rand choice within samples, 2) rand choose last samples in each visit, 3) choose whole visits
#   1) 'samples', 2) 'last_sample', 3) 'visits'
# returns bool for each sample, True means test
def rand_choose_test(nsample, rand_scheme, test_ratio):
    if rand_scheme == 'samples':
        return np.random.rand(nsample) < test_ratio
    elif rand_scheme == 'last_sample':
        ret = np.zeros(nsample, dtype=bool)
        ret[-1] = np.random.rand() < test_ratio
        return ret
    elif rand_scheme == 'visits':
        ret = np.zeros(nsample, dtype=bool)
        return (ret + (1 if np.random.rand() < test_ratio else 0)).astype(bool)
    raise ValueError


# training set history should not include test set predictions
# obviously predictions should not overlap either, but that should already have been dealt with
# test history can contain anything before the prediction, train history only back until the prev test prediction
def adjust_history(are_test, pred_boundaries, hist_boundaries):
    latest_test_boundary_end = 0
    for i, (is_test, pred_boundary) in enumerate(zip(are_test, pred_boundaries)):
        if is_test:
            latest_test_boundary_end = pred_boundary[1]
        else:
            hist_boundaries[i][0] = latest_test_boundary_end

    return hist_boundaries


MORNING_START, NIGHT_START = 6, 22
OVER_PRED_AHEAD = (MORNING_START - NIGHT_START) % 24  # when using daytime data to train, the amount of hours to predict over


def are_nights(tod):  # np array of time of day values
    return (tod >= NIGHT_START) | (tod < MORNING_START)


def when_last_night(sample, col, min_seq_len):
    tod = sample[:, col['tod']]

    # find last night
    nights = are_nights(tod)
    if not np.any(nights):
        return None, None

    end_of_last_night = max(loc for loc, val in enumerate(tod) if val >= NIGHT_START or val < MORNING_START)
    if end_of_last_night < min_seq_len:
        return None, None

    start_of_last_night = end_of_last_night
    while nights[start_of_last_night - 1] and start_of_last_night > 0:
        start_of_last_night -= 1
    if start_of_last_night < min_seq_len:
        return None, None

    return start_of_last_night, end_of_last_night


def when_all_nights(tod):
    nights = are_nights(tod)
    if not np.any(nights):
        return []

    night_boundaries = []
    prev_is_night = False
    for i, is_night in enumerate(nights):
        if is_night and not prev_is_night:  # dawn
            night_boundaries.append([i, i + 1])
        elif is_night and prev_is_night:  # still night
            night_boundaries[-1][1] += 1
        prev_is_night = is_night

    return night_boundaries


def when_all_nights_and_days(tod):
    nights = are_nights(tod)
    if not np.any(nights):
        return [], []

    day_boundaries = []
    night_boundaries = []
    prev_is_night = False
    for i, is_night in enumerate(nights):
        if is_night and not prev_is_night:  # dusk
            night_boundaries.append([i, i + 1])
        elif is_night and prev_is_night:  # still night
            night_boundaries[-1][1] += 1

        # any daytime start a new pair of boundaries, close it after OVER_PRED_AHEAD hours
        if not is_night:
            # daytime sequence starts at i now
            seq_t = 0  # accumulated length of the sequence
            for j in range(i + 1, len(tod)):
                dt = (tod[j] - tod[i]) % 24  # mod 24 works only if the gaps are not higher than 24 (should be true)
                seq_t += dt
                if seq_t > OVER_PRED_AHEAD:  # exceeded the amount of time to predict ahead
                    # finish and don't include record j (upper boundary is non-inclusive)
                    day_boundaries.append([i, j])
                    break
            # note: if the future sequence time does not exceed OVER_PRED_AHEAD, no day sample is added

        prev_is_night = is_night

    return night_boundaries, day_boundaries


# to standardize across sample extractor functions, how a sample should look like
# add newid (visitid) and sequence start and end dates as extra parameters to the tuple
# end dates are offset by 1 minute so they can be used as non-inclusive upper boundary (x < end_date)
# assemble + sample = assample; smart huh?
def assample(visit, col, start_i, end_i, pred_start_i, pred_end_i, hist, pred, interest, is_test):
    return hist, pred, interest, is_test, visit[0, col['newid']],\
           visit[start_i, col['AlertDtm']], visit[end_i - 1, col['AlertDtm']] + pd.DateOffset(minutes=1),\
           visit[pred_start_i, col['AlertDtm']], visit[pred_end_i - 1, col['AlertDtm']] + pd.DateOffset(minutes=1)


def regular_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    predicted_field_indices = [col[f] for f in predicted_fields]

    # determine the samples (prev values), then which is train/test, then adjust history
    pred_boundaries = np.arange(min_seq_len - 1, len(visit))
    pred_boundaries = [[s, e] for s, e in zip(pred_boundaries, pred_boundaries + 1)]
    are_test = rand_choose_test(len(pred_boundaries), rand_scheme, test_data_ratio)

    # by default from beginning until pred (including pred, later masking out everything but time variables)
    hist_boundaries = [[0, pred_s + 1] for pred_s, pred_e in pred_boundaries]
    hist_boundaries = adjust_history(are_test, pred_boundaries, hist_boundaries)

    samples = []
    except_time_mask = np.ones(len(col), dtype=bool)
    except_time_mask[[col[c] for c in ['td', 'tod']]] = False
    for i, (is_test, pred_boundary, hist_boundary) in enumerate(zip(are_test, pred_boundaries, hist_boundaries)):
        if hist_boundary[1] - hist_boundary[0] < min_seq_len:
            continue
        sample = visit[hist_boundary[0]:hist_boundary[1]].copy()
        pred = np.array(sample[-1, predicted_field_indices], dtype=np.float32)  # first take the predicted values, then mask
        interest = int(sample[-1, col['FullScore']] > 6. and sample[-2, col['FullScore']] < 7.)
        sample[-1, except_time_mask] = 0.  # zero out all but time
        hist = sample
        samples.append(assample(visit, col, hist_boundary[0], hist_boundary[1], pred_boundary[0], pred_boundary[1],
                                hist, pred, interest, is_test))

    return samples


def overnightX_samples(threshold, visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    # training and testing only on night predictions
    tod = visit[:, col['tod']]

    # find nights
    pred_boundaries = when_all_nights(tod)
    if len(pred_boundaries) == 0:
        return []
    # history before night at least min_seq_len long
    pred_boundaries = [[s, e] for s, e in pred_boundaries if s >= min_seq_len]
    if len(pred_boundaries) == 0:
        return []

    # choose test samples, cover history
    are_test = rand_choose_test(len(pred_boundaries), rand_scheme, test_data_ratio)
    hist_boundaries = [[0, pred_s] for pred_s, pred_e in pred_boundaries]
    hist_boundaries = adjust_history(are_test, pred_boundaries, hist_boundaries)

    samples = []
    for i, (is_test, pred_boundary, hist_boundary) in enumerate(zip(are_test, pred_boundaries, hist_boundaries)):
        hist = visit[hist_boundary[0]:hist_boundary[1], :].copy()
        pred = np.array([float(np.any(visit[pred_boundary[0]:pred_boundary[1], col['FullScore']] >= threshold))], dtype=np.float32)
        interest = int(pred[0] == 1.)
        samples.append(assample(visit, col, hist_boundary[0], hist_boundary[1], pred_boundary[0], pred_boundary[1],
                                hist, pred, interest, is_test))

    return samples


def overdayNnightX_samples(threshold, visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    # training on everything, testing only on night prediction
    tod = visit[:, col['tod']]

    # find nights
    night_boundaries, day_boundaries = when_all_nights_and_days(tod)
    for pred_boundaries in [night_boundaries, day_boundaries]:
        if len(pred_boundaries) == 0:
            return []
        # history before night/day at least min_seq_len long
        pred_boundaries = [[s, e] for s, e in pred_boundaries if s >= min_seq_len]
        if len(pred_boundaries) == 0:
            return []

    # choose test samples, cover history
    # all day samples are in training
    # simply multiply the test_data_ratio by 2.5, assuming that day samples are a somewhat overrepresented
    night_are_test = rand_choose_test(len(night_boundaries), rand_scheme, test_data_ratio * 2.5)  # TODO FIXME isn't this too high? check the performance results for model w/o varscores and dfs - that model was trained with a 1.5 seq
    day_are_test = np.array([False] * len(day_boundaries))  # all are training samples
    # comb together day n night with their test flag
    # adjust history needs sorted boundaries, so sort them
    night_boundaries = [(b[0], b[1], is_test) for b, is_test in zip(night_boundaries, night_are_test)]
    day_boundaries = [(b[0], b[1], is_test) for b, is_test in zip(day_boundaries, day_are_test)]
    pred_boundaries = sorted(night_boundaries + day_boundaries)  # by the first boundary
    are_test = np.array([is_test for _, _, is_test in pred_boundaries])  # extract test flag
    pred_boundaries = [[pred_s, pred_e] for pred_s, pred_e, _ in pred_boundaries]  # remove test flag

    hist_boundaries = [[0, pred_s] for pred_s, pred_e in pred_boundaries]
    hist_boundaries = adjust_history(are_test, pred_boundaries, hist_boundaries)

    samples = []
    for i, (is_test, pred_boundary, hist_boundary) in enumerate(zip(are_test, pred_boundaries, hist_boundaries)):
        hist = visit[hist_boundary[0]:hist_boundary[1], :].copy()
        pred = np.array([float(np.any(visit[pred_boundary[0]:pred_boundary[1], col['FullScore']] >= threshold))], dtype=np.float32)

        if is_test:  # FIXME just remove if causes problems, it was only needed for the rebuttal
            gather_input_outcome_stats(visit[pred_boundary[0]:pred_boundary[1]], pred, threshold, col)

        # is_last_night = np.any(visit[pred_boundary[0]:pred_boundary[1], col['last']] == 1)\
        #                 and np.all(are_nights(visit[pred_boundary[0]:pred_boundary[1], col['tod']]))
        # pred[0] = 1. if is_last_night else pred[0]
        interest = int(pred[0] == 1.)
        samples.append(assample(visit, col, hist_boundary[0], hist_boundary[1], pred_boundary[0], pred_boundary[1],
                                hist, pred, interest, is_test))

    return samples


def overdayNnight_LSKL_samples(pred_fun, visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    # training on everything, testing only on night prediction
    tod = visit[:, col['tod']]

    # find nights
    night_boundaries, day_boundaries = when_all_nights_and_days(tod)
    for pred_boundaries in [night_boundaries, day_boundaries]:
        if len(pred_boundaries) == 0:
            return []
        # history before night/day at least min_seq_len long
        pred_boundaries = [[s, e] for s, e in pred_boundaries if s >= min_seq_len]
        if len(pred_boundaries) == 0:
            return []

    # choose test samples, cover history
    # all day samples are in training
    # simply multiply the test_data_ratio by 2.5, assuming that day samples are a somewhat overrepresented
    night_are_test = rand_choose_test(len(night_boundaries), rand_scheme, test_data_ratio * 2.5)
    day_are_test = np.array([False] * len(day_boundaries))  # all are training samples
    # comb together day n night with their test flag
    # adjust history needs sorted boundaries, so sort them
    night_boundaries = [(b[0], b[1], is_test) for b, is_test in zip(night_boundaries, night_are_test)]
    day_boundaries = [(b[0], b[1], is_test) for b, is_test in zip(day_boundaries, day_are_test)]
    pred_boundaries = sorted(night_boundaries + day_boundaries)  # by the first boundary
    are_test = np.array([is_test for _, _, is_test in pred_boundaries])  # extract test flag
    pred_boundaries = [[pred_s, pred_e] for pred_s, pred_e, _ in pred_boundaries]  # remove test flag

    hist_boundaries = [[0, pred_s] for pred_s, pred_e in pred_boundaries]
    hist_boundaries = adjust_history(are_test, pred_boundaries, hist_boundaries)

    samples = []
    for i, (is_test, pred_boundary, hist_boundary) in enumerate(zip(are_test, pred_boundaries, hist_boundaries)):
        hist = visit[hist_boundary[0]:hist_boundary[1], :].copy()
        pred = np.array([pred_fun(visit[pred_boundary[0]:pred_boundary[1], :], col)], dtype=np.float32)
        interest = int(pred[0] == 1.)
        samples.append(assample(visit, col, hist_boundary[0], hist_boundary[1], pred_boundary[0], pred_boundary[1],
                                hist, pred, interest, is_test))

    return samples


def overdayNnight_LSKL_OR_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    abn_cols = ['HR_low', 'HR_high', 'RR_low', 'RR_high', 'TC_low', 'TC_high', 'SBP_low', 'SBP_high', 'SPO2_low', 'SPO2_high']
    pred_fun = lambda vis_, col_: float(np.any(vis_[:, [col_[c] for c in abn_cols]]))  # OR function of all abnormal cols
    return overdayNnight_LSKL_samples(pred_fun, visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme)


def overdayNnight_LSKL_2_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):

    abn_cols = ['HR_low', 'HR_high', 'RR_low', 'RR_high', 'TC_low', 'TC_high', 'SBP_low', 'SBP_high', 'SPO2_low', 'SPO2_high']

    # at least 2 abnormal records, 1 abnormal vital each
    # pred_fun = lambda vis_, col_: float(np.any(vis_[:, [col_[c] for c in abn_cols]], axis=-1).sum() >= 2)

    # at least 2 abnormal records, 2 abnormal vitals each
    pred_fun = lambda vis_, col_: float((vis_[:, [col_[c] for c in abn_cols]].sum(axis=-1) >= 2).sum() >= 2)

    return overdayNnight_LSKL_samples(pred_fun, visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme)


def overnight7_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    return overnightX_samples(7., visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme)


def overnight5_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    return overnightX_samples(5., visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme)


def overnight6_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    return overnightX_samples(6., visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme)


def overdayNnight7_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    return overdayNnightX_samples(7., visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme)


def overdayNnight5_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    return overdayNnightX_samples(5., visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme)


def overdayNnight6_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    return overdayNnightX_samples(6., visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme)


def overnightD_samples(visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme):
    # train and test on nights only
    tod = visit[:, col['tod']]

    # find nights
    pred_boundaries = when_all_nights(tod)
    if len(pred_boundaries) == 0:
        return []
    # history before night at least min_seq_len long
    pred_boundaries = [[s, e] for s, e in pred_boundaries if s >= min_seq_len]
    if len(pred_boundaries) == 0:
        return []

    # cover history
    are_test = rand_choose_test(len(pred_boundaries), rand_scheme, test_data_ratio)
    hist_boundaries = [[0, pred_s] for pred_s, pred_e in pred_boundaries]
    hist_boundaries = adjust_history(are_test, pred_boundaries, hist_boundaries)

    samples = []
    for i, (is_test, pred_boundary, hist_boundary) in enumerate(zip(are_test, pred_boundaries, hist_boundaries)):
        hist = visit[hist_boundary[0]:hist_boundary[1], :].copy()
        pred = np.array([float(np.any(visit[pred_boundary[0]:pred_boundary[1], col['dfs']] >= 3.))], dtype=np.float32)
        interest = int(pred[0] == 1.)
        samples.append(assample(visit, col, hist_boundary[0], hist_boundary[1], pred_boundary[0], pred_boundary[1],
                                hist, pred, interest, is_test))

    return samples


# data prep functions create multiple samples (history and pred value pairs),
#   out of which the data augmentation function make actual (oversampled) sequences
# data prep functions further separate the dataset into train and test sets,
#   and determine whether the sample is 'interesting' = more of a candidate for oversampling
# subsequent data augmentation func needs all the history columns regardless of input fields,
#   but predicted fields are selected for
# params: visit, col, input_fields, predicted_fields, min_seq_len, seq_len, test_data_ratio, rand_scheme
DATA_PREP_FUNS = {'prediction': regular_samples, 'overnight7': overnight7_samples, 'overnight5': overnight5_samples,
                  'overnight6': overnight6_samples, 'overnightD': overnightD_samples,
                  'overdayNnight7': overdayNnight7_samples, 'overdayNnight5': overdayNnight5_samples,
                  'overdayNnight6': overdayNnight6_samples, 'overdayNnight_LSKL_OR_samples': overdayNnight_LSKL_OR_samples,
                  'overdayNnight_LSKL_2_samples': overdayNnight_LSKL_2_samples}


def delta_feature(feature, mews):
    mews['delta' + feature] = mews[feature].diff()
    mews.at[0, 'delta' + feature] = 0.  # otherwise it would be NaN
    return mews  # this requires mews to be sorted by ['newid', 'AlertDtm']


def var_scores_feature(feature, mews):
    score_cols = [c for c in mews.columns.values if 'Score' in c]
    var = mews[score_cols].var(axis=1, numeric_only=True)
    mews[feature] = var
    return mews


# list of features and corresponding functions to extract them from the mews table
# if any feature is included in the input/predict fields, they are extracted and added to the table
FEATURES = {'delta': delta_feature, 'varscores': var_scores_feature}


# augmentation by shifting history (cutting off from the left/earlier part) or by rand skipping from history
# has to return the original samples as well
# when shifting the first element's delta values (td, dfs) has to be updated to zero
# when skipping the delta values have to be updated for those whose left neighbor is removed
# interest (as in its name) is an int {0,1}, indexing step_size and skip, so interesting samples are can get oversampled
# nskip is the number of skippy samples that should be generated for not interesting and interesting
# also zeroes out first delta elements for all, including the original samples
# also cuts history if too long
# also :D removes history (input) fields that are not needed
def augment_samples(samples, col, step_size, nskip, min_seq_len, max_seq_len, input_fields, augment_test=False):
    augmented_samples = []
    input_field_indices = [col[f] for f in input_fields]

    # skipping
    # TODO rand the number of elements to remove and then which ones
    #   high probability for sequences containing almost all elements
    #   update delta values for first elements and every other where prev element was removed
    # TODO https://stackoverflow.com/questions/3679694/a-weighted-version-of-random-choice
    if nskip[0] != 0 or nskip[1] != 0:
        raise NotImplementedError

    # stepping
    for sample in samples:
        hist, pred, interest, is_test, newid, start_date, end_date, pred_start_date, pred_end_date = sample

        hist = hist[-max_seq_len:]  # cut history
        hist[0, col['td']] = 0.  # zero deltas out
        if 'dfs' in col:
            hist[0, col['dfs']] = 0.  # not actually necessary, just to be safe
        augmented_samples.append((hist[:, input_field_indices], pred, interest, is_test, newid,
                                  start_date, end_date, pred_start_date, pred_end_date,
                                  hist[:, col['AlertDtm']]))
        # pred contains pred fields only already

        if step_size[interest] == 0 or (is_test and not augment_test):  # don't augment test set
            continue

        hist_steps = np.arange(step_size[interest], len(hist) - min_seq_len + 1, step_size[interest])
        for step in hist_steps:
            extra_hist = hist[step:].copy()
            extra_hist[0, col['td']] = 0.
            extra_hist[0, col['dfs']] = 0.
            augmented_samples.append((extra_hist[:, input_field_indices], pred, interest, is_test, newid,
                                      start_date, end_date, pred_start_date, pred_end_date,
                                      hist[:, extra_hist['AlertDtm']]))
            # as there's no longer access to the visit, start dates of the sequence can be off

    return augmented_samples  # contains the original samples as well


def create_seq(cfg, mews_table, data_size='', base_dir='/mnt/data/mews/seqs/', prefix=''):

    # extract vars from config
    min_seq_len = cfg['min_seq_len']
    input_fields = cfg['input']
    predicted_fields = cfg['predict']
    max_seq_len = cfg['seq_len']
    task = cfg['task']
    test_data_ratio = cfg['test_data_ratio']
    rand_scheme = cfg['test_rand_scheme']
    augm_step = cfg['data_augmentation']['step']
    augm_skip = cfg['data_augmentation']['skip']

    # constants
    dtype = tables.Float32Atom()
    npdtype = np.float32
    static_seq_len = min_seq_len == max_seq_len

    # load mews, drop records with nans
    # dsize = '' if data_size == '' else data_size + '_'
    # mews_table = 'data/' + dsize + 'processed_mews2.csv'
    mews = pd.read_csv(mews_table, parse_dates=['AlertDtm']).sort_values(['newid', 'AlertDtm'])
    must_not_be_nan = list(set(mews.columns.values).intersection(set(input_fields + predicted_fields)))
    mews = mews.dropna(axis=0, how='any', subset=must_not_be_nan).reset_index(drop=True)

    # feature extraction
    extra_features = set(input_fields + predicted_fields) - set(mews.columns.values)
    delta_extra_features = [f for f in extra_features if 'delta' in f]
    for feature in extra_features:
        if 'delta' in feature:
            src_feature = feature[5:]
            mews = FEATURES['delta'](src_feature, mews)
        elif feature in FEATURES:
            mews = FEATURES[feature](feature, mews)
        else:
            print('WARNING: can\'t find field/feature: {}'.format(feature))

    # convert categorical features to integers
    categorical_features = {'Hospital'}  # set
    feature_mappings = {}
    for feature in categorical_features.intersection(input_fields):
        feature_mapping = {unique_val: i for i, unique_val in enumerate(np.unique(mews[feature]))}
        mews[feature] = mews[feature].map(feature_mapping).astype(np.int32)
        feature_mappings[feature] = feature_mapping

    # recompute delta values (after removing rows because of nans in them)
    # then group data by id
    col = {v: i for i, v in enumerate(mews.columns.values)}
    rmews = np.array(mews)
    td = np.concatenate([np.array([0]), np.diff(rmews[:, col['ts']])])
    rmews[:, col['td']] = td
    if 'dfs' in input_fields + predicted_fields:
        dfs = np.concatenate([np.array([0]), np.diff(rmews[:, col['FullScore']])])
        rmews[:, col['dfs']] = dfs

    rvisits = npgroupby(rmews, col['newid'])
    seq_len = min_seq_len if static_seq_len else min(max_seq_len, max([len(v) for v in rvisits]))

    # misc stuff
    h5fname = base_dir + data_file_name(cfg, data_size, prefix)
    if 'over' in task:
        predicted_fields = [task]
    newid_max_len = np.max([len(s) for s in mews['newid']])

    # construct hdf5 file out of sequences of samples
    h5file = tables.open_file(h5fname, mode='w')
    storage_train_x = h5file.create_earray(h5file.root, 'seq_train_x', dtype, shape=(0, seq_len, len(input_fields) + 1))  # +1 is EOS flag
    storage_valid_x = h5file.create_earray(h5file.root, 'seq_valid_x', dtype, shape=(0, seq_len, len(input_fields) + 1))
    storage_test_x = h5file.create_earray(h5file.root, 'seq_test_x', dtype, shape=(0, seq_len, len(input_fields) + 1))
    storage_train_y = h5file.create_earray(h5file.root, 'seq_train_y', dtype, shape=(0, len(predicted_fields)))
    storage_valid_y = h5file.create_earray(h5file.root, 'seq_valid_y', dtype, shape=(0, len(predicted_fields)))
    storage_test_y = h5file.create_earray(h5file.root, 'seq_test_y', dtype, shape=(0, len(predicted_fields)))

    storage_train_newid = h5file.create_earray(h5file.root, 'train_newid', tables.StringAtom(newid_max_len), shape=(0,))
    storage_valid_newid = h5file.create_earray(h5file.root, 'valid_newid', tables.StringAtom(newid_max_len), shape=(0,))
    storage_test_newid = h5file.create_earray(h5file.root, 'test_newid', tables.StringAtom(newid_max_len), shape=(0,))

    storage_train_start = h5file.create_earray(h5file.root, 'train_start', tables.UInt64Atom(), shape=(0,))  # hist start
    storage_valid_start = h5file.create_earray(h5file.root, 'valid_start', tables.UInt64Atom(), shape=(0,))
    storage_test_start = h5file.create_earray(h5file.root, 'test_start', tables.UInt64Atom(), shape=(0,))

    storage_train_end = h5file.create_earray(h5file.root, 'train_end', tables.UInt64Atom(), shape=(0,))
    storage_valid_end = h5file.create_earray(h5file.root, 'valid_end', tables.UInt64Atom(), shape=(0,))
    storage_test_end = h5file.create_earray(h5file.root, 'test_end', tables.UInt64Atom(), shape=(0,))

    storage_train_pred_start = h5file.create_earray(h5file.root, 'train_pred_start', tables.UInt64Atom(), shape=(0,))
    storage_valid_pred_start = h5file.create_earray(h5file.root, 'valid_pred_start', tables.UInt64Atom(), shape=(0,))
    storage_test_pred_start = h5file.create_earray(h5file.root, 'test_pred_start', tables.UInt64Atom(), shape=(0,))

    storage_train_pred_end = h5file.create_earray(h5file.root, 'train_pred_end', tables.UInt64Atom(), shape=(0,))
    storage_valid_pred_end = h5file.create_earray(h5file.root, 'valid_pred_end', tables.UInt64Atom(), shape=(0,))
    storage_test_pred_end = h5file.create_earray(h5file.root, 'test_pred_end', tables.UInt64Atom(), shape=(0,))

    storage_train_dtm = h5file.create_earray(h5file.root, 'train_dtm', tables.UInt64Atom(), shape=(0, seq_len))
    storage_valid_dtm = h5file.create_earray(h5file.root, 'valid_dtm', tables.UInt64Atom(), shape=(0, seq_len))
    storage_test_dtm = h5file.create_earray(h5file.root, 'test_dtm', tables.UInt64Atom(), shape=(0, seq_len))
    # when loading dates, use pd.Timestamp, to convert it back to the pandas date type

    storage_train_x.attrs['columns'] = input_fields + ['EOS']
    storage_valid_x.attrs['columns'] = input_fields + ['EOS']
    storage_test_x.attrs['columns'] = input_fields + ['EOS']
    storage_train_y.attrs['columns'] = predicted_fields
    storage_valid_y.attrs['columns'] = predicted_fields
    storage_test_y.attrs['columns'] = predicted_fields
    storage_train_x.attrs['categorical_features'] = feature_mappings
    storage_valid_x.attrs['categorical_features'] = feature_mappings
    storage_test_x.attrs['categorical_features'] = feature_mappings

    # indexed by visit_i, contains all the nights info; prosp create seq uses it down the line
    # structure: {visit_id: [night_start_date, night_end_date, already_predicted_flag, let_sleep_flag], ...}
    # save it as pickle file in the end, prosp create seq will pull it and fill the flags while feeding the prediction
    # this does not work when actual augmentation is applied
    NIGHTS = {}

    # go visit-by-visit
    ngen_samples = 0
    naugmented_samples = 0
    ntest_visits = nvalid_visits = ntrain_visits = 0  # only reliable for per visit randomization scheme
    ntest_records = nvalid_records = ntrain_records = 0
    eos_flags = np.ones((seq_len, 1))  # is_not_padding flag  # np.expand_dims(np.array(([0.] * (seq_len - 1)) + [1]), axis=-1)
    for visit_i in range(len(rvisits)):
        visit = rvisits[visit_i]
        if len(visit) < min_seq_len:
            continue

        # finish up updated delta values and update 'last' column
        for feature in set(['td', 'dfs'] + delta_extra_features).intersection(input_fields + predicted_fields):
            visit[0, col[feature]] = 0.
        visit[-1, col['last']] = 1.

        # extract samples
        samples = DATA_PREP_FUNS[task](visit, col, input_fields, predicted_fields, min_seq_len, seq_len,
                                       test_data_ratio, rand_scheme)
        samples = [s for s in samples if len(s[0]) >= min_seq_len]  # history has to be at least min_seq_len long
        ngen_samples += len(samples)

        # augment and cut according to (max_)seq_len
        # also removes unnecessary fields
        samples = augment_samples(samples, col, augm_step, augm_skip, min_seq_len, seq_len, input_fields)
        naugmented_samples += len(samples)

        # pad, add EOS then place it into hdf5
        # end of sequence (EOS) as an extra scalar, 1 when present, 0 when not
        visit_has_test_sample = False
        belongs_to_test = np.random.random() < 0.5
        NIGHTS[visit[0, col['newid']]] = []  # gonna fill this added array with test and valid nights
        for sample in samples:
            hist, pred, interest, is_test, newid, start_date, end_date, pred_start_date, pred_end_date, dtms = sample
            nrecords = len(hist)

            if not static_seq_len:
                pad_len = seq_len - hist.shape[0]
                eos_flags = np.concatenate([np.zeros((pad_len, 1)), np.ones((hist.shape[0], 1))], axis=0)  # = is_not_padding flags
                hist = np.pad(hist, ((pad_len, 0), (0, 0)), mode='constant', constant_values=(0, 0))
                dtms = np.array([d.value for d in dtms])  # prepare for pandas
                dtms = np.pad(dtms, (pad_len, 0), mode='constant', constant_values=(0, 0))  # 1D
            hist = np.array(np.append(hist, eos_flags, axis=1), dtype=npdtype)

            if is_test:
                visit_has_test_sample = True
                # start, end, predicted_flag, let_sleep_flag
                NIGHTS[newid].append([pred_start_date, pred_end_date, False, False])  # only test and valid = real nights

                if belongs_to_test:
                    ntest_records += nrecords
                    storage_test_x.append(hist[None])
                    storage_test_y.append(pred[None])

                    storage_test_newid.append([newid.encode('ascii')])
                    storage_test_start.append([start_date.value])
                    storage_test_end.append([end_date.value])

                    storage_test_pred_start.append([pred_start_date.value])
                    storage_test_pred_end.append([pred_end_date.value])
                    storage_test_dtm.append(dtms[None])
                else:  # 50% (of test+valid) goes to valid
                    nvalid_records += nrecords
                    storage_valid_x.append(hist[None])
                    storage_valid_y.append(pred[None])

                    storage_valid_newid.append([newid.encode('ascii')])
                    storage_valid_start.append([start_date.value])
                    storage_valid_end.append([end_date.value])

                    storage_valid_pred_start.append([pred_start_date.value])
                    storage_valid_pred_end.append([pred_end_date.value])
                    storage_valid_dtm.append(dtms[None])
            else:  # rest to train
                ntrain_records += nrecords
                storage_train_x.append(hist[None])
                storage_train_y.append(pred[None])

                storage_train_newid.append([newid.encode('ascii')])
                storage_train_start.append([start_date.value])
                storage_train_end.append([end_date.value])

                storage_train_pred_start.append([pred_start_date.value])
                storage_train_pred_end.append([pred_end_date.value])
                storage_train_dtm.append(dtms[None])

        ntest_visits += 1 if visit_has_test_sample and belongs_to_test else 0
        nvalid_visits += 1 if visit_has_test_sample and not belongs_to_test else 0
        ntrain_visits += 1 if not visit_has_test_sample else 0

        if visit_i % 5000 == 0:
            print('{}/{}'.format(visit_i, len(rvisits)))

    print('samples augmented {} out of {}'.format(naugmented_samples - ngen_samples, naugmented_samples))
    print('SAMPLES train: {}; valid: {}; test: {}'.format(len(storage_train_x), len(storage_valid_x), len(storage_test_x)))
    print(f'SAMPLES in detail: train+pos {np.sum(storage_train_y)}, train+neg {len(storage_train_y) - np.sum(storage_train_y)}, '
          f'valid+pos {np.sum(storage_valid_y)}, valid+neg {len(storage_valid_y) - np.sum(storage_valid_y)}, '
          f'test+pos {np.sum(storage_test_y)}, test+neg {len(storage_test_y) - np.sum(storage_test_y)}')
    print('total visits:', len(rvisits))
    print('VISITS train: {}; valid: {}; test: {} (not showing the right numbers for overdayNnightX)'.format(ntrain_visits, nvalid_visits, ntest_visits))
    print('RECORDS train: {}; valid: {}; test: {} (does not work because there are overlap between records)'.format(ntrain_records, nvalid_records, ntest_records))

    # references back to the full dataset (with features computed here) for post-inference analysis:
    # for each sequence, store the newid, the start and end indices of the sequence a in a separate h5 earray
    # separate 'reference' attributes for training and test; shared indexing with the train_x and test_x storages
    # if orig_data_ref:
    #     storage_train_x.attrs['newid'] = train_newid
    #     storage_train_x.attrs['start_date'] = train_seqstart
    #     storage_train_x.attrs['end_date'] = train_seqend
    #
    #     storage_test_x.attrs['newid'] = test_newid
    #     storage_test_x.attrs['start_date'] = test_seqstart
    #     storage_test_x.attrs['end_date'] = test_seqend

    h5file.close()

    # save the NIGHTS array for prosp create seq
    with open(h5fname + '_NIGHTS.pickle', 'wb') as f:
        pickle.dump(NIGHTS, f)


def mews_test():
    global INPUTS, OUTCOMES, COL

    cfg = {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight7',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'],
    # 'BMINum', varscores removed, FullScore
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [32] * 5,  # 256
        'min_seq_len': 4,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.9,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': 'dropout_batchnorm_cpucompat'  # but_regr, w_rnn (order: rnn, dropout, batchnorm, ens), cpucompat
    }
    cfg_prosp = copy.deepcopy(cfg)
    cfg_prosp['test_data_ratio'] = 1.  # all into test and validation

    data_size = ''  # mini/small/medium/''

    base_folder = '/mnt/data/mews/seqs_test/'
    base_folder_prospective = '/mnt/data/mews/seqs_prosp_test/'
    dsize = '' if data_size == '' else data_size + '_'
    mews_table = '/mnt/data/mews/tables/' + dsize + 'processed_mews2.csv'
    mews_prospective = '/mnt/data/mews/tables/' + dsize + 'processed_mews3.csv'
    dfname = data_file_name(cfg, data_size)
    dfname_prosp = data_file_name(cfg_prosp, data_size)

    print('CREATING DATA')
    create_seq(cfg, mews_table, data_size, base_folder)

    # TODO rm if causing problems, only needed for the rebuttal: stats of inputs of positive vs negative cases
    from scipy.stats import iqr
    INPUTS = np.array(INPUTS)
    OUTCOMES = np.array(OUTCOMES).ravel()
    vars = ['age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum']
    for cname, ci in COL.items():
        if cname in vars:
            stab, uns = INPUTS[OUTCOMES == 0, ci], INPUTS[OUTCOMES == 1, ci]
            print(f'STAT {cname}:\n\tNOT stable: mean({np.mean(uns)}), sd({np.std(uns)}), median({np.median(uns)}), iqr({iqr(uns)})'
                  f'\n\tstable: mean({np.mean(stab)}), sd({np.std(stab)}), median({np.median(stab)}), iqr({iqr(stab)})')
    INPUTS, OUTCOMES = [], []

    print('CREATING PROSPECTIVE DATA')
    create_seq(cfg_prosp, mews_prospective, data_size, base_folder_prospective)

    INPUTS = np.array(INPUTS)
    OUTCOMES = np.array(OUTCOMES).ravel()
    vars = ['age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum']
    for cname, ci in COL.items():
        if cname in vars:
            stab, uns = INPUTS[OUTCOMES == 0, ci], INPUTS[OUTCOMES == 1, ci]
            print(f'STAT {cname}:\n\tNOT stable: mean({np.mean(uns)}), sd({np.std(uns)}), median({np.median(uns)}), iqr({iqr(uns)})'
                  f'\n\tstable: mean({np.mean(stab)}), sd({np.std(stab)}), median({np.median(stab)}), iqr({iqr(stab)})')
    INPUTS, OUTCOMES = [], []


def lskl_test():

    cfg = {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight_LSKL_OR_samples',  # prediction type
        'input': ['td', 'tod', 'age_days', 'HR', 'TC', 'SBP', 'SPO2',
                  'HR_low', 'HR_high', 'RR_low', 'RR_high', 'TC_low', 'TC_high',
                  'SBP_low', 'SBP_high', 'SPO2_low', 'SPO2_high'],
        'predict': ['HR_high'],  # ignore
        'lr': 0.0001,
        'layers': [32] * 5,  # 256
        'min_seq_len': 4,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['td', 'tod'],  # add tod, td, LocationName
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.9,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': 'lskl'  # but_regr, w_rnn (order: rnn, dropout, batchnorm, ens), cpucompat
    }
    cfg_prosp = copy.deepcopy(cfg)
    cfg_prosp['test_data_ratio'] = 1.  # all into test and validation

    lskl_ver = '030521'
    data_size = ''  # mini/small/medium/''
    dsize = '' if data_size == '' else data_size + '_'

    lskl_path = '/mnt/data/lskl'
    lskl_table = f'{lskl_path}/tables/{dsize}processed_lskl_{lskl_ver}.csv'
    # lskl_prospective = f'{lskl_path}/tables/{dsize}processed_lskl_{lskl_ver}.csv'

    print('CREATING DATA')
    create_seq(cfg, lskl_table, data_size, f'{lskl_path}/seqs_test/')

    # print('CREATING PROSPECTIVE DATA')
    # create_seq(cfg_prosp, lskl_prospective, data_size, f'{lskl_path}/seqs_prosp_test/')


if __name__ == '__main__':
    # mews_test()
    lskl_test()


# min_seq_len = 4
# ===============
# retrospective
# samples augmented 0 out of 6866021
# SAMPLES train: 4689403; valid: 1088884; test: 1087734
# SAMPLES in detail: train+pos 238944.0, train+neg 4450459.0, valid+pos 16177.0, valid+neg 1072707.0, test+pos 16022.0, test+neg 1071712.0
# total visits: 918722  # TODO check total visits (before filtering on seq len) from that fancy plot
# VISITS train: 390889; valid: 261966; test: 261650 (does not work with overdayNnightX)
# RECORDS train: 80568582; valid: 23589540; test: 23534190 (does not work because there are overlap between records)

# prospective
# samples augmented 0 out of 367757
# SAMPLES train: 156461; valid: 105805; test: 105491
# SAMPLES in detail: train+pos 14848.0, train+neg 141613.0, valid+pos 1367.0, valid+neg 104438.0, test+pos 1549.0, test+neg 103942.0
# total visits: 73898
# VISITS train: 21229; valid: 26176; test: 26095 (does not work with overdayNnightX)
# RECORDS train: 980365; valid: 2289983; test: 2282701 (does not work because there are overlap between records)

# min_seq_len = 8
# ===============
# retrospective
# samples augmented 0 out of 4758018
# SAMPLES train: 2965396; valid: 898484; test: 894138
# SAMPLES in detail: train+pos 144538.0, train+neg 2820858.0, valid+pos 13935.0, valid+neg 884549.0, test+pos 14177.0, test+neg 879961.0
# total visits: 715471
# VISITS train: 294288; valid: 208757; test: 208163 (does not work with overdayNnightX)
# RECORDS train: 72211968; valid: 22501608; test: 22361650 (does not work because there are overlap between records)

# prospective
# samples augmented 0 out of 205730
# SAMPLES train: 30112; valid: 87206; test: 88412
# SAMPLES in detail: train+pos 5909.0, train+neg 24203.0, valid+pos 1282.0, valid+neg 85924.0, test+pos 1267.0, test+neg 87145.0
# total visits: 55204
# VISITS train: 12910; valid: 20875; test: 20990 (does not work with overdayNnightX)
# RECORDS train: 358702; valid: 2164576; test: 2203013 (does not work because there are overlap between records)