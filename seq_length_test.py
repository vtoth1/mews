import numpy as np
from glob import glob
import matplotlib.pyplot as plt
from lstm import model_name, load_data, seqs_of_length
from prepare_seq_input import data_file_name


if __name__ == '__main__':
    cfg = {
        'type': 'regression',  # regression, simple_normal, var_normal, gaussian
        'task': 'overdayNnight7',  # prediction, overnight5/6/7, overdayNnight5/6/7, overnightD
        'input': ['td', 'tod', 'age', 'FullScore', 'BPNum', 'HRNum', 'TmprNum', 'RespRateNum', 'dfs', 'varscores'], # 'BMINum', varscores removed, FullScore
        'predict': ['FullScore'],
        'lr': 0.0001,
        'layers': [32] * 5,  # 256
        'min_seq_len': 4,
        'seq_len': 42,  # max_seq_len
        'batch_size': 512,
        'onehot_fields': ['tod'],  # add tod, td, Hospital
        'test_rand_scheme': 'visits',  # randomization scheme of test data selection; visits, samples, last_sample
        'data_augmentation': {'step': (0, 0), 'skip': (0, 0)},
        'test_data_ratio': 0.3,
        'majority_class_rate': 0.9,  # maximum allowed amount of the majority class
        'class_weight': {0: 1., 1: 8.},
        'name_suffix': 'dropout_batchnorm_balanced_padflag_cpucompat'  # but_regr, w_rnn (order: rnn, dropout, batchnorm, ens), cpucompat
    }

    base_folder = '/mnt/data/mews/seqs/'
    base_folder_prospective = '/mnt/data/mews/seqs_prosp/'
    data_size = ''
    mews_table = '/mnt/data/mews/tables/' + data_size + 'processed_mews2.csv'
    dfname = data_file_name(cfg, data_size)

    # get clinically applicable AUCs for each length
    test_folder = 'results/' + model_name(cfg) + '/'
    seq_lengths = range(cfg['min_seq_len'], cfg['seq_len'] + 1)
    aucs = []
    for l in seq_lengths:
        test_folder_l = f'{test_folder}/{l}/'
        with open(f'{test_folder_l}/log.txt', 'rt') as logf:
            auc = logf.readline()
            auc = float(auc[auc.index(':') + 2:-1])
            aucs.append(auc)
    aucs = np.array(aucs)

    # account for the amount of positive cases at each lengths
    print('LOADING DATA')
    train_data_x, train_data_y, valid_data_x, valid_data_y, test_data_x, test_data_y, x_cols, y_cols, normies = \
        load_data(cfg, base_folder + dfname)

    pos_cases, neg_cases = [], []
    for l in seq_lengths:
        right_length = seqs_of_length(test_data_x, l, x_cols)
        pos_cases.append(test_data_y[right_length].sum())
        neg_cases.append((1 - test_data_y[right_length]).sum())
    pos_cases = np.array(pos_cases)
    neg_cases = np.array(neg_cases)
    pos_ratio = pos_cases / (pos_cases + neg_cases)

    # plot aucs
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    # ax1.plot(seq_lengths, aucs, 'g-', label='clinically applicable AUC')
    ax1.plot(seq_lengths, aucs / ((pos_ratio - pos_ratio.min()) / (pos_ratio.max() - pos_ratio.min())), 'r-', label='adjusted CA AUC')
    ax2.plot(seq_lengths, pos_ratio, 'b--', label='ratio of unstable cases')
    ax1.set_xlabel('Sequence length')
    ax1.set_ylabel('AUC', color='g')
    ax2.set_ylabel('Ratio of positive cases', color='b')
    plt.legend()
    plt.show()
